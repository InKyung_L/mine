import java.awt.Point;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_백준_17135_캐슬디펜스2 {
	public static int N, M, D, r, kill, max;
	public static int[] v, a, di= {0,-1,0}, dj= {-1,0,1};
	public static int[][] map, visit, temp;
	public static Queue<Point> q;
	public static Point[] p; //적의 위치 저장
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		D = Integer.parseInt(st.nextToken());
		
		map = new int[N][M];
		temp = new int[N][M];
		for(int i=0;i<N;i++) {
			st = new StringTokenizer(br.readLine());
			for(int j=0;j<M;j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		
		r=3;
		a=new int[r];
		v=new int[M];
		max = Integer.MIN_VALUE;
		permcomb(0,0);
		System.out.println(max);
	}

	private static void permcomb(int start, int count) {
		if(count==r) {
			//System.out.println(Arrays.toString(a));
			kill=0;
			attack();
			//System.out.println(kill);
			max = Math.max(max, kill);
			return;
		}
		
		for(int i=start;i<M;i++) {
			if(v[i]==0) {
				v[i]=1;
				a[count]=i;
				permcomb(i, count+1);
				v[i]=0;
			}
		}
	}

	private static void attack() {
		for(int i=0;i<map.length;i++) {
			temp[i]=map[i].clone();
		}
		int t=N;
		while(t>0) {
			p = new Point[M];
			for(int i=0;i<r;i++) {
				bfs(a[i]);
			}
			for(int i=0;i<M;i++) {
				if(p[i]!=null && temp[p[i].x][p[i].y]==1) {
					temp[p[i].x][p[i].y]=0;
					kill++;
				}
			}
			for(int i=N-2;i>=0;i--) {
				for(int j=0;j<M;j++) {
					temp[i+1][j]=temp[i][j];
				}
			}
			
			
			for(int j=0;j<M;j++) {
				temp[0][j]=0;
			}
			t--;
			//System.out.println(kill);
		}
		
	}

	private static void bfs(int i) {
		q = new LinkedList<Point>();
		visit = new int[N][M];
		q.offer(new Point(N-1,i));
		visit[N-1][i]=1;
		int mdis=0;
		
		while(!q.isEmpty()) {
			int x = q.peek().x;
			int y = q.poll().y;
			
			int dis = Math.abs(N-x) + Math.abs(i-y);
			if(temp[x][y]==1 && visit[x][y]<=D) {
				if(p[i]==null) {
					p[i]=new Point(x,y);
					mdis = dis;
				}else if(p[i]!=null && y<p[i].y && visit[x][y]==visit[p[i].x][p[i].y]) {
					p[i]=new Point(x,y);
				}
			}
			
			for(int d=0; d<di.length; d++) {
				int nx = x+di[d];
				int ny = y+dj[d];
				
				if(nx>=0 && nx<N && ny>=0 && ny<M && visit[nx][ny]==0) {
					visit[nx][ny]=visit[x][y]+1;
					q.offer(new Point(nx,ny));
				}
			}
		}
	}
}
