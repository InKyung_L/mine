package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D3_6900{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_6900.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int N = sc.nextInt();
			int M = sc.nextInt();
			String[] num = new String[N];
			int[] money = new int[N];
			int[] star = new int[N];
			int sum=0, cnt=0;
			
			for(int i=0;i<N;i++) {
				num[i] = sc.next();
				money[i] = sc.nextInt();
			}
			
			for(int i=0;i<N;i++) {
				cnt=0;
				for(int j=0;j<num[i].length();j++) {
					if(num[i].charAt(j) != '*') cnt++;
				}
				star[i] = cnt;
			}
			
			for(int i=0;i<M;i++) {
				String lot = sc.next();
				
				for(int j=0;j<N;j++) {
					cnt=0;
					for(int l=0;l<8;l++) {
						if(num[j].charAt(l) != '*' && num[j].charAt(l)==lot.charAt(l)) cnt++;
					}
					if(cnt == star[j]) sum+=money[j];
				}
			}
			
			System.out.println("#" + tc + " " + sum);
		}
		
	}
}