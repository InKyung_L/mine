package com.swe;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

class Solution_D3_4522{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_4522.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();	
		for(int i=0;i<T;i++) {
			int cnt=0;
			String word = sc.next();
			int size = (int)Math.ceil(word.length()/2);
			for(int j=0;j<size;j++) {
				if(word.charAt(j)==word.charAt(word.length()-1-j)) cnt++;
				else {
					if(word.charAt(j)=='?' || word.charAt(word.length()-1-j)=='?') cnt++;
				}
			}
			System.out.print("#" + (i+1) + " ");
			if(cnt==size) {
				System.out.println("Exist");
			}else {
				System.out.println("Not exist");			
			}
		}
	}
}