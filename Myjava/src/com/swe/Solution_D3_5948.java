package com.swe;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;


class Solution_D3_5948{
	public static int n,r,a[], v[], cnt, num[], sum;
	public static Set<Integer> set;
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_5948.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			set = new HashSet<>();
			num = new int[7];
			
			for(int i=0;i<7;i++) {
				num[i] = sc.nextInt();
			}
			
			n=7;
			r=3;
			a = new int[r];
			v = new int[n];
			permcomb(0,0);
			
			ArrayList<Integer> arr = new ArrayList<>(set);
			Collections.sort(arr);
//			System.out.println(arr);
//			System.out.println(arr.get(arr.size()-5));
			System.out.println("#" + tc + " " + arr.get(arr.size()-5));
		}
		
	}
	private static void permcomb(int start, int count) {
		if(count == r) {
			sum=0;
			cnt++;
			for(int i=0;i<r;i++) {
				sum += a[i];
			}
			set.add(sum);
			//System.out.println(Arrays.toString(a) + " " + sum);
			return;
		}
		
		for(int i=start;i<n;i++) {
			if(v[i] == 0) {
				v[i] = 1;
				a[count] = num[i];
				permcomb(i ,count+1);
				v[i]=0;
			}
		}
		
	}
}