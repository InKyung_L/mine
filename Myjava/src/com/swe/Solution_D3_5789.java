package com.swe;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;


class Solution_D3_5789{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_5789.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int num = sc.nextInt();
			int loop = sc.nextInt();
			int[] arr = new int[num+1];
			Arrays.fill(arr, 0);
			
			for(int i=1;i<=loop;i++) {
				int f = sc.nextInt();
				int s = sc.nextInt();
				
				for(int j=f;j<=s;j++) {
					arr[j] = i;
				}
			}
			System.out.print("#" + tc + " ");
			for(int i=1;i<=num;i++) {
				System.out.print(arr[i] + " ");
			}
			System.out.println();
		}
		
	}
}