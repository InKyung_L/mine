package com.swe;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

class Solution_D4_1233{
	//public static char st[];
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_1233.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		
	
		for(int tc=1;tc<=10;tc++) {
			int T = Integer.parseInt(br.readLine());
			
			int ans=1;
			loop:for(int i=0;i<T;i++) {
				StringTokenizer st = new StringTokenizer(br.readLine());
				String[] sen = new String[st.countTokens()];
				int size = st.countTokens();
				
				for(int j=0;j<size;j++) {
					sen[j] = st.nextToken();
				}
				if(ans==0)continue loop; 
				if(sen.length==4) {
					if(Character.isDigit((sen[1].charAt(0)))) {
						ans=0;
					}
				}else{
					if(Character.isDigit((sen[1].charAt(0)))) {
						ans=1;	
					}
				}
				
			}
			System.out.println("#" + tc + " " + ans);
		}
		
	}
}