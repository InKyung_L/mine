package com.swe;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;

class Solution_D3_7087{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_7087.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int loop = sc.nextInt();
			int cnt=1;
			String[] dic = new String[loop];
			HashSet<Character> word = new HashSet<>();
			
			for(int i=0;i<loop;i++) {
				dic[i] = sc.next();
				word.add(dic[i].charAt(0));
			}
			ArrayList<Character> l = new ArrayList<Character>(word);
			Collections.sort(l);
			
			
			for(int i=0;i<l.size()-1;i++) {
				if(l.get(0) == 'A') {
					if(Math.abs(l.get(i)-l.get(i+1))==1) cnt++;
					else break;
				}else {
					cnt=0;
				}
			}
			
			System.out.println("#" + tc + " " + cnt);
		}
		
	}
}