package com.swe;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
  
public class Solution_D4_3234 {
    static int T, N; // N : 무게 추의 수
    private static int[] w;
    private static int cnt;
      
    public static void main(String[] args) throws Exception{
    	System.setIn(new FileInputStream("res/input_d4_3234.txt"));
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
       
        T = Integer.parseInt(br.readLine());
        for (int tc = 1; tc <= T; tc++) {
            N = Integer.parseInt(br.readLine());
            
            StringTokenizer st = new StringTokenizer(br.readLine());
            w = new int[N];
            for (int i = 0; i < w.length; i++) {
                w[i] = Integer.parseInt(st.nextToken());
            }
            cnt = 0; 
            perm(0, 0, 0);
            System.out.println("#" + tc + " " + cnt);
        }
    }
  
    private static void perm(int step, int left, int right) {
        if (step == w.length) {
            cnt++;
            //System.out.println(Arrays.toString(w));
        } else {
            for (int i = step; i < w.length; i++) {
                int temp = w[step];
                w[step] = w[i];
                w[i] = temp;
                perm(step+1, left+w[step], right);
                
                if (left >= right+w[step]) {
                    perm(step+1, left, right+w[step]); 
                }
                
                temp = w[step];
                w[step] = w[i];
                w[i] = temp;
            }
        }
    }
}