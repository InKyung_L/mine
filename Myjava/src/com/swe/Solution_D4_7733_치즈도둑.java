package com.swe;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Stack;
import java.util.StringTokenizer;


class Solution_D4_7733{
	public static int day, N, cnt ,d, mday;
	public static int[][] cheeze, max, visit;
	public static int[] di= {1,-1,0,0}, dj= {0,0,-1,1};
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_7733.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1;tc<=T;tc++) {
			N = Integer.parseInt(br.readLine());
			cheeze = new int[N][N];
			max = new int[1][2]; //day와 max블록값 저장
			for(int i=0;i<N;i++) {
				StringTokenizer st = new StringTokenizer(br.readLine());
				for(int j=0;j<N;j++) {
					cheeze[i][j] = Integer.parseInt(st.nextToken());
					mday = Math.max(mday, cheeze[i][j]);
				}
			}//end of cheeze
			
			for(d=0;d<=mday;d++) {
				visit=new int[N][N];
				cnt=0;
				for(int i=0;i<N;i++) {
					for(int j=0;j<N;j++) {
						if(cheeze[i][j]==d) {
							cheeze[i][j]=0;
						}
					}
				}
				for(int i=0;i<N;i++) {
					for(int j=0;j<N;j++) {
						if(cheeze[i][j]!=0 && visit[i][j]==0) {
							check(i,j);
							cnt++;	
						}
					}
				}
				if(max[0][1] < cnt) {
					max[0][1]=cnt;
					max[0][0]=d+1;
				}
			}//end of day
			
			System.out.println("#" + tc + " " + max[0][1]);
		}//end of tc	
	}//end of main
	
	private static void check(int i, int j) {
		visit[i][j]=1;
		
		for(int v=0;v<di.length;v++) {
			int ni = i+di[v];
			int nj = j+dj[v];
			
			if(ni>=0 && ni<N && nj>=0 && nj<N && cheeze[ni][nj]!=0 && visit[ni][nj]==0) {
				visit[ni][nj]=1;
				check(ni,nj);
			}
		}
	}
	
}