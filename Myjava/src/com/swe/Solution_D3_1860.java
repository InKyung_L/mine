package com.swe;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

class Solution_D3_1860{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1860.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1;tc<=T;tc++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			int ck=0;
			int N = Integer.parseInt(st.nextToken());
			int M = Integer.parseInt(st.nextToken());
			int K = Integer.parseInt(st.nextToken());		
			
			StringTokenizer st1 = new StringTokenizer(br.readLine());
			int[] bb = new int[N];
			for(int i=0;i<N;i++) {
				bb[i] = Integer.parseInt(st1.nextToken());
			}
			Arrays.sort(bb);
			if(bb[0]<M) ck=0;
			else {
				int bread=0, time=0;
				for(int i=0;i<N;i++) {
					while(M<bb[i]) {
						bread+=K;
						time+=M;
					}
					if(bb[i]<time) break;
				}
				
			}
		}
		
	}
}