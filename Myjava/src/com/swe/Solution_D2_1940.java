package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D2_1940{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d2_1940.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int loop = sc.nextInt();
			int ans = 0;
			int ms=0;
			int spd = 0;
			for(int i=0;i<loop;i++) {
				int pm = sc.nextInt(); //plus minus
				if(pm != 0) ms = sc.nextInt(); //speed

				if(pm == 1) {
					spd += ms;
				}else if(pm == 2) {
					spd -= ms;
				}
				if(spd<0) spd=0;
				
				ans += spd;
			}
			
			System.out.println("#" + tc + " " + ans);
		}
		
	}
}