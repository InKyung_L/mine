package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D3_1206{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1206.txt"));
		Scanner sc = new Scanner(System.in);
		

		for(int tc=1;tc<=10;tc++) {
			int cases = sc.nextInt();
			int[] buildings = new int[cases];
			int max = 0, max2 = 0, sum=0;
			boolean find = false;
			
			for(int i=0;i<cases;i++) {
				buildings[i] = sc.nextInt();
			}
			
			for(int i=2;i<cases-2;i++) {
				if(buildings[i] > buildings[i+1] && buildings[i] > buildings[i-1]) {
					max = Math.max(buildings[i+1], buildings[i-1]);
					if(buildings[i] > buildings[i+2] && buildings[i] > buildings[i-2]) {
						find = true;
						max2 = Math.max(buildings[i-2], buildings[i+2]);
						max = Math.max(max, max2);
					}
				}
				if(find == true) sum += buildings[i]-max;
				max = 0; max2 = 0;
				find = false;
			}
			System.out.println("#" + tc + " " + sum);
		}
		
	}
}