package com.swe;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;


class Solution_D3_4047{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_4047.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			String card = sc.next();
			int size = card.length()/3;
			char[] cards = new char[size];
			int[] num = new int[size];
			boolean flag = true;
			int s=13,d=13,h=13,c=13;
			
			int ic=0, in=0;
			for(int i=0;i<card.length();i++) {
				if(i%3 == 0) cards[ic++] = card.charAt(i);
				else if(i%3 == 1) num[in++] = ((card.charAt(i)-'0')*10 + (card.charAt(i+1)-'0'));
			}
			
			for(int i=0;i<size-1;i++) {
				for(int j=i+1;j<size;j++) {
					if(cards[i] == cards[j]) {
						if(num[i] == num[j]) flag = false;
					}
				}
			}
			if(flag == true) {
				for(int i=0;i<size;i++) {
					if(cards[i] == 'S') {
						s--;
					}else if(cards[i] == 'D') {
						d--;
					}else if(cards[i] == 'H') {
						h--;
					}else if(cards[i] == 'C') {
						c--;
					}
				}
			}
			
			System.out.print("#" + tc + " ");
			if(flag == false) System.out.println("ERROR");
			else {
				System.out.println(s + " " + d + " " + h + " " + c);
			}
		}
		
	}
}