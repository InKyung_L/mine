package com.swe;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

class Solution_D3_6019{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_6019.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();	
		for(int tc=1;tc<=T;tc++) {
			double ans=0.0;
			double[] arr = new double[4];
			
			for(int i=0;i<4;i++) {
				arr[i]=sc.nextDouble();
			}
			double sp = arr[1]+arr[2];
			ans = (arr[0]/sp)*arr[3];
			
			System.out.println("#" + tc + " " + String.format("%.6f", ans));
		}
	}
}