package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D2_1926{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d2_1926.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int i=1;i<=T;i++) {
			if(i/10 == 3 || i/10 == 6 || i/10 == 9) {
				if(i%10 == 3 || i%10 == 6 || i%10 == 9) {
					System.out.print("--"+ " ");
				}
				else System.out.print("-"+ " ");
			}
			else if(i%10 == 3 || i%10 == 6 || i%10 == 9) {
				System.out.print("-"+ " ");
			}else {
				System.out.print(i + " ");
			}
		}
		
	}
}