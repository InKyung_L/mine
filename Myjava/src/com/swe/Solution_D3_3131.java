package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;

class Solution_D3_3131{
	
	public static void main(String args[]) throws Exception{
		Scanner sc = new Scanner(System.in);
		int T = 1000000;
		int cnt=0;
		
		for(int i=2;i<=T;i++) {
			boolean isPrime = true;
			for(int j=2;j<=Math.sqrt(i);j++) {
				if(i%j == 0) {
					isPrime=false;
					break;
				}
			}
			if(isPrime) {
				System.out.print(i + " ");
			}
		}
		
		
	}
}