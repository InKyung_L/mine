package com.swe;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

class Solution_D3_6853{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_6853.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();	
		for(int tc=1;tc<=T;tc++) {
			int[] rect = new int[4];
			for(int i=0;i<4;i++) {
				rect[i] = sc.nextInt();
			}
			int in=0, on=0, out=0;
			
			int num=sc.nextInt();
			for(int i=0;i<num;i++) {
				int x = sc.nextInt();
				int y = sc.nextInt();
				if(x>=rect[0] && x<=rect[2] && y>=rect[1] && y<=rect[3]) {
					if(x>rect[0] && x<rect[2] && y>rect[1] && y<rect[3]) in++;
					else on++;
				}else out++;

			}
			System.out.println("#" + tc + " " + in + " " + on + " " + out);
		}
		
	}
}