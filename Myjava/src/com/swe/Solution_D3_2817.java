package com.swe;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

class Solution_D3_2817{
	public static int N,K,n,r,cnt,ans,a[],v[],arr[];
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_2817.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();	
		for(int tc=1;tc<=T;tc++) {
			N = sc.nextInt();
			K = sc.nextInt();
			arr = new int[N];
			
			for(int i=0;i<N;i++) {
				arr[i] = sc.nextInt();
			}
			

			for(int i=1;i<=N;i++) {
				n=N;
				r=i;
				v = new int[N];
				a = new int[r];
				permcomb(0,0);
			}
			//System.out.println(cnt);
			System.out.println("#" + tc + " " + ans);
			ans=0;
		}
	}

	private static void permcomb(int start, int count) {
		if(count == r) {
			cnt++;
			int sum=0;
			for(int i=0;i<r;i++) {
				sum+=a[i];
			}
			if(sum==K) ans++;
			//System.out.println(Arrays.toString(a) + " " + sum);
			return;
		}

		
		for(int i=start;i<N;i++) {			
			if(v[i]==0) {
				v[i] = 1;
				a[count] = arr[i];
				permcomb(i, count+1);
				v[i] = 0;
			}
		}

	}
}