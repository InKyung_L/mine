package com.swe;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

class Solution_D3_7272{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_7272.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();	
		for(int tc=1;tc<=T;tc++) {
			String A = sc.next();
			String B = sc.next();
			int ans=0;
			
			if(A.length() != B.length()) ans=1;
			else {
				for(int i=0;i<A.length();i++) { //ADOPQR
					if(ans==1) break;
					switch (A.charAt(i)) {
					case 'A':
						if(B.charAt(i)=='A' || B.charAt(i)=='D' || B.charAt(i)=='O' || B.charAt(i)=='P'|| B.charAt(i)=='Q' || B.charAt(i)=='R') {
							ans=0;
						}else ans=1;
						break;
					case 'D':
						if(B.charAt(i)=='A' || B.charAt(i)=='D' || B.charAt(i)=='O' || B.charAt(i)=='P'|| B.charAt(i)=='Q' || B.charAt(i)=='R') {
							ans=0;
						}else ans=1;
						break;
					case 'O':
						if(B.charAt(i)=='A' || B.charAt(i)=='D' || B.charAt(i)=='O' || B.charAt(i)=='P'|| B.charAt(i)=='Q' || B.charAt(i)=='R') {
							ans=0;
						}else ans=1;
						break;
					case 'P':
						if(B.charAt(i)=='A' || B.charAt(i)=='D' || B.charAt(i)=='O' || B.charAt(i)=='P'|| B.charAt(i)=='Q' || B.charAt(i)=='R') {
							ans=0;
						}else ans=1;
						break;
					case 'Q':
						if(B.charAt(i)=='A' || B.charAt(i)=='D' || B.charAt(i)=='O' || B.charAt(i)=='P'|| B.charAt(i)=='Q' || B.charAt(i)=='R') {
							ans=0;
						}else ans=1;
						break;
					case 'R':
						if(B.charAt(i)=='A' || B.charAt(i)=='D' || B.charAt(i)=='O' || B.charAt(i)=='P'|| B.charAt(i)=='Q' || B.charAt(i)=='R') {
							ans=0;
						}else ans=1;
						break;
					case 'B':
						if(B.charAt(i)!='B') ans=1;
						break;
					default:
						if(B.charAt(i)=='A' || B.charAt(i)=='D' || B.charAt(i)=='O' || B.charAt(i)=='P'|| B.charAt(i)=='Q' || B.charAt(i)=='R') ans=1;
						if(B.charAt(i)=='B') {
							if(A.charAt(i)!='B') 
							ans=1;
						}
						break;
						
					}
				}	
			}
			System.out.print("#" + tc + " ");
			if(ans==0) System.out.println("SAME");
			else System.out.println("DIFF");
		}
	}
}