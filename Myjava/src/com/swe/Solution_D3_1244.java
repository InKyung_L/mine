package com.swe;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

class Solution_D3_1244{
	public static int n, max;
	public static char[] num;
	
	public static void swap(int i, int j) {
		char T=num[i]; num[i]=num[j];num[j]=T;
	}
	
	public static void dfs(int count, int cnt) {
		if(cnt==n) {
			max=Math.max(max, Integer.parseInt(String.valueOf(num)));
			return;
		}
		for(int i=count;i<num.length;i++) {
			for(int j=i+1;j<num.length;j++) {
				if(num[i]<=num[j]) { //이거 없어도 돌아가지만 시간을 줄이려면 얘를 사용할 것.
					swap(i,j);
					dfs(i,cnt+1);
					swap(i,j);
				}
				
			}
		}
	}
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1244.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();	
		for(int tc=1;tc<=T;tc++) {
			num = sc.next().toCharArray();
			
		}
		
	}
}