package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D2_1984{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d2_1984.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int[] num = new int[10];
			
			for(int i=0;i<10;i++) {
				num[i] = sc.nextInt();
			}
			double sum = 0;
			Arrays.sort(num);
			for(int j=1;j<=8;j++) {
				sum += num[j];
			}
			
			System.out.println("#" + tc + " " + Math.round(sum/8));
		}
		
	}
}