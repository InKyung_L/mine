package com.swe;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

class Solution_D3_6808{
	public static int cnt,a[],v[],gyu[],in[],win,lose;
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_6808.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();	
		for(int tc=1;tc<=T;tc++) {
			gyu = new int[9];
			in = new int[9];
			boolean[] ck = new boolean[19];
			
			for(int i=0;i<9;i++) {
				gyu[i] = sc.nextInt();
				ck[gyu[i]] = true;
			}
			int j=0;
			for(int i=0;i<=18;i++) {
				if(i!=0 && ck[i]==false)
					in[j++] = i;
			}

			win=0;lose=0;
			a=new int[9];
			v=new int[9];
			permcomb(0,0);
			//System.out.println(cnt);
			System.out.println("#"+tc+" "+win+" "+lose);
		}
		
	}
	private static void permcomb(int start, int count) {
		if(count==9) {
			//cnt++;
			//System.out.println(Arrays.toString(a));

			int insum=0, gyusum=0;
			for(int i=0;i<9;i++) {
				if(a[i] > gyu[i]) insum+=(a[i] + gyu[i]);
				else gyusum+=(a[i] + gyu[i]);
			}
			if(insum>gyusum) lose++;
			else if(insum==gyusum) {}
			else win++;
			
			return;
		}
		for(int i=0;i<9;i++) {
			if(v[i]==0) {
				v[i]=1;
				a[count] = in[i];
				permcomb(i,count+1);
				v[i]=0;
			}
		}
		
	}
}