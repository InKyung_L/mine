package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D3_1215{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1215.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int tc=1;tc<=10;tc++) {
			int q=0;
			int len = sc.nextInt();
			int er = len;
			char[][] puzzle = new char[8][8];
			int ans=0, cnt=0;
			for(int i=0;i<8;i++) {
				String line = sc.next();
				for(int j=0;j<8;j++) {
					puzzle[i][j] = line.charAt(j);
				}
			}			
			
			q=0;
			while(q<8) {
				for(int p=0;p<=8-len;p++) {
					er = len;
					int el = 0;
					for(int l=0;l<(int)Math.ceil(len/2.0);l++) {
						if(puzzle[q][p+el] == puzzle[q][p+er-1]) {
							cnt++;
							er--;
							el++;
						}
						
					}
					if(cnt == (int)Math.ceil(len/2.0)) {
						ans++;
					}
					cnt=0;
				}
				q++;
			}
			
			q=0;
			while(q<8) {
				for(int p=0;p<=8-len;p++) {
					er = len;
					int el = 0;
					for(int l=0;l<(int)Math.ceil(len/2.0);l++) {
						if(puzzle[p+el][q] == puzzle[p+er-1][q]) {
							cnt++;
							er--;
							el++;
						}
						
					}
					if(cnt == (int)Math.ceil(len/2.0)) {
						ans++;
					}
					cnt=0;
				}
				q++;
			}
			
			System.out.println("#" + tc + " " + ans);
		}
			
		
	}
}