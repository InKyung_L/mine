package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;

class Solution_D3_5986{
	public static int a[],num,sums[];
	public static boolean b[];
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_5986.txt"));
		Scanner sc = new Scanner(System.in);

		b = new boolean[1000];
		b[0] = true;
		b[1] = true;
		for(int i=2;i<b.length;i++) {
			if(!b[i]) {
				for(int j=i*2;j<b.length;j+=i) {
					b[j]=true;
				}
			}
		}
		a=new int[3];
		sums = new int[3000];
		permcomb(0,0,0);
		
		int T = sc.nextInt();		
		for(int tc=1;tc<=T;tc++) {
			num = sc.nextInt();				
			System.out.println("#" + tc + " " + sums[num]);
		}
		
	}
	
	private static void permcomb(int start, int count, int sum) {
		if(count==3) {
			sums[sum]++;
			return;
		}
		
		for(int i=start;i<1000;i++) {
			if(b[i]==false) {
				a[count]=i;
				permcomb(i,count+1, sum+i);
			}
		
		}
	}
}