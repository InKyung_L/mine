package com.swe;
import java.io.FileInputStream;
import java.util.Scanner;

class Solution_D3_7510{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_7510.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int num = sc.nextInt();
			int sum=0, cnt=0, n=1, r=1;
			
			while(r<=num) {
				for(int i=n;i<=num;i++) {
					sum+=i;
					if(sum == num) {
						cnt++;
						sum=0;
						n=++r;
						break;
					}
					n++;
					if(sum>num) {
						n=++r; sum=0;
						break;
					}
				}
			}
			System.out.println("#" + tc + " " + cnt);
		}		
	}
}