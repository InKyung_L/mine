package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D2_1983{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d2_1983.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int stud = sc.nextInt();
			int studg = sc.nextInt();
			double ans=0.0;
			String[] grade = {"-", "A+", "A0", "A-", "B+", "B0", "B-", "C+", "C0", "C-", "D0"};
			double bal = stud/10.0;
			double[] sum = new double[stud+1]; //정렬
			double[] nsum = new double[stud+1]; //원본
			double[] resum = new double[stud+1]; //내림차순
			
			for(int i=1;i<=stud;i++) {
				double mid = sc.nextInt();
				double fin = sc.nextInt();
				double ass = sc.nextInt();
				
				sum[i] = mid*0.35 + fin*0.45 + ass*0.2;
			}
			for(int i=1;i<=stud;i++) {
				nsum[i] = sum[i];
			}
			Arrays.sort(sum);
			int v=stud;
			for(int z=1;z<=stud;z++) {
				resum[z] = sum[v];
				v--;
			}
			
			for(int k=1;k<=stud;k++) {
				if(nsum[studg] == resum[k])
					ans=k;
			}


			
			System.out.print("#" + tc + " ");
			if(bal == 1) {
				System.out.println(grade[(int) ans]);
			}else {
				int rans=0;
				rans = (int) Math.ceil(ans/bal);
				System.out.println(grade[rans]);
			}
		}
			
		
	}
}