package com.swe;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

class Solution_D3_4789{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_4789.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();	
		for(int tc=1;tc<=T;tc++) {
			String num = sc.next();
			int cnt=0,sum=0,ans=0;
			for(int i=0;i<num.length();i++) {
				if(num.charAt(i) != '0' && sum <= i) {
					ans += (i-sum);
					sum += ans;
				}
				sum += (num.charAt(i)-'0');
				
			}
			System.out.println("#" + tc + " " +ans);
		}
		
	}
}