package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D3_3431{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_3431.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int L = sc.nextInt();
			int U = sc.nextInt();
			int X = sc.nextInt();
			int ans=0;
			
			if(X>U) {
				ans = -1;
			}else if(X<L) {
				ans = L-X;
			}else {
				ans=0;
			}
			
			System.out.println("#" + tc + " " + ans);
		}
		
	}
}