package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D3_1289{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1289.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int cnt=0;
			String num = sc.next();
			char[] arr = new char[num.length()];
			char[] ori = new char[num.length()];
			Arrays.fill(ori, '0');
			
			for(int i=0;i<num.length();i++) {
				arr[i] = num.charAt(i);
			}
			
			
			for(int i=0;i<num.length();i++) {
				if(arr[i] != ori[i]) {
					for(int j=i;j<num.length();j++) {
						ori[j] = arr[i];
					}
					cnt++;
				}
			}
			System.out.println("#" + tc + " " + cnt);
		}
		
	}
}