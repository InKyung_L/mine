package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D2_1859{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d2_1859.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int day = sc.nextInt();
			long[] sell = new long[day];
			long max=day-1;
			long cnt=0, sum=0;
			long ben=0;
			
			for(int i=0;i<day;i++) {
				sell[i] = sc.nextInt();
			}
			
			for(int j=(int) (max-1);j>=0;j--) {
				if(sell[(int) max] < sell[j]) {
					ben += cnt*sell[(int) max] - sum;
					max = j;
					cnt=0;	
					sum=0;
				}else {
					sum += sell[j];
					cnt++;
				}		
			}
			ben += cnt*sell[(int) max] - sum;
			
			if(cnt==0) {
				for(int i=0;i<=max-1;i++) {
					ben += sell[i];
				}
				ben = sell[(int) max]*(max)-ben;
			}
			
			System.out.println("#" + tc + " " + ben);
		}
		
	}
}