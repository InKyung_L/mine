package com.swe;
import java.util.Arrays;
import java.util.Scanner;

import javax.swing.plaf.synth.SynthSeparatorUI;

import java.io.FileInputStream;


class Solution_D2_2005{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d2_2005.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int n = sc.nextInt();
			int[][] arr = new int[n][n];
			
			arr[0][0]=1;
			if(n>1) {
				for(int i=1;i<n;i++) {
					arr[n-i][0] = 1;
					arr[n-i][n-i] = 1;
				}
				
				for(int p=2;p<n;p++) {
					for(int q=1;q<n;q++) {
						arr[p][q] = arr[p-1][q] + arr[p-1][q-1];
					}
				}
			}
			
			System.out.println("#" + tc);
			for(int a=0;a<n;a++) {
				for(int b=0;b<n;b++) {
					if(arr[a][b] == 0) {
						System.out.print("");
					}
					else System.out.print(arr[a][b] + " ");
				}
				System.out.println();
			}
			
		}
		
	}
}