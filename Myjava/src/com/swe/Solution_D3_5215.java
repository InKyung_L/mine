package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;

class Solution_D3_5215{
	public static int ham,cal,n,r,cnt,a[][],b[],v[],arr[][],max;
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_5215.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			ham = sc.nextInt();
			cal = sc.nextInt();
			arr = new int[ham][2];
			
			for(int i=0;i<ham;i++) {
				for(int j=0;j<2;j++) {
					arr[i][j] = sc.nextInt();
				}
			}
			
			for(r=1;r<=ham;r++) {
				a = new int[r][2];
				v = new int[ham];
				permcomb(0,0);
				
			}System.out.println("#" + tc + " " + max);
		}
		
	}
	private static void permcomb(int start, int count) {
		if(count == r) {
			cnt++;
			int sc_sum=0;
			int cal_sum=0;
			for(int i=0;i<r;i++) {
				sc_sum+=a[i][0];
				cal_sum+=a[i][1];
			}
			
			if(cal_sum<=cal) {
				max = Math.max(max, sc_sum);
			}
			
			//System.out.println(sc_sum + " " + cal_sum);
			
			return;
		}
		
		for(int i=start;i<ham;i++) {
			if(v[i]==0) {
				v[i]=1;
				a[count][0] = arr[i][0];
				a[count][1] = arr[i][1];
				permcomb(i,count+1);
				v[i]=0;
			}
		}
	}
}