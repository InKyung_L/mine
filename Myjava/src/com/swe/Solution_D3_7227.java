package com.swe;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

class Solution_D3_7227{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_7227.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();	
		for(int tc=1;tc<=T;tc++) {
			int w = sc.nextInt();
			int[][] wt = new int[w][2];
			
			for(int i=0;i<w;i++) {
				wt[i][0] = Math.abs(sc.nextInt());
				wt[i][1] = Math.abs(sc.nextInt());
			}
		}
		
	}
}