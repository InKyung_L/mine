package com.swe;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

class Solution_D3_3975{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_3975.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1;tc<=T;tc++) {
			String[] s = br.readLine().split(" ");	
			
			double as = Double.parseDouble(s[0])/Double.parseDouble(s[1]);
			double bs = Double.parseDouble(s[2])/Double.parseDouble(s[3]);
			
			System.out.print("#" + tc + " ");
			if(as>=bs) {
				if(as==bs) System.out.println("DRAW");
				else System.out.println("ALICE");
			}
			else System.out.println("BOB");
		}
		
	}
}