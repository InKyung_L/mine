package com.swe;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;

class Solution_D3_1491{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1491.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int N = sc.nextInt();
			int A = sc.nextInt();
			int B = sc.nextInt();
			
			long val, min = Integer.MAX_VALUE;
			
			for(long C=1;C<=N/2;C++) {
				for(long R=C;C*R<=N;R++) {
					val = A*(R-C) + B*(N-R*C);
					min = Math.min(val, min);
				}
			}
			System.out.println("#" + tc + " " + min);
		}
	
	}
}