package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D3_1220{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1220.txt"));
		Scanner sc = new Scanner(System.in);
		
		//�� N�� �� S��.  1�� N�� 2�� S��
		for(int tc=1;tc<=10;tc++) {
			int big = sc.nextInt();
			int[][] arr = new int [big][big];
			int num=0,cnt=0;
			
			for(int i=0;i<big;i++) {
				for(int j=0;j<big;j++) {
					arr[i][j] = sc.nextInt();
				}
			}
			for(int i=0;i<big;i++) {
				num=0;
				for(int j=0;j<big;j++) {
					if(arr[j][i] == 1) {
						num=1;
					}if(num!=0 && arr[j][i] == 2) {
						num=0;
						cnt++;
					}
				}
			}
			System.out.println("#" + tc + " " + cnt);
		}
		
	}
}