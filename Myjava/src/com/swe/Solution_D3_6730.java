package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D3_6730{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_6730.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int cases = sc.nextInt();
			int[] arr = new int[cases];
			int min = 0;
			int max = 0;
			
			for(int i=0;i<cases;i++) {
				arr[i] = sc.nextInt();
			}
			
			for(int i=0;i<cases-1;i++) {
				if(arr[i] < arr[i+1]) { //오르막
					int sub = arr[i+1]-arr[i];
					max = Math.max(sub, max);
				}else { //내리막
					int sub = arr[i]-arr[i+1];
					min = Math.max(min, sub);
				}
			}
			System.out.println("#" + tc + " " + max + " " + min);
		}
		
	}
}