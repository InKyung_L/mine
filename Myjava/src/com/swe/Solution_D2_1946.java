package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D2_1946{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d2_1946.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			System.out.println("#" + tc);
			int num = sc.nextInt();
			int ten=0;
			
			for(int i=0;i<num;i++) {
				String alph = sc.next();
				int cnt = sc.nextInt();
				
				
				for(int j=0;j<cnt;j++) {
					System.out.print(alph);
					ten++;
					if(ten==10) {
						ten=0;
						System.out.println();
					}
				}
			}
			System.out.println();
			
			
		}
		
	}
}