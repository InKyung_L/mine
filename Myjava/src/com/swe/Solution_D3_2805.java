package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;

class Solution_D3_2805{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_2805.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int N = sc.nextInt();
			int cen = (int)Math.ceil(N/2);
			int sum = 0;
			int end = cen;
			int[][] arr = new int[N][N];
			
			for(int i=0;i<N;i++) {
				String str = sc.next();			
				for(int j=0;j<N;j++) {
					arr[i][j] = str.charAt(j)-'0';
				}
			}
			
			for(int i=0;i<N;i++) {
				if(i<(int)Math.ceil(N/2)) {
					for(int j=0;j<N;j++) {
						if(j == cen) {
							for(int k=cen;k<=end;k++) {
								sum += arr[i][k];
							}
							cen--; end++; break;
						}						
					}
				}else {
					for(int j=0;j<N;j++) {
						if(j == cen) {
							for(int k=cen;k<=end;k++) {
								sum += arr[i][k];
							}
							cen++; end--; break;					
						}	
					}
				}
			}			
			System.out.println("#" + tc + " " + sum);
		}		
	}
}