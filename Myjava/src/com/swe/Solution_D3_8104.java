package com.swe;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

class Solution_D3_8104{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_8104.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int N = sc.nextInt();
			int K = sc.nextInt();
			int[] score = new int[K];
			int dir=1;
			
			int df=0;
			for(int i=0;i<N*K;i+=K) {
				if(dir==1) {
					for(int s=0;s<K;s++) {
						score[df+s] += (i+s+1);
					}	
					dir*=-1;
				}else {
					int p=0;
					for(int s=K-1;s>=0;s--) {
						score[df+s] += (i+1+p);
						p++;
					}	
					dir*=-1;
				}
							
			}
			System.out.print("#" + tc + " ");
			for(int i=0;i<K;i++) {
				System.out.print(score[i] + " ");
			}
			System.out.println();
		}
		
	}
}