package com.swe;
import java.io.FileInputStream;
import java.util.Scanner;

class Solution_D3_4299{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_4299.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int D = sc.nextInt();
			int H = sc.nextInt();
			int M = sc.nextInt();
			int pp = 11*1440 + 11*60 + 11;
			int dd = D*1440 + H*60 + M;	
			int ans=0;		

			if(dd-pp < 0) {
				ans = -1;
			}else {
				ans = dd-pp;
			}
			
			System.out.println("#" + tc + " " + ans);
		}		
	}
}