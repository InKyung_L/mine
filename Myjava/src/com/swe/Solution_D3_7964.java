package com.swe;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D3_7964{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_7964.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int city = sc.nextInt();
			int wall = sc.nextInt();
			int cnt=0, ans=0;
			int[] arr = new int[city];
			boolean flag = false;
			
			for(int i=0;i<city;i++) {
				arr[i] = sc.nextInt();
			}
			
			for(int i=0;i<city;i++) {
				flag = false;
				if(arr[i] == 0) {
					flag = true;
					cnt++;
				}else if(arr[i] == 1) {
					ans += cnt/wall;
					cnt=0;
				}
			}
			if(flag == true) ans += cnt/wall;
			System.out.println("#" + tc + " " + ans);
		}
		
	}
}