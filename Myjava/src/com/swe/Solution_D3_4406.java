package com.swe;
import java.io.FileInputStream;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;


class Solution_D3_4406{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_4406.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			String word = sc.next();
			Queue<Character> q = new LinkedList<>();
			
			for(int i=0;i<word.length();i++) {
				char temp = word.charAt(i);
				if(temp != 'a' && temp != 'e' && temp != 'i' && temp != 'o' && temp != 'u') {
					q.add(word.charAt(i));
				}			
			}
			
			System.out.print("#" + tc + " ");
			int fin = q.size();
			for(int i=0;i<fin;i++) {
				System.out.print(q.peek());
				q.poll();
			}
			System.out.println();
		}
		
	}
}