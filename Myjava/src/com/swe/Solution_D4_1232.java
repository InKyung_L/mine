package com.swe;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Stack;
import java.util.StringTokenizer;

class makeTree{
	String data;
	int left, right;

	public makeTree(String data) {
		this.data = data;
	}

	public makeTree(String data, int left) {
		this.data = data;
		this.left = left;
	}

	makeTree(String data, int left, int right) {
		this.data = data;
		this.left = left;
		this.right = right;
		
	}
}


class Solution_D4_1232{
	
	public static ArrayList<makeTree> arr;
	public static int T;
	public static Stack<Integer> stack = new Stack<>();
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_1232.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		
				
		for(int tc=1;tc<=10;tc++) {
			int T = Integer.parseInt(br.readLine());
			arr = new ArrayList<>();
			for(int i=0;i<T;i++) {
				StringTokenizer st = new StringTokenizer(br.readLine());
				st.nextToken();
				if(st.countTokens()==1) {
					arr.add(new makeTree(st.nextToken()));
				}else if(st.countTokens()==2) {
					arr.add(new makeTree(st.nextToken(), Integer.parseInt(st.nextToken())));
				}else {
					arr.add(new makeTree(st.nextToken(), Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken())));
				}
			}
			postorder(1);
			System.out.println("#" + tc + " " + stack.pop());
		}			
	}
	
	public static void postorder(int n) {
		if(n != 0) {
			postorder(arr.get(n-1).left);
			postorder(arr.get(n-1).right);
			//System.out.print(arr.get(n-1).data + " ");
			
			calc(arr.get(n-1).data);
		}
	}
	
	public static void calc(String data) {
		if(Character.isDigit(data.charAt(0))) stack.push(Integer.parseInt(data));
		else {
			int left = stack.pop();
			int right = stack.pop();
			int nn=0;
			
			switch(data.charAt(0)) {
			case '+': nn= right+left; break;
			case '-': nn= right-left; break;
			case '*': nn= right*left; break;
			case '/': nn= right/left; break;
			}
			
			stack.push(nn);
		}
		
		
	}
}