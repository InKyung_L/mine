package com.swe;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

class Solution_D3_7985{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_7985.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();	
		for(int tc=1;tc<=T;tc++) {
			int a = sc.nextInt();
			int size = (int)Math.pow(2, a)-1;
			int[] tree = new int[size];
			
			for(int i=0;i<size;i++) {
				tree[i] = sc.nextInt();
			}
			
			
		}
		
	}
}