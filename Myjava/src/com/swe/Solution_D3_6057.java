package com.swe;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

class Solution_D3_6057{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_6057.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();	
		for(int tc=1;tc<=T;tc++) {
			int N = sc.nextInt();
			int M = sc.nextInt();
			int[][] tri = new int[51][51];
			int cnt=0;
			
			for(int i=1;i<=M;i++) {
				int a = sc.nextInt();
				int b = sc.nextInt();
				tri[a][b] = 1;
				tri[b][a] = 1;
			}
			
			for(int i=1;i<=N;i++) {
				for(int j=i+1;j<=N;j++) {
					if(tri[j][i]==0)continue;
					for(int k=j+1;k<=N;k++) {
						if(tri[k][i]==0)continue;
						if(tri[k][j]==0)continue;
						cnt++;
					}
				}
			}
			System.out.println("#" + tc + " " + cnt);
		}
		
	}
}