package com.swe;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;


class Solution_D3_6958{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_6958.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1;tc<=T;tc++) {
			String s = br.readLine();
			StringTokenizer st = new StringTokenizer(s);
			String temp[] = s.split(" ");
			int per = Integer.parseInt(temp[0]);
			int prob = Integer.parseInt(temp[1]);
			int[] sum = new int[per];
			int max=0, fst=0;
			
			for(int i=0;i<per;i++) {
				String s1 = br.readLine();
				StringTokenizer st1 = new StringTokenizer(s1);
				String temp1[] = s1.split(" ");
				for(int j=0;j<prob;j++) {
					sum[i] += Integer.parseInt(temp1[j]);
				}
				max = Math.max(max, sum[i]);
			}
			
			for(int i=0;i<per;i++) {
				if(sum[i] == max) fst++;
			}

			System.out.println("#" + tc + " " + fst + " " + max);
		}
		
	}
}