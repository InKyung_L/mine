package com.swe;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D3_7728{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_7728.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			String num = sc.next();
			LinkedList<Character> ll = new LinkedList<>();
			int cnt=0;
			
			for(int i=0;i<num.length();i++) {
				ll.add(i, num.charAt(i));
			}
			for(int j=0;j<ll.size()-1;j++) {
				for(int i=j+1;i<ll.size();i++) {
					if(ll.get(j) == ll.get(i)) {
						ll.remove(i);
						i--;
					}
				}
			}
			
			System.out.println("#" + tc + " " + ll.size());
		}
		
	}
}