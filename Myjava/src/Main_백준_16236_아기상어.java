import java.awt.Point;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_백준_16236_아기상어 {
	public static int si,sj, shark, N, cnt, dep, ans, ck;
	public static int[][] sea, visit;
	public static int[] di= {-1,1,0,0}, dj= {0,0,-1,1};
	public static Queue<Point> q; 
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		N = Integer.parseInt(br.readLine());
		sea = new int[N][N];				
		for(int i=0;i<N;i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int j=0;j<N;j++) {
				sea[i][j] = Integer.parseInt(st.nextToken());
				if(sea[i][j]==9) {
					si=i; sj=j;
				}
			}
		}
		shark=2;
		visit = new int[N][N];
		q = new LinkedList<>();
		bfs(si,sj);
		
		System.out.println(ans);
	}
	
	private static void bfs(int x, int y) {
		visit = new int[N][N];
		visit[x][y]=1;
		q.add(new Point(x,y));
		int ni=0, nj=0;
		int fishck=0, fishi=0, fishj=0;
		
		while(!q.isEmpty()) {
			int i = q.peek().x;
			int j = q.poll().y;
			
			for(int d=0;d<di.length;d++) {
				ni = i+di[d];
				nj = j+dj[d];
				if(ni>=0 && ni<N && nj>=0 && nj<N && visit[ni][nj]==0 && shark>=sea[ni][nj]) {
					System.out.println(ni + " " + nj);
					visit[ni][nj]=visit[i][j]+1;
					q.add(new Point(ni,nj));					
					
					if(sea[ni][nj]!=0 && shark>sea[ni][nj]) {
						//depth, ninj 저장
						fishck=visit[ni][nj];
						fishi=ni;
						fishj=nj;
						
					}else if(fishck==visit[ni][nj] && sea[ni][nj]!=0 && shark>sea[ni][nj]){
						//ni,nj 어떤게 더 작은지
						if(fishi > ni) {
							fishi=ni;
							fishj=nj;
						}else if(fishi==ni) {
							if(fishj>nj) {
								fishj=nj;
							}
						}						
					}
					
					//fishck가 visit보다 크면 break clear
					if(fishck!=0 && fishck < visit[ni][nj]) {
						for(int[] w:visit)
						System.out.println(Arrays.toString(w));
						sea[ni][nj]=0;
						ans+=((visit[ni][nj])-1);
						visit = new int[N][N];
						visit[ni][nj]=1;
						ck++;
						q.clear();
						q.add(new Point(ni,nj));
						if(ck==shark) {
							shark++;
							ck=0;
						}
						break;
					}					
				}
			}		
		}	
	}
}