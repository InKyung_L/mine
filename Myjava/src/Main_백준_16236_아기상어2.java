import java.awt.Point;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_백준_16236_아기상어2 {
	public static int si,sj, shark, N, cnt, dep, ans, ck;
	public static int[][] sea, visit;
	public static int[] di= {-1,1,0,0}, dj= {0,0,-1,1};
	public static Queue<Point> q; 
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		N = Integer.parseInt(br.readLine());
		sea = new int[N][N];				
		for(int i=0;i<N;i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int j=0;j<N;j++) {
				sea[i][j] = Integer.parseInt(st.nextToken());
				if(sea[i][j]==9) {
					si=i; sj=j;
				}
			}
		}
		shark=2;
		visit = new int[N][N];
		q = new LinkedList<>();
		while(cnt>=0) {
			cnt=bfs(si,sj);
			if(cnt > 0)
				ans += cnt;
		}		
		System.out.println(ans);
	}
	
	private static int bfs(int x, int y) {		
		visit = new int[N][N];
		if(sea[x][y]==9) sea[x][y]=0;
		visit[x][y]=1;
		q.add(new Point(x,y));
		int ni=0, nj=0;
		int fishck=0, fishi=N, fishj=N; //밖에 있는 범위를 가는 것을 미리 방지하거나 0으로 쓰면 방문체크해줘야하니까
		
	loop:while(!q.isEmpty()) {
			int i = q.peek().x;
			int j = q.poll().y;
			for(int d=0;d<di.length;d++) {
				ni = i+di[d];
				nj = j+dj[d];
				if(ni>=0 && ni<N && nj>=0 && nj<N && visit[ni][nj]==0 && shark>=sea[ni][nj]) {
					System.out.println(ni + " " + nj);
					visit[ni][nj]=visit[i][j]+1;
					q.add(new Point(ni,nj));					
					
					if(fishck==0 && sea[ni][nj]!=0 && shark>sea[ni][nj]) {
						//depth, ninj 저장
						fishck=visit[ni][nj];
						fishi=ni;
						fishj=nj;
						
					}else if(fishck==visit[ni][nj] && sea[ni][nj]!=0 && shark>sea[ni][nj]){
						//ni,nj 어떤게 더 작은지
						if(fishi > ni) { //같은 depth에서 위에 꺼
							fishi=ni;
							fishj=nj;
						}else if(fishi==ni) { //같은 detph 같은 위치라면 왼쪽 꺼
							if(fishj>nj) {
								fishj=nj;
							}
						}						
					}else if(fishck>visit[ni][nj]) {
						break loop;
					}
				}else if(ni>=0 && ni<N && nj>=0 && nj<N && visit[ni][nj]==0 && shark<sea[ni][nj]) {
					visit[ni][nj]=-1;//조건에는 안맞지만 계속 탐색하기를 방지하기 위함.
				}
			}		
		}	
		if(fishi != N && fishj != N) {
			sea[fishi][fishj]=0;
			visit = new int[N][N];
			visit[fishi][fishj]=1;
			ck++;
			q.clear();
			q.add(new Point(fishi,fishj));
			si=fishi; sj=fishj;
			if(ck==shark) {
				shark++;
				ck=0;
			}
		}
		return fishck-1;
	}
}