package D2;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D2_1966 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D2_1966.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int size = sc.nextInt();
			int[] nums = new int[size];
			for(int i=0;i<size;i++) {
				nums[i] = sc.nextInt();
			}
			Arrays.sort(nums);
			System.out.print("#" + tc + " ");
			for(int v:nums) System.out.print(v + " ");
			System.out.println();
		}
	}

}