package D2;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D2_1938 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D2_1938.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int n = sc.nextInt();
			int stdnum = sc.nextInt();
			int[] sum = new int[3];
			int[] grade = new int[n+1];
			
			for(int i=1;i<=n;i++) {
				for(int st=0;st<3;st++) {
					sum[st] = sc.nextInt();
					if(st==0) grade[i] += sum[st]*0.35;
					if(st==1) grade[i] += sum[st]*0.45;
					if(st==2) grade[i] += sum[st]*0.2;
				}
				
			}
			
			for(int i=1;i<=n;i++) {
				System.out.println(grade[i]);
			}
		}
	}
	
}