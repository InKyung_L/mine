package D2;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D2_1926 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D2_1926.txt"));
		Scanner sc = new Scanner(System.in);
		
		
		int n = sc.nextInt();
		String num="";
		
		for(int i=1;i<=n;i++) {
			num = Integer.toString(i);
			int len = num.length();

			if(num.contains("3") || num.contains("6") || num.contains("9")) {
				if(num.length()>1 && (num.charAt(0) == num.charAt(1))) {
					num = num.replace('3', '-');
					num = num.replace('6', '-');
					num = num.replace('9', '-');
				}	
				else num = "-";
			}
			
			System.out.print(num + " ");
		}
		
	}
	
}