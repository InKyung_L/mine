package D2;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D2_1284 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D2_1284.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int asum = 0; int bsum = 0;
			int min=0;
			int P = sc.nextInt();
			int Q = sc.nextInt();
			int R = sc.nextInt();
			int S = sc.nextInt();
			int W = sc.nextInt();
			
			asum = P*W;
			if(W <= R)
				bsum = Q;
			else
				bsum = Q + S*(W-R);
			
			if(asum<bsum)
				min = asum;
			else min = bsum;
			
			System.out.println("#" + tc + " " + min);
			
		}
	}

}