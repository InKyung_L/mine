package D2;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D2_1288 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D2_1288.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int inum;
			int xnum=0;
			String snum;
			char n;
			int x=1;
			int ans = 10;
			int[] sheep = new int[10];
			Arrays.fill(sheep, 1);
			inum = sc.nextInt();
			while(ans>0) {
				xnum = inum*x;
				snum = Integer.toString(xnum);
				x++;
				for(int i=0;i<snum.length();i++) {
					n = snum.charAt(i);
				
					switch (n) {
					case '0':
						if(sheep[0] == 1) {
							sheep[0] = 0;
							ans--;
						}
						break;
						
					case '1':
						if(sheep[1] == 1){
							sheep[1] = 0;
							ans--;
						}
						break;
						
					case '2':
						if(sheep[2] == 1){
							sheep[2] = 0;
							ans--;
						}
						break;
						
					case '3':
						if(sheep[3] == 1){
							sheep[3] = 0;
							ans--;
						}
						break;
					case '4':
						if(sheep[4] == 1){
							sheep[4] = 0;
							ans--;
						}
						break;
						
					case '5':
						if(sheep[5] == 1){
							sheep[5] = 0;
							ans--;
						}
						break;
						
					case '6':
						if(sheep[6] == 1){
							sheep[6] = 0;
							ans--;
						}
						break;
						
					case '7':
						if(sheep[7] == 1){
							sheep[7] = 0;
							ans--;
						}
						break;
						
					case '8':
						if(sheep[8] == 1){
							sheep[8] = 0;
							ans--;
						}
						break;
						
					case '9':
						if(sheep[9] == 1){
							sheep[9] = 0;
							ans--;
						}
						break;

					}
				}
				
			}
			System.out.println("#" + tc + " " + xnum);
		}
	}

}