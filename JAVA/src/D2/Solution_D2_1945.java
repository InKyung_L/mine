package D2;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D2_1945 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D2_1945.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int na=0;
			int ele=0;
			int sev=0;
			int fiv=0;
			int thr=0;
			int two=0;
			int num = sc.nextInt();
			
			while(na <= 0) {
				na = num%11;
				if(num%11 == 0) {
					ele++;
					num = num/11;
					na = num%11;
				}
			}
			na=0;
			while(na <= 0) {
				na = num%7;
				if(num%7 == 0) {
					sev++;
					num = num/7;
					na = num%7;
				}
			}
			na=0;
			while(na <= 0) {
				na = num%5;
				if(num%5 == 0) {
					fiv++;
					num = num/5;
					na = num%5;
				}
			}
			na=0;
			while(na <= 0) {
				na = num%3;
				if(num%3 == 0) {
					thr++;
					num = num/3;
					na = num%3;
				}
			}
			na=0;
			while(na <= 0) {
				na = num%2;
				if(num%2 == 0) {
					two++;
					num = num/2;
					na = num%2;
				}
			}
			na=0;
			System.out.println("#" + tc + " " + two + " " + thr + " " + fiv + " " + sev + " " + ele);
		}
	}

}