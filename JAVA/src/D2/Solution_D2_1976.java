package D2;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D2_1976 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D2_1976.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int hour1 = sc.nextInt();
			int minute1 = sc.nextInt();
			int hour2 = sc.nextInt();
			int minute2 = sc.nextInt();
			int up=0, hans=0, mans=0;
			
			mans = minute1+minute2;
			if(mans > 60) {
				up++;
				mans = mans-60; 
			}
			hans = (hour1 + hour2+up)%12;
			if(hans == 0) hans = 12;
			
			System.out.println("#" + tc + " " + hans + " " + mans);
		}
	}
	
}