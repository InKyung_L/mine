package D2;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D2_1979 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D2_1979.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int N = sc.nextInt();
			int block = sc.nextInt();
			int[][] puzzle = new int[N][N];
			int ans=0;
			
			for(int i=0;i<N;i++) { //ä���
				for(int j=0;j<N;j++) {
					puzzle[i][j] = sc.nextInt();
				}
			}
			
			for(int i=0;i<N;i++) {
				int cnt=0;
				for(int j=0;j<N;j++) {
					if(puzzle[i][j] == 1) {
						cnt++;
						if(cnt == block) {
							if(j == N-1) ans++;
							else if(j<N-1 && puzzle[i][j+1] == 0) {
								ans++;
							}
						}
					}
					else
						cnt=0;
				}
			}
			
			for(int j=0;j<N;j++) {
				int cnt=0;
				for(int i=0;i<N;i++) {
					if(puzzle[i][j] == 1) {
						cnt++;
						if(cnt == block) {
							if(i == N-1) ans++;
							else if(i<N-1 && (puzzle[i+1][j] == 0)) {
								ans++;
							}
						}
					}
					else
						cnt=0;
				}
			}
			
			System.out.println("#" + tc + " " + ans);
		}
	}

}