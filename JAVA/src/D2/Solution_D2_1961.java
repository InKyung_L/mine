package D2;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D2_1961 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D2_1961.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int N = sc.nextInt();
			int[][] origin = new int[N][N];
			int[][] rotate9 = new int[N][N];
			int[][] rotate18 = new int[N][N];
			int[][] rotate27 = new int[N][N];
			
			for(int i=0;i<N;i++) {
				for(int j=0;j<N;j++) {
					origin[i][j] = sc.nextInt();
				}
			}
			
			for(int i=0;i<N;i++) {
				for(int j=0;j<N;j++) {
					rotate9[i][j] = origin[N-j-1][i];
				}
			}
			
			for(int i=0;i<N;i++) {
				for(int j=0;j<N;j++) {
					rotate18[i][j] = rotate9[N-j-1][i];
				}
			}
			
			for(int i=0;i<N;i++) {
				for(int j=0;j<N;j++) {
					rotate27[i][j] = rotate18[N-j-1][i];
				}
			}
			
			System.out.println("#" + tc);
			for(int i=0;i<N;i++) {
				for(int j=0;j<N;j++) {
					System.out.print(rotate9[i][j]);
				}System.out.print(" ");
				for(int j=0;j<N;j++) {
					System.out.print(rotate18[i][j]);
				}System.out.print(" ");
				for(int j=0;j<N;j++) {
					System.out.print(rotate27[i][j]);
				}
				System.out.println();
			}
		}
	}
	
}