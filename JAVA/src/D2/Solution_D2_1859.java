package D2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;


class Solution_D2_1859{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/D2_1859.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		
		for(int tc=1;tc<=T;tc++) {
			int day = Integer.parseInt(br.readLine());
			long[] sell = new long[day];
			long max=day-1;
			long cnt=0, sum=0, ben=0;
			
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int i=0;i<day;i++) {
				sell[i] = Integer.parseInt(st.nextToken());
			}
			
			for(int j=(int)(max-1);j>=0;j--) {
				if(sell[(int)max] < sell[j]) {
					ben += cnt*sell[(int) max] - sum;
					max = j;
					cnt=0;	
					sum=0;
				}else {
					sum += sell[j];
					cnt++;
				}		
			}
			ben += cnt*sell[(int) max] - sum;
			
			if(cnt==0) {
				for(int i=0;i<=max-1;i++) {
					ben += sell[i];
				}
				ben = sell[(int) max]*(max)-ben;
			}			
			System.out.println("#" + tc + " " + ben);
		}		
	}
}