package D2;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D2_1970 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D2_1970.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int money = sc.nextInt();
			int[] mon = {50000,10000,5000,1000,500,100,50,10};
			int[] ans = new int[8];
			
			for(int i=0;i<8;i++) {
				ans[i] = money/mon[i];
				money = money-(ans[i]*mon[i]);
			}
			
			System.out.println("#" +tc);
			for(int i=0;i<8;i++) {
				System.out.print(ans[i] + " ");
			}
			System.out.println();
		}
	}
	
}