package D2;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D2_1285 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D2_1285.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int n = sc.nextInt();
			int[] arr = new int[n];
			int min=100000;
			int cnt=0;
			
			for(int i=0;i<n;i++) {
				arr[i] = sc.nextInt();
				if(min >= Math.abs(arr[i])) {
					min = Math.abs(arr[i]);
				}
			}
			for(int i=0;i<n;i++) {
				if(min == Math.abs(arr[i])) {
					cnt++;
				}
			}
			System.out.println("#" + tc + " " + min + " " + cnt);
		}
	}
	
}