package D2;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D2_1989 {

	public static void main(String[] args) throws Exception{
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int ans=0;
			String s = sc.next();
			
			StringBuilder re = new StringBuilder(s);
			StringBuilder rev = re.reverse();
			
			if(s.equals(rev.toString()))
				ans=1;
			
			System.out.println("#" + tc + " " + ans);
		}
	}

}