package D2;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D2_2001 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D2_2001.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int N = sc.nextInt();
			int M = sc.nextInt();
			int[][] arr = new int [N][N];
			int max=0;
			
			for(int i=0;i<N;i++) {
				for(int j=0;j<N;j++) {
					arr[i][j] = sc.nextInt();
				}
			}
			
			for(int i=0;i<N-M+1;i++) {
				for(int j=0;j<N-M+1;j++) {
					int sum=0;
					
					for(int p=i;p<M+i;p++) {
						for(int q=j;q<M+j;q++) {
							sum += arr[p][q];
						}
					}
					if(sum > max) max = sum;
					
				}
			}
			System.out.println("#" + tc + " " + max);
		}
	}
	
}