package D2;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D2_1948 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D2_1948.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int fmon = sc.nextInt();
			int fday = sc.nextInt();
			int smon = sc.nextInt();
			int sday = sc.nextInt();
			int ans=0;
			
			
			int[] days = {0,31,28,31,30,31,30,31,31,30,31,30,31};
			
			if(fmon == smon) {
				ans = sday - fday +1;
			}else {
				for(int i=fmon;i<smon;i++) {
					ans += days[i];
				}
				ans += (sday - fday + 1);
			}
			
			System.out.println("#" + tc + " " + ans);
		}
	}

}