package D2;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D2_2005 {

	public static void main(String[] args) throws Exception{
		//System.setIn(new FileInputStream("res/D2_2005.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int n = sc.nextInt();
			int[] arr = new int[n];
			
			arr[0]=1;
			
			for(int v:arr) System.out.println(v);
		}
	}
	
}