package D2;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D2_1974 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D2_1974.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int[][] sdoku = new int[9][9];
			int ans=1;
			
			for(int i=0;i<9;i++) {
				int sum=0;
				for(int j=0;j<9;j++) {
					sdoku[i][j] = sc.nextInt();
					sum += sdoku[i][j];
				}
				if(sum != 45) ans=0;
			}
			
			for(int i=0;i<9;i++) {
				int sum=0;
				for(int j=0;j<9;j++) {
					sum += sdoku[j][i];
				}
				if(sum != 45) ans=0;
			}

			int p=0, q=0;
			for(int i=0;i<9;i++) {
				int sum=0;
				int st=0;
				for(p=0;p<st+3;p++) {
					for(q=0;q<st+3;q++) {
						sum += sdoku[p][q];
					}
				}
				p += 3; q += 3; st += 3;
				if(sum != 45) ans=0;
			}
			
			System.out.println("#" + tc + " " + ans);
		}
	}
	
}