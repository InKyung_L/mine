package D2;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D2_1986 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D2_1986.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int num = sc.nextInt();
			int sum=0;
			for(int i=1;i<=num;i++) {
				if(i%2 == 1)
					sum = sum+i;
				else
					sum = sum-i;
			}		
			System.out.println("#" + tc + " " + sum);
		}
	}

}