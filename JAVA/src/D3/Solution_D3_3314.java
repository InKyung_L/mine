package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Solution_D3_3314 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_3314.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int[] score = new int[5];
			int sum=0;
			
			for(int i=0;i<5;i++) {
				score[i] = sc.nextInt();
				if(score[i] < 40) {
					score[i] = 40;
				}
				sum += score[i];
			}
			System.out.println("#" + tc + " " + sum/5);
		}
	}
}
