package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_5603 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_5603.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int n = sc.nextInt();
			int[] dum = new int[n];
			int cnt=0, min=10000, max=0;
			
			for(int i=0;i<n;i++) {
				dum[i] = sc.nextInt();
				max = Math.max(dum[i], max);
				min = Math.min(dum[i], min);
			}

			boolean flag = true;
			while(flag == true) {
				for(int i=0;i<n;i++) {
					if(max == dum[i]) {
						dum[i] -= 1;
						max = dum[i];
						break;
					}
				}
				for(int i=0;i<n;i++) {
					if(min == dum[i]) {
						dum[i] += 1;
						min = dum[i];
						break;
					}
				}
				cnt++;
				for(int i=0;i<n;i++) {
					max = Math.max(dum[i], max);
					min = Math.min(dum[i], min);
				}
				if(max == min) flag = false;
			}
			
			
			System.out.println("#" + tc + " " + cnt);
		}
	}
}
