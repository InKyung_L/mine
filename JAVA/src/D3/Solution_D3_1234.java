package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_1234 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_1234.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int tc=1;tc<=10;tc++) {
			int length = sc.nextInt();
			String num = sc.next();
			LinkedList<Character> ll = new LinkedList<>();
			
			for(int i=0;i<num.length();i++) {
				ll.add(num.charAt(i));
			}
			
			for(int i=0;i<ll.size()-1;i++) {
				if(ll.get(i) == ll.get(i+1)) {
					ll.remove(i+1);
					ll.remove(i);
					i--;
					i--;
					if(i<=0) i=-1;
				}
				
			}
			int end = ll.size();
			System.out.print("#" + tc + " ");
			for(int i=0;i<end;i++) {
				System.out.print(ll.peek());
				ll.poll();
				
			}
			System.out.println();
		}
	}
}
