package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_4676 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_4676.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			String word = sc.next();
			char[] words = new char[word.length()+1];
			Arrays.fill(words, '-');
			for(int i=1;i<=word.length();i++) {
				words[i] = word.charAt(i-1);
			}
			int hyphen = sc.nextInt();
			int[] times = new int[hyphen];
			for(int i=0;i<hyphen;i++) {
				times[i] = sc.nextInt();
			}
			Arrays.sort(times);
			
			System.out.print("#" + tc + " ");
			for(int i=0;i<=word.length();i++) {
				boolean flag = false; int cnt=0;
				for(int j=0;j<hyphen;j++) {					
					if(times[j] == i) {
						flag=true;					
						//if(cnt==0) System.out.print(words[i+1]);
						System.out.print("-");	
						cnt++;
					}								
				}
				//if(flag == false) 
				if(i==word.length()) {
					System.out.print("");	
				}else System.out.print(words[i+1]);	
			}System.out.println();
		}
	}
}
