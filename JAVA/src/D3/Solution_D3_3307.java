package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_3307 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_3307.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int leng = sc.nextInt();
			int[] arr = new int[leng];
			int cnt=1,max=0;		
			
			for(int i=0;i<leng;i++) {
				arr[i] = sc.nextInt();
			}
			
			for(int i=0;i<leng;i++) {
				int k=i; cnt=1;
				for(int j=k+1;j<leng;j++) {
					if(arr[k]<arr[j]) {
						cnt++;
						k=j;
						continue;
					}
				}			
				max=Math.max(cnt, max);
			}
			System.out.println("#" + tc + " " + max);
		}
	}
}
