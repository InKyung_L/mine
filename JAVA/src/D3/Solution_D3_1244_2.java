package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_1244_2 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_1244.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();	
		for(int tc=1;tc<=T;tc++) {
			String num = sc.next();
			int[] n = new int[num.length()];
			int cnt = sc.nextInt();
			int imax=0, idx=0;
			
			
			for(int i=0;i<num.length();i++) {
				n[i] = num.charAt(i)-'0';
				imax=Math.max(imax, n[i]);
			}
			System.out.println(Arrays.toString(n)+"\n");
			
here:		for(int i=num.length()-1;i>=0;i--) {
				
				if(cnt==1) {
					if(n[num.length()-1]>=n[0]) {
						int tt = n[num.length()-1];
						n[num.length()-1]=n[0];
						n[0]=tt;
						cnt--;
					}
					else if(n[0]!=imax) {
						for(int j=0;j<num.length()-2;j++) {
							if(n[j]<=n[j+1]) {
								idx=j+1;
								break;
							}					
						}
						int tt = n[idx];
						n[idx]=n[0];
						n[0]=tt;
						cnt--;
					}
				}
				if(cnt==0) break;
				int max=i;
				for(int j=i-1;j>=0;j--) {
					if(n[j]<n[max]) {
						max=j;
					}					
				}
				if(max==i) {
					if(cnt!=0) {
					int tp = n[num.length()-1];
					n[num.length()-1]=n[num.length()-2];
					n[num.length()-2]=tp;
				}
					else continue here;
					
				}
				int temp = n[i];
				n[i]=n[max];
				n[max]=temp;
				System.out.println(Arrays.toString(n));
				cnt--;
			}
			System.out.println("\n" +Arrays.toString(n));
		}
		
	}
}