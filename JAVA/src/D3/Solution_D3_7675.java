package D3;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Solution_D3_7675 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_7675.txt"));
		//Scanner sc = new Scanner(System.in);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1;tc<=T;tc++) {
			int sen = Integer.parseInt(br.readLine());
			int[] ans = new int[sen];
			Arrays.fill(ans, 0);
			String[] name = br.readLine().split(" ");
			
			int f=0;
			for(int k=0;k<sen;k++) {
				//f=0;
				for(int i=0+f;i<name.length;i++) {
					boolean c = true;
					if(name[i].charAt(0)<65 || name[i].charAt(0)>90) c=false;
					if(name[i].charAt(0)>=65 && name[i].charAt(0)<=90) {
						if(name[i].charAt(name[i].length()-1)=='.' || name[i].charAt(name[i].length()-1)=='!' || name[i].charAt(name[i].length()-1)=='?') {
							for(int j=1;j<name[i].length()-1;j++) {
								if(name[i].charAt(j)<97 || name[i].charAt(j)>122) {c=false; break;}
							}
						}					
						else {
							for(int j=1;j<name[i].length();j++) {
								if(name[i].charAt(j)<97 || name[i].charAt(j)>122) {c=false; break;}
							}
						}						
					}
					if(c==true) ans[k]++;
					if(name[i].charAt(name[i].length()-1)=='.' || name[i].charAt(name[i].length()-1)=='!' || name[i].charAt(name[i].length()-1)=='?')
						{f=i+1; break;}
				}
			}
			System.out.print("#" + tc + " ");
			for(int i=0;i<sen;i++) {
				System.out.print(ans[i]+" ");
			}System.out.println();
		}
	}
}
