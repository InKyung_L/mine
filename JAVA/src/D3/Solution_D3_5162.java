package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_5162 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_5162.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int A = sc.nextInt();
			int B = sc.nextInt();
			int min = Math.min(A, B);
			int C = sc.nextInt();
			
			System.out.println("#" + tc + " " + C/min);
		}
	}
}
