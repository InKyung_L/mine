package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_4466 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_4466.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int max=0;
			int n = sc.nextInt();
			int r = sc.nextInt();
			int[] d = new int[n];
			
			for(int i=0;i<n;i++) {
				d[i] = sc.nextInt();
			}
		
			Arrays.sort(d);
			
			for(int i=d.length-1;i>d.length-1-r;i--) {
				max += d[i];
			}		
			System.out.println("#" + tc + " " + max);
		}
	}
}
