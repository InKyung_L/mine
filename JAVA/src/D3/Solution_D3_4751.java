package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D3_4751 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_4751.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			String word = sc.next();
			int size =word.length()*4+1;
			char[] arr1 = new char[size];
			char[] arr2 = new char[size];
			char[] arr3 = new char[size];
			
			Arrays.fill(arr1, '.');
			for(int i=2;i<size;i+=4) {
				arr1[i] = '#';
			}
			
			Arrays.fill(arr2, '.');
			for(int i=1;i<size;i+=2) {
				arr2[i] = '#';
			}
			
			Arrays.fill(arr3, '.');
			for(int i=0;i<size;i+=4) {
				arr3[i] = '#';
			}
			int j=0;
			for(int i=2;i<size;i+=4) {
				arr3[i] = word.charAt(j++);
			}
			
			for(int i=0;i<size;i++) {
				System.out.print(arr1[i]);
			}System.out.println();
			for(int i=0;i<size;i++) {
				System.out.print(arr2[i]);
			}System.out.println();
			for(int i=0;i<size;i++) {
				System.out.print(arr3[i]);
			}System.out.println();
			for(int i=0;i<size;i++) {
				System.out.print(arr2[i]);
			}System.out.println();
			for(int i=0;i<size;i++) {
				System.out.print(arr1[i]);
			}
			System.out.println();
		}
	}
}
