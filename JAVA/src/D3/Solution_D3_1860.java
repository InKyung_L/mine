package D3;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Solution_D3_1860 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_1860.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Scanner sc = new Scanner(System.in);
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1;tc<=T;tc++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			int ck=0;
			int N = Integer.parseInt(st.nextToken());
			int M = Integer.parseInt(st.nextToken());
			int K = Integer.parseInt(st.nextToken());		
			
			StringTokenizer st1 = new StringTokenizer(br.readLine());
			int[] customer = new int[N];
			for(int i=0;i<N;i++) {
				customer[i] = Integer.parseInt(st1.nextToken());
			}
			Arrays.sort(customer);
			if(customer[0]<M) ck=0;
			else {
				int bread=0, time=0,j=0;
				for(int i=j;i<N;i++) {
					if(time<customer[i]) {
						bread+=((customer[i]-time)/M)*K;
						time+=((customer[i]-time)/M)*M;
					}
					bread--;
					if(bread<0) break;
				}
				if(bread>=0) {
					ck=1;
				}			
			}
			if(ck==0) System.out.println("#"+ tc + " " + "Impossible");
			else System.out.println("#"+ tc + " " + "Possible");
		}
		
	}
}