package D3;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D3_1213 {
	public static void main(String[] args) throws Exception{
		//System.setIn(new FileInputStream("res/D3_1213.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int tc=1;tc<=1;tc++) {
			int n = sc.nextInt();
			String sample = sc.next();
			String origin = sc.next();
			char[] samp = new char[sample.length()];
			char[] ori = new char[origin.length()];
			int cnt=1, ans=0;
			
			for(int i=0;i<sample.length();i++) {
				samp[i] = sample.charAt(i);
			}
			
			for(int i=0;i<origin.length();i++) {
				ori[i] = origin.charAt(i);
			}
			
			for(int i=0;i<=origin.length()-sample.length();i++) {
				if(ori[i] == samp[0]) {
					for(int j=1;j<sample.length();j++) {
						if(samp[j] == ori[i+j]) cnt++;
					}
					if(cnt == sample.length()) {ans++; cnt=1;}
					cnt=1;
				}
			}
			System.out.println("#" + tc + " " + ans);
		}
	}
}
