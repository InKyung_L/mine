package D3;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

public class Solution_D3_1244_3 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_1244.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();	
		for(int tc=1;tc<=T;tc++) {
			String num = sc.next();
			Integer[] n = new Integer[num.length()];
			int cnt = sc.nextInt();
			int[] index = new int[10];
			Arrays.fill(index, -1);
			ArrayList<Integer> al = new ArrayList<Integer>();
			
			for(int i=0;i<num.length();i++) {
				n[i]=num.charAt(i)-'0';
				if(index[n[i]] != -1) {
					 if(al.isEmpty()) al.add(index[n[i]]); //겹치는 숫자의 인덱스 저장
	                    al.add(i);
				}
				else index[n[i]]=i; //겹치지 않는 숫자와 인덱스 저장
			}
			
			 for(int i=0;i<n.length;i++) { //선택정렬
	                if(cnt==0) break;                
	                int midx = i; //최대값의 인덱스
	                for(int j=i+1;j<n.length;j++) {
	                    if(n[midx]<=n[j]) {
	                    	midx = j;
	                    }
	                }
	                if(midx != i) {
	                    int tmp = n[midx]; //처음 숫자가 max가 아니면 바꾸기
	                    n[midx]=n[i]; 
	                    n[i]=tmp;
	                    cnt --;                    
	                } 
	           }
			 HashSet<Integer> hs = new HashSet<Integer>(Arrays.asList(n)); //중복제거해서저장	            
			 if(hs.size() != n.length) {
            	 Integer[] tmpArr = new Integer[al.size()];
                 
                 for(int i=0;i<al.size();i++) {
                     tmpArr[i] = n[al.get(i)]; //겹치는 숫자들 인덱스로 값 찾아서 임시저장
                 }
                 
                 List<Integer> list = Arrays.asList(tmpArr);
                 Collections.sort(list);
                 Collections.reverse(list);
                 
                 int idx = 0;
                 for(int i : al) {
                     n[i] = tmpArr[idx++];
                 }
             }
	            
            if(cnt%2 == 1 && hs.size() == n.length) {
                int len = n.length-1;
                int tmp = n[len]; //이미 최대로 정렬이 돼있는데 cnt가 남았으면 뒤에 두자리 교환
                n[len]=n[len-1]; 
                n[len-1]=tmp;
            } 
            
            System.out.print("#" + tc + " ");
            for(int i=0;i<n.length;i++) System.out.print(n[i]);
            System.out.println();
	        }	        
	    }
	}