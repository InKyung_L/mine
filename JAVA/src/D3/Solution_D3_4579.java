package D3;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D3_4579 {	
	static char[] word;
	static boolean isExist() {
		int len = word.length;
		for(int i=0; i<len/2; i++) {
			if(word[i] == '*' ) return true;
			if(word[len-1-i] == '*') return true;
			if(word[i] != word[len-1-i] ) return false;
		}
		return true;
	}
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_4579.txt"));
		Scanner sc = new Scanner(System.in);
	
    	int T = sc.nextInt();
    	for(int tc=1; tc<=T; tc++) {
    		word = sc.next().toCharArray();
    		System.out.print("#" + tc + " ");
            if(isExist()==true) System.out.println("Exist");
            else System.out.println("Not exist");
        }
    }
}