package D3;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class Solution_D3_4789 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_4789.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();	
		for(int tc=1;tc<=T;tc++) {
			String num = sc.next();
			int cnt=0,sum=0,ans=0;
			for(int i=0;i<num.length();i++) {
				//sum += num.charAt(i)-'0';	
				if(sum>=i) {
					sum+=num.charAt(i)-'0';	
				}else {
					cnt = i-sum;
					ans += cnt;
					sum = sum + num.charAt(i)-'0' + cnt;	
					//sum += cnt;
				}
			}
			System.out.println("#" + tc + " " +ans);
		}		
	}
}
