package D3;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;
import java.util.StringTokenizer;

public class Solution_D3_7012 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_7012.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1;tc<=T;tc++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			int N = Integer.parseInt(st.nextToken());
			int M = Integer.parseInt(st.nextToken());
			int[] arr = new int[N+M+1];
			int max = Integer.MIN_VALUE;
			
			for(int i=1;i<=N;i++) {
				for(int j=1;j<=M;j++) {
					arr[i+j]++;
					max = Math.max(max, arr[i+j]);
				}
			}
			System.out.print("#" + tc + " ");
			for(int i=2;i<arr.length;i++) {
				if(max==arr[i])
					System.out.print(i + " ");
			}
			System.out.println();
		}
		
	}
}
