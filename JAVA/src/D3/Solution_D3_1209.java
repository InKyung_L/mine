package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_1209 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_1209.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int tc=1;tc<=10;tc++) {
			sc.nextInt();
			int[][] num = new int[100][100];
			int sum1=0, sum2=0, sum3=0, sum4=0, max=0;
			
			for(int i=0;i<100;i++) {
				for(int j=0;j<100;j++) {
					num[i][j] = sc.nextInt();
					sum1 += num[i][j];
				}
				sum3 += num[i][i];
				sum4 += num[i][99-i];
				max = Math.max(max, sum1);
				sum1=0;
			}
			max = Math.max(max, sum3);
			max = Math.max(max, sum4);
			for(int i=0;i<100;i++) {
				for(int j=0;j<100;j++) {
					sum2 += num[j][i];
				}
				max = Math.max(max, sum2);
				sum2=0;
			}
			System.out.println("#" + tc + " " + max);
		}
	}
}
