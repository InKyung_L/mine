package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_1240 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_1240.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int I = sc.nextInt();
			int J = sc.nextInt();
			int[][] arr = new int[I][J];
			String[] ans = {"0001101", "0011001", "0010011", "0111101", "0100011", "0110001", "0101111", "0111011", "0110111", "0001011"};
			String[] pw = new String[56]; //암호저장
			int[] num = new int[9]; //암호를 정수로 변환해서 저장
			int si=0, sj=0;
			int res=0;
			
			for(int i=0;i<I;i++) {
				String line = sc.next();
				for(int j=0;j<J;j++) {
					arr[i][j] = line.charAt(j)-'0';
					if(line.charAt(j)-'0' == 1) {
						si=i; sj=j;
					}
				}
			}
			
			for(int k=55;k>=0;k--) {
				pw[k] = Integer.toString(arr[si][sj--]);
			}
			
			int s = 1; int ti=0;	
			while(ti<56) {		
				String tem = "";
				for(int i=ti;i<ti+7;i++) {
					tem += pw[i];
				}ti+=7;
				for(int p=0;p<=9;p++) {
					if(ans[p].equals(tem)) {
						num[s++] = p;
					}
				}
			}

			int odd=0; int even=0;
			for(int i=0;i<8;i++) {				
				if(i%2 == 0) even += num[i];
				else odd += num[i];							
			}
			int tsum = even+odd*3+num[8];
			if((tsum%10) != 0) res=0;
			else res = even+odd+num[8];
			System.out.println("#" + tc + " " + res);
		}
	}
}
