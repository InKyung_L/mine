package D3;

import java.io.FileInputStream;
import java.math.BigInteger;
import java.util.Scanner;

public class Solution_D3_6718 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_6718.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int ans=0;
			long num = sc.nextLong();
			double nums = num/1000.0;
			
			if(nums<0.1) ans=0;
			else if(nums>=0.1 && nums<1) ans=1;
			else if(nums>=1 && nums<10) ans=2;
			else if(nums>=10 && nums<100) ans=3;
			else if(nums>=100 && nums<1000) ans=4;
			else ans=5;
			
			System.out.println("#" + tc +" " +ans);
			
		}
	}
}
