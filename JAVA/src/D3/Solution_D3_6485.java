package D3;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Solution_D3_6485 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_6485.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int b = sc.nextInt();
			int[] bus = new int[5001];
			
			for(int i=0;i<b;i++) {
				int s = sc.nextInt();
				int e = sc.nextInt();
				
				for(int s1=s;s1<=e;s1++) {
					bus[s1]++;
				}
			}
			
			int bst = sc.nextInt();
			System.out.print("#" + tc + " ");
			for(int i=0;i<bst;i++) {
				int stop = sc.nextInt();
				System.out.print(bus[stop] + " ");
			}System.out.println();
		}
	}
}
