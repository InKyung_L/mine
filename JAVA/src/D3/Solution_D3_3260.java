package D3;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class Solution_D3_3260 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_3260.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			String a = sc.next();
			String b = sc.next();
			int max = Math.max(a.length(), b.length());
			char[] ac = new char[max];
			char[] bc = new char[max];
			Arrays.fill(ac, '0');
			Arrays.fill(bc, '0');
			Stack<Integer> st = new Stack<>();
			
			int size=max-1;
			for(int i=a.length()-1;i>=0;i--) {
				ac[size--]=a.charAt(i);
			}
			size=max-1;
			for(int i=b.length()-1;i>=0;i--) {
				bc[size--]=b.charAt(i);
			}
			int temp=0;
			for(int i=max-1;i>=0;i--) {
				int sum=0;
				sum = (ac[i]-'0')+(bc[i]-'0');
				if(sum>=10) {
					//if(i==max-1) {
						st.add(sum-10+temp);
						temp=1;
					//}
					//else st.add(sum-10+temp);
					//if(i==1) st.add(1);
				}else {
					if(sum+temp>=10) {
						st.add(sum-10+temp);
						temp=1;
					}
					else{
						st.add(sum+temp);
						temp=0;
					}
				}
			}
			if(temp==1) st.add(1);
			System.out.print("#" + tc + " ");
			int end = st.size();
			for(int i=end-1;i>=0;i--) {
				System.out.print(st.pop());
	
				
			}System.out.println();
		}
		
	}
}
