package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_3456 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_3456.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int[] arr = new int[3];
			int ans = 0;
			
			for(int i=0;i<3;i++) {
				arr[i] = sc.nextInt();
			}
			if(arr[0] == arr[1]) ans=arr[2];
			else if(arr[1] == arr[2]) ans=arr[0];
			else if(arr[0] == arr[2]) ans=arr[1];
			
			System.out.println("#" + tc + " " + ans);
		}
	}
}
