package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_4698 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_4698.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			String D = sc.next();
			int A = sc.nextInt();
			int B = sc.nextInt();
			int cnt=0;
			
			//체크 상태를 저장할 배열.
			boolean[] b = new boolean[B+1];
			b[1]=true;
			for(int i=2;i<=B;i++) {
				if(!b[i]) {
					for(int j=i*2;j<b.length;j+=i) {
						b[j]=true;
					}
				}			
			}
			for(int i=A;i<=B;i++) {
				if(!b[i]) {
					String num = Integer.toString(i);
					if(num.contains(D))
						cnt++;
				}
			}
			System.out.println("#" + tc + " " + cnt);
		}
	}
}
