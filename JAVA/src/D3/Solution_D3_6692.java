package D3;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Solution_D3_6692 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_6692.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int size = sc.nextInt();
			double[] num = new double[2];
			double per = 0;
			double sum = 0;
			
			for(int i=0;i<size;i++) {
				num[0] = sc.nextDouble();
				num[1] = sc.nextDouble();
				per = num[0]*12;
				//per = Math.round(num[0]*12*100)/100.0;
				sum += per*num[1];
			}
			double ans = sum/12.0;
			System.out.println("#" + tc + " " + String.format("%.6f", ans));
		}
	}
}
