package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_3142 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_3142.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int horn = sc.nextInt();
			int beast = sc.nextInt();
			int uni=0, twin=0;
			
			twin = horn-beast;
			uni = beast-twin;
			
			System.out.println("#" + tc + " " + uni + " " + twin);
		}
	}
}
