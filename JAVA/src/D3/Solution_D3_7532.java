package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_7532 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_7532.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int S = sc.nextInt();
			int E = sc.nextInt();
			int M = sc.nextInt();
			
			int s = E, m = E;
			int cnt = E;
			
			while(true) {
				if(s == S && m == M) {
					break;
				}
				s += 24;
				if(s>365) s%=365;
				m += 24;
				if(m>29) m%=29;
				cnt += 24;
			}
			
			System.out.println("#" + tc + " " + cnt);
		}
	}
}
