package D3;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Solution_D3_5356 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_5356.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			char[][] dic = new char[5][15];
			for(char[] row:dic) Arrays.fill(row, '-');
			int max=0;
			for(int i=0;i<5;i++) {
				String word = sc.next();
				max = Math.max(max, word.length());
				for(int j=0;j<word.length();j++) {
					dic[i][j] = word.charAt(j);
				}
			}
			
			System.out.print("#" + tc + " ");
			for(int j=0;j<max;j++) {
				for(int i=0;i<5;i++) {
					if(dic[i][j] == '-') {
						System.out.print("");
					}else {
						System.out.print(dic[i][j]);
					}
				}
			}
			System.out.println();
			
		}
	}
}
