package D3;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Solution_D3_7732 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_7732.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1;tc<=T;tc++) {
			String starttime = br.readLine();
			String endtime = br.readLine();
			StringTokenizer st = new StringTokenizer(starttime);
			StringTokenizer et = new StringTokenizer(endtime);
			String[] stime = starttime.split(":");
			int[] stimes = new int[3];
			String[] etime = endtime.split(":");
			int[] etimes = new int[3];
			for(int i=0;i<3;i++) {
				stimes[i] = Integer.parseInt(stime[i]);
				etimes[i] = Integer.parseInt(etime[i]);
			}
			
			if(etimes[0] == 0) etimes[0] = 24;
			int sec=0,min=0,hour=0;
			sec = etimes[2]-stimes[2];
			min = etimes[1]-stimes[1];
			hour = etimes[0]-stimes[0];
			
			if(sec<0) {
				sec+=60;
				min-=1;
			}
			if(min<0) {
				min+=60;
				hour-=1;
			}
			if(hour<0) {
				hour+=24;
			}
			
			System.out.println("#" + tc + " " + String.format("%02d", hour) + ":" + String.format("%02d", min) + ":" + String.format("%02d", sec));
		}
	}
}
