package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_5549 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_5549.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			String num = sc.next();
			
			int n = (int)num.charAt(num.length()-1);
			if(n%2 != 0) {
				System.out.println("#" + tc + " " + "Odd");
			}else {
				System.out.println("#" + tc + " " + "Even");
			}
		}
	}
}
