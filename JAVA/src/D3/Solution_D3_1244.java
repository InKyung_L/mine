package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_1244 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_1244.txt"));
Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();	
		for(int tc=1;tc<=T;tc++) {
			String num = sc.next();
			int[] n = new int[num.length()];
			int cnt = sc.nextInt();
			
			for(int i=0;i<num.length();i++) {
				n[i] = num.charAt(i)-'0';
			}
			System.out.println(Arrays.toString(n)+"\n");
			
here:		for(int i=0;i<num.length()-1;i++) {
				if(cnt==0) break;
				int max=i;
				for(int j=i+1;j<num.length();j++) {
					if(n[j]>=n[max]) {
						max=j;
					}					
				}
				if(max==i) continue here;
				int temp = n[i];
				n[i]=n[max];
				n[max]=temp;
				System.out.println(Arrays.toString(n));
				cnt--;
			}
			System.out.println("\n" +Arrays.toString(n));
		}
		
	}
}