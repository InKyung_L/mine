package D3;

import java.io.FileInputStream;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

//첫번째 값 : (줄-1)^2*2+1
//마지막 값 : (줄)^2*2-1

public class Solution_D3_8016 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_8016.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			BigInteger i1 = new BigInteger(sc.next());
			BigInteger f1 = i1.subtract(BigInteger.ONE);
			
			BigInteger first = f1.multiply(f1).multiply(BigInteger.valueOf(2)).add(BigInteger.ONE);
			BigInteger last = i1.multiply(i1).multiply(BigInteger.valueOf(2)).subtract(BigInteger.ONE);
			
			System.out.println("#" + tc + " " + first + " " + last);
		}
		
	}
}
