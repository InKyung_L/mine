package D3;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Solution_D3_1221 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_1221.txt"));
		//Scanner sc = new Scanner(System.in);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1;tc<=T;tc++) {
			Queue<String> q = new LinkedList<>();
			String s = br.readLine();
			StringTokenizer  st = new StringTokenizer(s);
			String cases[] = s.split(" ");
			int prob = Integer.parseInt(cases[1]);
			String[] nori = {"ZRO", "ONE", "TWO", "THR", "FOR", "FIV", "SIX", "SVN", "EGT", "NIN"};
			
			String num = br.readLine();
			String[] obo = num.split(" ");
			//int cp = 0;
			//String cmp = nori[cp];
			for(int j=0;j<10;j++) {
				for(int i=0;i<prob;i++) {
					if(obo[i].equals(nori[j])) {
						q.add(obo[i]);
					}
				}
			}		
			System.out.print("#" + tc + " ");
			for(int i=0;i<prob;i++) {
				System.out.print(q.poll() + " ");
			}
			System.out.println();
		}
	}
}
