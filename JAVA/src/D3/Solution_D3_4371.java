package D3;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class Solution_D3_4371 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_4371.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();	
		for(int tc=1;tc<=T;tc++) {
			int cs = sc.nextInt();
			int[] boat = new int[cs];
			boolean[] ck = new boolean[cs];
			
			for(int i=0;i<cs;i++) {
				boat[i] = sc.nextInt();
			}
			int p=0;
			int cnt=cs-1;
			int ans=0;
			while(cnt>0) {
				int min = boat[p+1]-boat[0];
				for(int i=1;i<cs;i++) {
					if(ck[i] == false && boat[0]+min*i == boat[i]) {
						ck[i] = true;
						cnt--;
					}
				}p++;ans++;
			}
			System.out.println("#" + tc + " " + ans);
		}		
	}
}
