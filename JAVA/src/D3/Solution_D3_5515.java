package D3;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_5515 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D3_5515.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int mon = sc.nextInt();
			int day = sc.nextInt();
			int sum=0, na=0, res=0;
			
			int[] days = {0,31,29,31,30,31,30,31,31,30,31,30,31};
			
			for(int i=1;i<mon;i++) {
				sum += days[i];
			}
			sum = sum+day-1;
			na = sum%7;
			res = na+4;
			if(res > 6) {
				System.out.println("#" + tc + " " + (res-7));
			}else {
				System.out.println("#" + tc + " " + res);
			}
		}
	}
}
