package D1;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D1_2058 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D1_2058.txt"));
		Scanner sc = new Scanner(System.in);
		
		int a = sc.nextInt();
		int t=10;
		int na = 0;
		int mo=0;
		int sum = 0;
		while(a>0) {
			na = a%t;
			mo = a/t;
			sum += na;
			a=mo;
		}
		
		System.out.println(sum);

	}

}
