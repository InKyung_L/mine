package D1;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D1_2056 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D1_2056.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		int day=0;
		int month=0;
		int year=0;

		for(int tc=1;tc<=T;tc++) {
			int date = sc.nextInt();
			year = date/10000;
			day = date%100;
			month = (date/100)%100;
			
			switch (month) {
			case 0:
				System.out.println("#" + tc + " " + "-1");
				continue;
				
			case 1:
				if(day>31 || day<=0){
					System.out.println("#" + tc + " " + "-1");
					continue;
				}		
				else break;
				
			case 2:
				if(day>28 || day<=0) {
					System.out.println("#" + tc + " " + "-1");
					continue;
				}		
				else break;
				
			case 3:
				if(day>31 || day<=0){
					System.out.println("#" + tc + " " + "-1");
					continue;
				}		
				else break;
				
			case 4:
				if(day>30 || day<=0){
					System.out.println("#" + tc + " " + "-1");
					continue;
				}		
				else break;
				
			case 5:
				if(day>31 || day<=0){
					System.out.println("#" + tc + " " + "-1");
					continue;
				}		
				else break;
				
			case 6:
				if(day>30 || day<=0){
					System.out.println("#" + tc + " " + "-1");
					continue;
				}		
				else break;
				
			case 7:
				if(day>31 || day<=0){
					System.out.println("#" + tc + " " + "-1");
					continue;
				}		
				else break;
				
			case 8:
				if(day>31 || day<=0){
					System.out.println("#" + tc + " " + "-1");
					continue;
				}		
				else break;
				
			case 9:
				if(day>30 || day<=0){
					System.out.println("#" + tc + " " + "-1");
					continue;
				}		
				else break;
				
			case 10:
				if(day>31 || day<=0){
					System.out.println("#" + tc + " " + "-1");
					continue;
				}		
				else break;
				
			case 11:
				if(day>30 || day<=0){
					System.out.println("#" + tc + " " + "-1");
					continue;
				}		
				else break;
				
			case 12:
				if(day>31 || day<=0){
					System.out.println("#" + tc + " " + "-1");
					continue;
				}		
				else break;
				
			}
			
			System.out.print("#" + tc + " ");
			System.out.printf("%04d", year);
			System.out.print("/");
			System.out.printf("%02d", month);
			System.out.print("/");
			System.out.printf("%02d", day);
			System.out.println();
			
		}
		
	}

}
