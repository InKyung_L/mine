package D1;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D1_1933 {

	public static void main(String[] args) throws Exception{
		Scanner sc = new Scanner(System.in);
		
		int num = sc.nextInt();
		
		for(int i=1;i<=num;i++) {
			if(num%i == 0)
				System.out.print(i + " ");
		}
	}

}
