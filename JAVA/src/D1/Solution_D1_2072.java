package D1;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D1_2072 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D1_2072.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int sum=0;
			for(int i=0;i<10;i++) {
				int num = sc.nextInt();
				if(num%2 == 1)
					sum += num;
			}
			System.out.println("#" + tc + " " + sum);
		}
	}

}
