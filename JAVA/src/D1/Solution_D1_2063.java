package D1;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D1_2063 {

	public static void main(String[] args) throws Exception{
		Scanner sc = new Scanner(System.in);
		
		int a = sc.nextInt();
		int ans;
		
		for(int i=0;i<=8;i++) {
			ans = (int) Math.pow(2, i);
			System.out.print(ans + " ");
		}
	}

}
