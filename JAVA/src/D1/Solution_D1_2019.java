package D1;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D1_2019 {

	public static void main(String[] args) throws Exception{
		Scanner sc = new Scanner(System.in);
		
		int a = sc.nextInt();
		int[] arr = new int[a];
		
		
		for(int i=0;i<a;i++) {
			arr[i] = sc.nextInt();
		}
		Arrays.sort(arr);
		System.out.println(arr[a/2]);
	}

}
