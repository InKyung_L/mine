package D1;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D1_2050 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D1_2050.txt"));
		Scanner sc = new Scanner(System.in);
		
		String t = sc.next();

		for(int i=0;i<t.length();i++) {
			System.out.print((int)t.charAt(i)-64 + " ");
		}

	}

}
