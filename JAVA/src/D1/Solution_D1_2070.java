package D1;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D1_2070 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D1_2070.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		char ans;
		
		for(int tc=1;tc<=T;tc++) {
			int a = sc.nextInt();
			int b = sc.nextInt();
			
			if(a>b)
				ans = '>';
			else if(a<b)
				ans = '<';
			else
				ans = '=';
			
			System.out.println("#" + tc + " " + ans);
			
		}

	}

}
