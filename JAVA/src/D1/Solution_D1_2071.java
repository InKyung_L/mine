package D1;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D1_2071 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D1_2071.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			double sum=0.0;
			for(int i=0;i<10;i++) {
				int num = sc.nextInt();
				sum += num;
			}
			System.out.println("#" + tc + " " + Math.round(sum/10));
		}
	}

}
