package D1;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D1_2025 {

	public static void main(String[] args) throws Exception{
		Scanner sc = new Scanner(System.in);
		
		int a = sc.nextInt();
		int sum=0;
		
		for(int i=a;i>0;i--) {
			sum += i;
		}
		System.out.println(sum);
	}

}
