package D1;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D1_1936 {

	public static void main(String[] args) throws Exception{
		Scanner sc = new Scanner(System.in);
		
		int A = sc.nextInt();
		int B = sc.nextInt();
		
		switch (B) {
		case 1:
			if(A == 3)
				System.out.println("B");
			else System.out.println("A");
			break;
			
		case 2:
			if(A == 1)
				System.out.println("B");
			else System.out.println("A");
			break;
			
		case 3:
			if(A == 2)
				System.out.println("B");
			else System.out.println("A");
			break;
		}

	}

}
