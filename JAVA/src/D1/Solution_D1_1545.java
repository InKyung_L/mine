package D1;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D1_1545 {

	public static void main(String[] args) throws Exception{
		Scanner sc = new Scanner(System.in);
		
		int num = sc.nextInt();
		
		for(int i=num;i>=0;i--) {
			System.out.print(i + " ");
		}
	}

}
