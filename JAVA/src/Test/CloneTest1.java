package Test;

import java.util.Arrays;

public class CloneTest1 {

	public static void main(String[] args) {
		int[] a = {1,2,3,4,5,6,7,8,9};
		
		//int[] b = a;
		//int[] b = a.clone();
		//int[] b = Arrays.copyOf(a, a.length);
		int[] b = new int[a.length];
		System.arraycopy(1,0,b,0,a.length); //위에 배열객체를 미리 만들어 놓으면 얘를 사용하는 시간만 쓰면 됨. 그렇지 않으면 copyOf가 나음
//		
//		System.out.println(Arrays.toString(a));
//		System.out.println(Arrays.toString(b));
//		System.out.println();
//
//		a[0]=11;
//		System.out.println(Arrays.toString(a));
//		System.out.println(Arrays.toString(b));
//		System.out.println();
//		
//		b[8]=99;
//		System.out.println(Arrays.toString(a));
//		System.out.println(Arrays.toString(b));
//		System.out.println();

	}

}
