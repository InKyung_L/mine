package Test;

import java.util.Arrays;

public class CloneTest2 {

	public static void main(String[] args) {
		int[][] a = {{1,2,3},{4,5,6},{7,8,9}};


//		int[][] b = a;
//		int[][] b = a.clone();
//		int[][] b = Arrays.copyOf(a, a.length);
//		int[][] b = new int[a.length][a.length];
//		System.arraycopy(1,0,b,0,a.length); //위에 배열객체를 미리 만들어 놓으면 얘를 사용하는 시간만 쓰면 됨. 그렇지 않으면 copyOf가 나음

		int[][] b = new int[a.length][a.length];
		for(int i=0;i<a.length;i++) {
//			b[i] = a[i].clone();
//			b[i] = Arrays.copyOf(a[i], a[i].length);
			System.arraycopy(a[i], 0, b[i], 0, a[i].length);
		}
		
		for(int[] r:a) System.out.println(Arrays.toString(r));
		for(int[] r:b) System.out.println(Arrays.toString(r));
		System.out.println();
		a[0][0]=11;
		for(int[] r:a) System.out.println(Arrays.toString(r));
		for(int[] r:b) System.out.println(Arrays.toString(r));
		System.out.println();
		
		b[2][2]=99;
		for(int[] r:a) System.out.println(Arrays.toString(r));
		for(int[] r:b) System.out.println(Arrays.toString(r));
		System.out.println();
	}

}
