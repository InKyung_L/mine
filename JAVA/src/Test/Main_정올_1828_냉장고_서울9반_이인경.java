package Test;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Main_정올_1828_냉장고_서울9반_이인경 {
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_jo_1828.txt"));
		Scanner sc = new Scanner(System.in);
		ArrayList<int[]> al = new ArrayList<>();

		int T = sc.nextInt();
		int[] chem = new int[2];
		int cnt=1;
		for(int i=0;i<T;i++) {
			chem = new int[2];
			
			chem[0] = sc.nextInt();
			chem[1] = sc.nextInt();
			
			al.add(chem);
		}
		Collections.sort(al, new Comparator<int []>() {
			@Override
			public int compare(int[] o1, int[] o2) {
					return (o1[1]>o2[1])?1:-1;
			}
		});
//		
		int max=al.get(0)[1];
		for(int i=1;i<T;i++) {
			if(max<al.get(i)[0]) {
				max=al.get(i)[1];
				cnt++;
			}
		}
		System.out.println(cnt);
	}
}
