package Test;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Solution_D5_최적경로 {
	public static int v[],ax[],ay[],sum,cs,cmp,home,xrd[],yrd[],min,xcmp,ycmp,xhome,yhome;
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/d5_최적경로.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			cs = sc.nextInt();
			xcmp = sc.nextInt();
			ycmp = sc.nextInt();
			xhome = sc.nextInt();
			yhome = sc.nextInt();
			xrd = new int[cs];
			yrd = new int[cs];
			
			for(int i=0;i<cs;i++) {
				xrd[i]=sc.nextInt();
				yrd[i]=sc.nextInt();
			}
			
			v = new int[cs];
			ax = new int[cs];
			ay = new int[cs];
			min=Integer.MAX_VALUE;
			permcomb(0,0);
			System.out.println("#"+tc+" "+min);
		}
	}
	private static void permcomb(int start, int count) {
		if(count==cs) {
			sum=0;
			sum+=(Math.abs(ax[0]-xcmp)+Math.abs(ay[0]-ycmp))+(Math.abs(ax[cs-1]-xhome)+Math.abs(ay[cs-1]-yhome));
			for(int i=1;i<cs;i++) {
				sum+=(Math.abs(ax[i]-ax[i-1])+Math.abs(ay[i]-ay[i-1]));
			}
			min=Math.min(min, sum);
			//System.out.println(Arrays.toString(ax) + " " + Arrays.toString(ay));
			return;
		}
		
		for(int i=0;i<cs;i++) {
			if(v[i]==0) {
				v[i]=1;
				ax[count]=xrd[i];
				ay[count]=yrd[i];
				permcomb(i, count+1);
				v[i]=0;
			}
		}
		
	}
	
}
