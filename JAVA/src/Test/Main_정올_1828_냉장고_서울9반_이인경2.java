package Test;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Main_정올_1828_냉장고_서울9반_이인경2 {
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_jo_1828.txt"));
		Scanner sc = new Scanner(System.in);

		int N = sc.nextInt();
		int[][] matter = new int[N][2];
		
		for(int i=0;i<N;i++) {
			matter[i][0]=sc.nextInt();
			matter[i][1]=sc.nextInt();
		}
		//최고 온도를 기준으로 오름차순 정렬.
		Arrays.sort(matter, new Comparator<int[]>() {
			@Override
			public int compare(int[] o1, int[] o2) {
				return Integer.compare(o1[1], o2[1]);
			}
		});
		
		int cnt=1;
		int max = matter[0][1];
		for(int i=1;i<N;i++) {
			if(max<matter[i][0]) { //현재 화학물질의 최저 온도가 냉장고의 최고 온도보다 크면 새로운 냉장고 사용
				max=matter[i][1];
				cnt++;
			}
		}
		System.out.println(cnt);
	}
}
