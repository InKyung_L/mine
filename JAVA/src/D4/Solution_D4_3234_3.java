package D4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Solution_D4_3234_3 {
	public static boolean[] check;
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/D4_3234.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int N = sc.nextInt();
			int[] weight = new int[N];
			for(int i=0;i<N;i++) {
				weight[i]=sc.nextInt();
			}
			check = new boolean[N];
			System.out.println("#" + tc + " " + dfs(weight, new boolean[N],0,0,0));
		}
	}

	private static int dfs(int[] weight, boolean[] check, int left, int right, int depth) {
		if(left<right) return 0;
		int r=0;
		if(weight.length==depth) {
			return 1;
		}
		for(int i=0;i<weight.length;i++) {
			if(check[i]) continue; //방문하지 않았어야 하고,
			check[i]=true;
			r=r+dfs(weight, check, left+weight[i], right, depth+1);
			r=r+dfs(weight, check, left, right+weight[i], depth+1);
			check[i]=false; //돌고 다시 원위치를 시켜야함.
		}
		return r;
	}

}
