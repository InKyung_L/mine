package D4;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class Solution_D4_1227 {
	public static char[][] maze;
	public static int[][] visit;
	public static int[] dx= {0,0,1,-1}, dy= {1,-1,0,0};
	public static int si,sj,ei,ej;
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D4_1227.txt"));
		Scanner sc = new Scanner(System.in);		
			
		for(int tc=1;tc<=10;tc++) {
			sc.nextInt();			
			int ans=0;
			maze = new char[100][100];
			visit = new int[100][100];
			
			for(int i=0;i<100;i++) {
				String st = sc.next();
				for(int j=0;j<100;j++) {
					maze[i][j] = st.charAt(j);
					if(maze[i][j]=='2') {
						si=i;
						sj=j;
					}else if(maze[i][j]=='3') {
						ei=i;
						ej=j;
					}
				}
			}
			dfs(si,sj);
			
			if(visit[ei][ej] == 1) {
				ans=1;
			}
			System.out.println("#" + tc + " " + ans);
		}		
	}
	private static void dfs(int i, int j) {
		visit[i][j]=1;
		
		for(int d=0;d<dx.length;d++) {
			int ni=i+dx[i];
			int nj=j+dy[j];
			
			if(0<=ni && ni<100 && 0<=nj && nj<100 && visit[ni][nj]==0 && maze[ni][nj]!='1') {
				visit[ni][nj] = 1;
				dfs(ni,nj);
			}
		}
		
		
	}
}
