package D4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Solution_D4_1251_ks {
	private static Node[] nodes, routes;
    private static int[] parents;

    private static class Node implements Comparable<Node> {
        int x, y;
        long p;

        private Node(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public int compareTo(Node o) {
            return Long.compare(this.p, o.p);
        }
    }

    private static int find(int a) {
        if (parents[a] < 0) return a;
        return parents[a] = find(parents[a]);
    }

    private static boolean union(int a, int b) {
        int aRoot = find(a);
        int bRoot = find(b);
        if (aRoot != bRoot) {
            parents[bRoot] = aRoot;
            return true;
        }
        return false;
    }

    public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/D4_1251.txt"));
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        int T = Integer.parseInt(br.readLine());
        for (int tc = 1; tc <= T; tc++) {
            int N = Integer.parseInt(br.readLine());
            double ans = 0;
            parents = new int[N];
            nodes = new Node[N];
            routes = new Node[N*N];
            
            int[] X = new int[N];
            int[] Y = new int[N];
            
            StringTokenizer st1 = new StringTokenizer(br.readLine());
            for(int i=0;i<N;i++) {
            	X[i] = Integer.parseInt(st1.nextToken());
            }
            StringTokenizer st2 = new StringTokenizer(br.readLine());
            for(int i=0;i<N;i++) {
            	Y[i] = Integer.parseInt(st2.nextToken());
            }
            
            for (int i=0;i<N;i++) {
                nodes[i] = new Node(X[i], Y[i]);
            }
            double e = Double.parseDouble(br.readLine());

            int cnt = 0;
            for (int i=0;i<N;i++) {
                for (int j=0;j<N;j++) {
                	routes[cnt] = new Node(i, j);                 
                	routes[cnt++].p = (long)(Math.pow((nodes[i].x-nodes[j].x),2)+(Math.pow((nodes[i].y-nodes[j].y), 2)));
                }
            }
            Arrays.sort(routes);
            Arrays.fill(parents, -1);

            for (int i = 0; i < routes.length; i++) {
                Node cur = routes[i];
                if (cur.p == 0) continue;
                if (union(cur.x, cur.y)) {
                    ans += cur.p;
                }
            }
            System.out.println("#" + tc + " " + Math.round(ans*e));
        }
    }
}