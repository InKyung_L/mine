package D4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Solution_D4_1211 {
	public static int[][] maze, visit;
	public static int si,sj,ans,min;
	public static int[] di= {0,0,1}, dj= {-1,1,0};
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D4_1211.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		for(int tc=1;tc<=10;tc++) {
			maze = new int[10][10];
			br.readLine();
			for(int i=0;i<10;i++) {
				StringTokenizer st = new StringTokenizer(br.readLine());
				for(int j=0;j<10;j++) {
					maze[i][j]=Integer.parseInt(st.nextToken());
				}
			}//end of for(maze)
			
			for(int i=0;i<10;i++) {
				if(maze[0][i]==1) {
					si=0;sj=i;
					visit = new int[10][10];
					min = Integer.MAX_VALUE;
					dfs(si,sj,1);
				}
			}
			
			System.out.println("#" + tc + " " + ans);
			
		}//end of tc
		
	}


	private static void dfs(int x, int y, int count) {
		visit[x][y] = 1;
		
		for(int d=0;d<di.length;d++) {
			int ni = x+di[d];
			int nj = y+dj[d];
			
			if(ni>=0 && ni<10 && nj>=0 && nj<10 && visit[ni][nj]==0 && maze[ni][nj]!=0) {
				visit[ni][nj]=1;
				if(ni==9) {
					if(min>=count) {
						min = count;
						if(min==count) {
							ans = Math.max(ans, sj);
						}
						ans = ni;
					}
				}
				dfs(ni,nj,count+1);
			}
		}
		//return ans;
	}
}
