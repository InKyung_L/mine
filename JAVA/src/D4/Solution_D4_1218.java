package D4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;
import java.util.StringTokenizer;

public class Solution_D4_1218 {
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D4_1218.txt"));
		Scanner sc = new Scanner(System.in);		
		//BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Stack<Character> stack = new Stack<>();	
		
		for(int tc=1;tc<=10;tc++) {
			int N = sc.nextInt();
			String st = sc.next();
			int ans=1;
			
			for(int i=0;i<N;i++) {
				if(st.charAt(i) == '(' || st.charAt(i) == '{' || st.charAt(i) == '<' || st.charAt(i) == '[') {
					stack.add(st.charAt(i));
				}else {
					switch (st.charAt(i)) {
					case ']':
						if(stack.peek() == '[') stack.pop();
						else ans=0;
						break;

					case '}':
						if(stack.peek() == '{') stack.pop();
						else ans=0;
						break;
						
					case ')':
						if(stack.peek() == '(') stack.pop();
						else ans=0;
						break;
						
					case '>':
						if(stack.peek() == '<') stack.pop();
						else ans=0;
						break;
					}
				}
			}
			System.out.println("#" + tc + " " + ans);
		}
		
		
	}
}
