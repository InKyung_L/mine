package D4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;
import java.util.StringTokenizer;

public class Solution_D4_1486 {
	public static int emp, B, r, min;
	public static int[] v, a, tower;
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D4_1486.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1;tc<=T;tc++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			emp = Integer.parseInt(st.nextToken());
			B = Integer.parseInt(st.nextToken());
			
			tower = new int[emp];
			StringTokenizer st2 = new StringTokenizer(br.readLine());
			for(int i=0;i<emp;i++) {
				tower[i] = Integer.parseInt(st2.nextToken());
			}
			
			v=new int[emp];
			min = Integer.MAX_VALUE;
			for(r=1;r<=emp;r++) {
				a = new int[r];
				dfs(0,0,0);
			}
			System.out.println("#" + tc + " " + min);
		}
		
	}
	private static void dfs(int start, int count, int sum) {
		if(count==r) {
			if(sum>=B) {
				min = Math.min(min, sum-B);
			}
			//System.out.println(Arrays.toString(a) + " sum: "+sum + " sub: " + min);
			
			return;
		}
		
		for(int i=start;i<emp;i++) {
			if(v[i]==0) {
				v[i]=1;
				a[count]=tower[i];
				//if(sum>=B) {
					dfs(i,count+1,sum+a[count]);
				//}			
				v[i]=0;
			}
		}
		
	}
}
