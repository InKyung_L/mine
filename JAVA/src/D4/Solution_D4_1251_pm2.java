package D4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

		
public class Solution_D4_1251_pm2 {
	public static int N, cnt=1;
	public static long ans=0;
	public static long[][] island;
	
	public static long prim() {
		PriorityQueue<long[]> pq = new PriorityQueue<long[]>(new Comparator<long[]>() {
            @Override
            public int compare(long[] o1, long[] o2) {
                return Long.compare(o1[1], o2[1]);
            }
        });
		 boolean[] v = new boolean[N];
         v[0] = true;
         for(int i=0;i<N;i++) {
         	pq.offer(new long[] { i, island[0][i] });
         }  
         
         while (!pq.isEmpty()) {
             long[] arr = pq.poll();
             if (!v[(int) arr[0]]) {
                 v[(int) arr[0]]=true;
                 ans+=arr[1];
                 cnt++;
                 for (int i=0;i<N;i++) {
                 	pq.offer(new long[] { i, island[(int) arr[0]][i] });
                 }          	
             }
         }
         return ans;
		
	}
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/D4_1251.txt"));
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        int T = Integer.parseInt(br.readLine());
        for (int tc=1; tc<=T; tc++) {
            N = Integer.parseInt(br.readLine());
            int[] X = new int[N];
            int[] Y = new int[N];
            ans=0;
            
            StringTokenizer st1 = new StringTokenizer(br.readLine());
            for(int i=0;i<N;i++) {
            	X[i] = Integer.parseInt(st1.nextToken());
            }                
            StringTokenizer st2 = new StringTokenizer(br.readLine());
            for(int i=0;i<N;i++) {
            	Y[i] = Integer.parseInt(st2.nextToken());
            }                
            double e = Double.parseDouble(br.readLine());
 
            island = new long[N][N];
            for(int i=0;i<N;i++) {
            	for(int j=0;j<N;j++) {
            		if(i==j) {
            			island[i][j]=0;
            		}else {
            			island[i][j]=Long.MAX_VALUE/2;
            		}
            	}          	
            }
            
            for (int i=0;i<N-1;i++) {
                for (int j=i+1;j<N;j++) {
                    long distance = (long) (Math.pow(X[j]-X[i],2)+Math.pow(Y[j]-Y[i],2));
                    island[i][j] = island[j][i] = distance;
                }
            }

           
            System.out.println("#" + tc + " " + Math.round(prim()*e));
        }
    }
}