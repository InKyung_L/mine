package D4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Solution_D4_1251_pm {
	public static int N;
	public static long ans = 0;
	public static long[][] island;
	public static long[] w;

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/D4_1251.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int T = Integer.parseInt(br.readLine());
		for (int tc = 1; tc <= T; tc++) {
			N = Integer.parseInt(br.readLine());
			int[] X = new int[N];
			int[] Y = new int[N];
			w = new long[N];
			ans = 0;

			StringTokenizer st1 = new StringTokenizer(br.readLine());
			for (int i = 0; i < N; i++) {
				X[i] = Integer.parseInt(st1.nextToken());
			}
			StringTokenizer st2 = new StringTokenizer(br.readLine());
			for (int i = 0; i < N; i++) {
				Y[i] = Integer.parseInt(st2.nextToken());
			}
			double e = Double.parseDouble(br.readLine());

			island = new long[N][N];
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					if (i == j) {
						island[i][j] = 0;
					} else {
						island[i][j] = Long.MAX_VALUE / 2;
					}
				}
			}		
			
			for (int i = 0; i < N - 1; i++) {
				for (int j = i + 1; j < N; j++) {
					long distance = (long) (Math.pow(X[j] - X[i], 2) + Math.pow(Y[j] - Y[i], 2));
					island[i][j] = island[j][i] = distance;
				}
			}
			
			Arrays.fill(w, -1);
			w[0]=0;
			for(int k=1;k<N;k++) {
				long minWeight=Long.MAX_VALUE / 2;
				int minVertex=0;
				for(int i=0;i<N;i++) {
					if(w[i]<0) continue;
					for(int j=0;j<N;j++) {
						if(w[j]>=0) continue;
						if(island[i][j]<minWeight) {
							minWeight=island[i][j];
							minVertex=j;
						}
					}
				}
				w[minVertex]=minWeight;
			}
			
			
			long ans=0;
			for(int i=1;i<N;i++) {
				ans+=w[i];
			}
			
			System.out.println("#" + tc + " " + Math.round(ans * e));
		}
	}
}