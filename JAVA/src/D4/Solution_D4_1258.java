package D4;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.StringTokenizer;
import java.util.TreeSet;

import javax.swing.plaf.synth.SynthSeparatorUI;

public class Solution_D4_1258 {
	
	public static int N,cnt, mini, minj, maxi, maxj;
	public static TreeSet<int[]> ts;
	public static int[][] map, v;
	public static int[] di= {1,-1,0,0}, dj= {0,0,-1,1};
	public static ArrayList<int[]> al;
	public static ArrayList<int[][]> al2;
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D4_1258.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		
		int T = Integer.parseInt(br.readLine());	
		for(int tc=1;tc<=T;tc++) {
			N = Integer.parseInt(br.readLine());
			map = new int[N][N];
			v = new int[N][N];
			al = new ArrayList<int[]>();
			al2 = new ArrayList<int[][]>();
			ts = new TreeSet<>();
			int[] t;
			int[][] t2;
			for(int i=0;i<N;i++) {
				StringTokenizer st = new StringTokenizer(br.readLine());
				for(int j=0;j<N;j++) {
					map[i][j] = Integer.parseInt(st.nextToken());
				}
			}//end of map
			cnt=0;
			for(int i=0;i<N;i++) {
				for(int j=0;j<N;j++) {
					if(map[i][j]!=0 && v[i][j]==0) {
						mini = Integer.MAX_VALUE;
						minj = Integer.MAX_VALUE;
						maxi = Integer.MIN_VALUE;
						maxj= Integer.MIN_VALUE;
						dfs(i,j);
						//System.out.println(mini + " " + minj + " " + maxi + " " + maxj);
						t = new int[3];
						t2 = new int[1][3];
						t[0] = maxi-mini+1; t[1]=maxj-minj+1; t[2]=t[0]*t[1];
						t2[0][0]=maxi-mini+1; t2[0][1]=maxj-minj+1; t2[0][2]=t2[0][0]*t2[0][1];
						al.add(t);
						al2.add(t2);
						cnt++;
					}
				}
			}
			
//			for(int i=0;i<al.size();i++) {
//				for(int j=0;j<3;j++) {
//					System.out.print(al.get(i)[j] + " ");
//				}System.out.println();
//			}
			Collections.sort(al, new Comparator<int[]>() {

				@Override
				public int compare(int[] o1, int[] o2) {
					if(o1[2] > o2[2])
						return 1;
					return -1;
				}
			});
			
//			for(int i=0;i<al.size();i++) {
//				for(int j=0;j<3;j++) {
//					System.out.print(al.get(i)[j] + " ");
//				}System.out.println();
//			}
			System.out.print("#"+tc+" "+cnt + " ");
			for(int i=0;i<al.size();i++) {
				for(int j=0;j<2;j++) {
					System.out.print(al.get(i)[j] + " ");
				}
			}System.out.println();
		}//end of tc
		
		
	}
	private static void dfs(int i, int j) {
		v[i][j]=1;
		int ni=0,nj=0;
		for(int d=0;d<di.length;d++) {
			ni = i+di[d];
			nj = j+dj[d];
			
			if(ni>=0 && ni<N && nj>=0 && nj<N && map[ni][nj]!=0 && v[ni][nj]==0) {
				mini = Math.min(mini, i);
				minj = Math.min(minj, j);
				maxi = Math.max(maxi, ni);
				maxj = Math.max(maxj, nj);
				v[ni][nj]=1;
				dfs(ni,nj);
			} //end of if
		}//end of for
		
	}
}
