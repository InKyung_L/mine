package D4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Solution_D4_3234 {
	public static int[] left, b, v,a;
	public static int r, cnt, N, total, right;
//왼쪽이 항상 커야한다.
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/D4_3234.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int T = Integer.parseInt(br.readLine());
		for (int tc = 1; tc <= T; tc++) {
			N = Integer.parseInt(br.readLine());

			b = new int[N];
			StringTokenizer st = new StringTokenizer(br.readLine());
			for (int i = 0; i < N; i++) {
				b[i] = Integer.parseInt(st.nextToken());
				total+=b[i];
			}
			
			r=N;
			a = new int[r];
			v = new int[N];
			//for(int i=0;i<=N;i++) {
			//	r = N-i;
				left = new int[r];
				v = new int[N];
				permcomb(0,0);
			//}
			System.out.println(cnt);
		}
	}

	private static void permcomb(int start, int count) {
		if(count==r) {
			//cnt++;
			int sum1=0, sum2=0;
			for(int i=0;i<left.length;i++) {
				sum1+=left[i];
				for(int j=i+1;j<left.length;j++) {
					sum2+=left[j];
				}
				if(sum1>sum2) {
					cnt++;
				}
				sum1=0; sum2=0;
			}
			System.out.println(Arrays.toString(left));
			System.out.println(cnt);
			return;
		}
		
		for(int i=0;i<N;i++) {
			if(v[i]==0) {
				v[i]=1;
				left[count] = b[i];
				permcomb(i, count+1);
				v[i]=0;
			}
		}
	}

}
