package D4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.StringTokenizer;

class Solution_D4_7699{
	public static int R, C, max=0;
	public static int[] di= {1,-1,0,0}, dj= {0,0,1,-1};
	public static Character[][] travel;
	public static HashSet<Character> hs;
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/D4_7699.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=T; tc++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			R = Integer.parseInt(st.nextToken());
			C = Integer.parseInt(st.nextToken());
			hs = new HashSet<>();
			
			travel = new Character[R][C];
			for(int i=0;i<R;i++) {
				String s = br.readLine();
				for(int j=0;j<C;j++) {
					travel[i][j]=s.charAt(j);
				}
			}
			max=0;

			hs.add(travel[0][0]);
			gotrip(0,0,1);	
		
			System.out.println("#" + tc + " " + max);
		}
	}

	private static void gotrip(int i, int j, int dep) {
		max = Math.max(max, dep);
		
		for(int d=0;d<di.length;d++) {
			int ni=i+di[d];
			int nj=j+dj[d];
			
			if(ni>=0 && ni<R && nj>=0 && nj<C && hs.add(travel[ni][nj])) { //지금 이동하는 좌표의 알파벳이 방문하지 않은 곳이라면(add가 true)
				gotrip(ni,nj,dep+1);
				hs.remove(travel[ni][nj]); //방문처리 해제. 해제를 안해주면 한바퀴만 돌고 나옴.
			}
		}
		
	}
}