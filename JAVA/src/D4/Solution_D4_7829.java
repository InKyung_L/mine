package D4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Solution_D4_7829 {
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D4_7829.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1;tc<=T;tc++) {
			int N = Integer.parseInt(br.readLine());
			long max = Integer.MIN_VALUE;
			long min = Integer.MAX_VALUE;
			
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int i=0;i<N;i++) {
				int p = Integer.parseInt(st.nextToken());
				max = Math.max(p, max);
				min = Math.min(p, min);
			}
			
			System.out.println("#" + tc + " " + min*max);
		}
		
	}
}
