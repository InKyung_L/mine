package D4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

public class Solution_D4_1251 {
	public static int N;
	public static List<int[]> v;
	public static int[] p;
	
	public static int findSet(int x) {
		if(p[x]==x) return x;
		else return p[x]=findSet(p[x]);
	}
	
	public static void union(int a, int b) {
		a=findSet(a);
		b=findSet(b);
		if(a<b) p[b]=a;
		else p[a]=b;
	}
	
	public static int kruskal() {
		Collections.sort(v, new Comparator<int[]>() {

			@Override
			public int compare(int[] o1, int[] o2) {
				return Integer.compare(o1[4], o2[4]);
			}
		});
		for(int[] e:v) System.out.println(Arrays.toString(e));
		
		p = new int[N+1]; //makeset을 얘로 대신한다.
		for(int i=0;i<N+1;i++) p[i]=i;
		
		int sum=0;
		for(int i=0;i<v.size();i++) {
			if(findSet(v.get(i)[0])!=findSet(v.get(i)[2])) { //이쪽 부모와 저쪽 부모가 다르면
				System.out.println("->"+Arrays.toString(v.get(i)));
				sum+=v.get(i)[4]; //가중치를 더하고
				union(v.get(i)[0], v.get(i)[2]);
			}
		}
		return sum;
	}
	
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/D4_1251.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		
		int T = Integer.parseInt(br.readLine());	
		for(int tc=1;tc<=T;tc++) {
			N = Integer.parseInt(br.readLine());
			int[][] island = new int[N][2];
			
			for(int j=0;j<2;j++) {	
				StringTokenizer st = new StringTokenizer(br.readLine());
				for(int i=0;i<N;i++) {		
					island[i][j]=Integer.parseInt(st.nextToken());
				}
			}
			double E = Double.parseDouble(br.readLine());

			v = new ArrayList<>();
			for(int i=0;i<N-1;i++) {
				for(int j=i+1;j<N;j++) {
					if(island[i][0]==island[j][0]) {
						int distance = (int)(E*Math.round(Math.pow(Math.abs((island[j][0]-island[i][0]))+Math.abs((island[j][1]-island[i][1])), 2)));
						v.add(new int[] {island[i][0], island[i][1], island[j][0], island[j][1], distance});
					}
				}		
			}
			
			System.out.println(kruskal());
		}
		
	}
}
