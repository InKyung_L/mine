package Solution;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main_17406_배열돌리기4_서울9반_이인경 {
	public static int[] v;
	public static int[][] ck, a, arr, narr, rarr;
	public static int r, K, N, M, min, sum;
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		K = Integer.parseInt(st.nextToken());
		min=Integer.MAX_VALUE;
		sum=0;
		
		arr = new int[N][M];
		for(int i=0;i<N;i++) {
			st = new StringTokenizer(br.readLine());
			for(int j=0;j<M;j++) {
				arr[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		
		narr = new int[N][M];
		rarr = new int[N][M];
		for(int i=0;i<arr.length;i++) {
			narr[i] = arr[i].clone();
		}

		for(int i=0;i<arr.length;i++) {
			rarr[i] = arr[i].clone();
		}
		
		ck = new int[K][3];
		for(int kk=0;kk<K;kk++) {
			st = new StringTokenizer(br.readLine());
			ck[kk] = new int[] {Integer.parseInt(st.nextToken())-1,Integer.parseInt(st.nextToken())-1,Integer.parseInt(st.nextToken())};		
		}
			
		v=new int[K];
		r=K;
		a=new int[K][3];
		permcomb(0,0);
		
		System.out.println(min);
	}

	private static void permcomb(int start, int count) {
		if(count==r) {
			for(int i=0;i<arr.length;i++) {
				arr[i] = rarr[i].clone();
				narr[i] = rarr[i].clone();
			}
			
			for(int p=0;p<K;p++) {
				int r = a[p][0];
				int c = a[p][1];
				int s = a[p][2];
				
				int cnt = (2*s+1)*(2*s+1)-1;
				int sI=r-s, sJ=c-s, eI=r+s, eJ=c+s;
				int ss=r-s, ee=c+s;
				while(cnt>0) {
					for(int j=sJ;j<=eJ;j++) {
						ee=j;
						if(j+1>eJ) {
							break;
						}
						narr[ss][j+1]=arr[ss][j];
						cnt--;
					}
					
					for(int i=sI;i<=eI;i++) {
						ss=i;
						if(i+1>eI) {
							break;
						}
						narr[i+1][ee]=arr[i][ee];
						cnt--;
					}
					
					for(int j=eJ;j>=sJ;j--) {
						ee=j;
						if(j-1<sJ) {
							break;
						}
						narr[ss][j-1]=arr[ss][j];
						cnt--;
					}
					
					for(int i=eI;i>=sI;i--) {
						ss=i;
						if(i-1<sI) {
							break;
						}
						narr[i-1][ee]=arr[i][ee];
						cnt--;
					}
					
					sI++;sJ++;eI--;eJ--;ss++;
				}//end of while
				for(int i=0;i<arr.length;i++) {
					arr[i] = narr[i].clone();
				}
							
			}//end of K
			
			for(int i=0;i<N;i++) {
				for(int j=0;j<M;j++) {
					sum+=arr[i][j];
				}
				min = Math.min(sum, min);
				sum=0;
			}
			
			
			return;
		}
		
		for(int i=0;i<K;i++) {
			if(v[i]==0) {
				v[i]=1;
				a[count]=ck[i];
				permcomb(i,count+1);
				v[i]=0;
			}
		}	
	}	
}
/*
5 6 2
1 2 3 2 5 6
3 8 7 2 1 3
8 2 3 1 4 5
3 4 5 1 1 1
9 3 2 1 4 3
3 4 2
4 2 1*/