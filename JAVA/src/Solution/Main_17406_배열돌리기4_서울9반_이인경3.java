package Solution;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_17406_배열돌리기4_서울9반_이인경3 {
	public static int[] v;
	public static ArrayList<int[]> al;
	public static int[][] ck, a;
	public static int r, K;
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		K = Integer.parseInt(st.nextToken());
		int min=Integer.MAX_VALUE;
		int sum=0;
		
		int[][] arr = new int[N][M];
		for(int i=0;i<N;i++) {
			st = new StringTokenizer(br.readLine());
			for(int j=0;j<M;j++) {
				arr[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		
		ck = new int[K][3];
		al = new ArrayList<>();
		for(int kk=0;kk<K;kk++) {
			st = new StringTokenizer(br.readLine());
			ck[kk] = new int[] {Integer.parseInt(st.nextToken())-1,Integer.parseInt(st.nextToken())-1,Integer.parseInt(st.nextToken())};
			//al.add(ck);		
		}
			
		v=new int[K];
		r=K;
		a=new int[K][3];
		//for(int i=0;i<K;i++) {
			permcomb(0,0);
		//}
		
		
	}

	private static void permcomb(int start, int count) {
		if(count==r) {
			
			return;
		}
		
		for(int i=0;i<K;i++) {
			if(v[i]==0) {
				v[i]=1;
				a[count]=ck[i];
				permcomb(i,count+1);
				v[i]=0;
			}
		}	
	}	
}
/*
5 6 2
1 2 3 2 5 6
3 8 7 2 1 3
8 2 3 1 4 5
3 4 5 1 1 1
9 3 2 1 4 3
3 4 2
4 2 1*/