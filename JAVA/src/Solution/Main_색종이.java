package Solution;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main_������ {
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		int[][] land = new int[100][100];
		int cnt=0;
		
		for(int i=0;i<T;i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			int a = Integer.parseInt(st.nextToken());
			int b = Integer.parseInt(st.nextToken());
			
			for(int x=a;x<a+10;x++) {
				for(int y=b;y<b+10;y++) {
					land[x][y]=1;
				}
			}
		}
		for(int x=0;x<100;x++) {
			for(int y=0;y<100;y++) {
				if(land[x][y]==1) {
					cnt++;
				}
			}
		}
		System.out.println(cnt);
	}
}
