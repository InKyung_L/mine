package Solution;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_17135_캐슬디펜스_서울9반_이인경 {
	public static int N, M, D, r, kill, max;
	public static int[] v, a, di= {0,-1,0}, dj= {-1,0,1};
	public static int[][] map, visit, temp;
	public static Queue<Point> q;
	public static Point[] p; //적의 위치 저장
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());
		D = Integer.parseInt(st.nextToken());
		
		map = new int[N][M];
		temp = new int[N][M];
		for(int i=0;i<N;i++) {
			st = new StringTokenizer(br.readLine());
			for(int j=0;j<M;j++) {
				map[i][j] = Integer.parseInt(st.nextToken());
			}
		}
		
		r=3;
		a=new int[r];
		v=new int[M];
		max = Integer.MIN_VALUE;
		permcomb(0,0);
		System.out.println(max);
	}

	private static void permcomb(int start, int count) {
		if(count==r) {
			//System.out.println(Arrays.toString(a));
			kill=0;
			attack(); //좌표가 지정이 되었으니 이제 공격을 하러 가볼까?
			//System.out.println(kill);
			max = Math.max(max, kill);
			return;
		}
		
		for(int i=start;i<M;i++) {
			if(v[i]==0) {
				v[i]=1;
				a[count]=i;
				permcomb(i, count+1); //궁수가 위치할 수 있는 모든 경우의 수를 찾으러 가자!
				v[i]=0;
			}
		}
	}

	private static void attack() {
		for(int i=0;i<map.length;i++) {
			temp[i]=map[i].clone(); //사용할 맵을 일단 복사를 해놓고,
		}
		int t=N;
		while(t>0) {
			p = new Point[M];
			for(int i=0;i<r;i++) {
				bfs(a[i]); //궁수 한명이 몇명이나 죽일 수 있는지 알아보러 가보자.
			}
			for(int i=0;i<M;i++) {
				if(p[i]!=null && temp[p[i].x][p[i].y]==1) {
					temp[p[i].x][p[i].y]=0;
					kill++;
				}
			}
			for(int i=N-2;i>=0;i--) { //궁수들이 아래로 움직임.
				for(int j=0;j<M;j++) {
					temp[i+1][j]=temp[i][j];
				}
			}			
			for(int j=0;j<M;j++) { //빈 곳은 0으로 채우자.
				temp[0][j]=0;
			}
			t--;
			//System.out.println(kill);
		}
		
	}

	private static void bfs(int i) {
		q = new LinkedList<Point>();
		visit = new int[N][M];
		q.offer(new Point(N-1,i)); //궁수가 제일 아래 있으니 N-1시작은 고정. (x좌표와 y좌표를 헷갈리지 말 것!)
		visit[N-1][i]=1;
		
		while(!q.isEmpty()) {
			int x = q.peek().x;
			int y = q.poll().y;
			
			if(temp[x][y]==1 && visit[x][y]<=D) { //적이 있고, 명시된 거리보다 depth가 작다면 조사.
				if(p[i]==null) { //적의 위치를 저장하는 배열이 비어있으면 일단 넣고, 
					p[i]=new Point(x,y);
				}else if(p[i]!=null && y<p[i].y && visit[x][y]==visit[p[i].x][p[i].y]) { //빈 배열이 아니고 저장된 좌표보다 왼쪽에 있고 depth도 같다면 넣자
					p[i]=new Point(x,y);
				}
			}
			
			for(int d=0; d<di.length; d++) {
				int nx = x+di[d];
				int ny = y+dj[d];
				
				if(nx>=0 && nx<N && ny>=0 && ny<M && visit[nx][ny]==0) {
					visit[nx][ny]=visit[x][y]+1;
					q.offer(new Point(nx,ny));
				}
			}
		}
	}
}