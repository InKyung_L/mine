package Solution;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_백준_16236_아기상어 {
	public static int si,sj, shark, N, cnt, dep, ans, ck;
	public static int[][] sea, visit;
	public static int[] di= {-1,1,0,0}, dj= {0,0,-1,1};
	public static Queue<Point> q; 
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		N = Integer.parseInt(br.readLine());
		sea = new int[N][N];				
		for(int i=0;i<N;i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int j=0;j<N;j++) {
				sea[i][j] = Integer.parseInt(st.nextToken());
				if(sea[i][j]==9) {
					si=i; sj=j;
				}
			}
		}
		shark=2;
		visit = new int[N][N];
		q = new LinkedList<>();
		while(cnt>=0) {
			cnt=bfs(si,sj);
			if(cnt > 0)
				ans += cnt;
		}		
		System.out.println(ans);
	}
	
	private static int bfs(int x, int y) {		
		visit = new int[N][N];
		if(sea[x][y]==9) sea[x][y]=0;
		visit[x][y]=1;
		q.add(new Point(x,y));
		int ni=0, nj=0;
		int fishck=0, fishi=N, fishj=N; //다 돌고 나왔을 때를 확인해주기 위해서 일단 N으로 저장. 먹을 게 있다면 갱신이 될거니까.
		
	loop:while(!q.isEmpty()) {
			int i = q.peek().x;
			int j = q.poll().y;
			for(int d=0;d<di.length;d++) {
				ni = i+di[d];
				nj = j+dj[d];
				if(ni>=0 && ni<N && nj>=0 && nj<N && visit[ni][nj]==0 && shark>=sea[ni][nj]) {
					System.out.println(ni + " " + nj);
					visit[ni][nj]=visit[i][j]+1;
					q.add(new Point(ni,nj));					
					
					if(fishck==0 && sea[ni][nj]!=0 && shark>sea[ni][nj]) {
						//일단 조건에 맞는 depth, 좌표 저장.
						fishck=visit[ni][nj];
						fishi=ni;
						fishj=nj;
						
					}else if(fishck==visit[ni][nj] && sea[ni][nj]!=0 && shark>sea[ni][nj]){
						//기존에 저장한 좌표와 새로 들어오는 좌표중에서 어떤게 더 작은지
						if(fishi > ni) { //depth가 같은데 x좌표가 다르면 위에 꺼
							fishi=ni;
							fishj=nj;
						}else if(fishi==ni) { //depth가 같은데 x도 같다면 왼쪽 꺼
							if(fishj>nj) {
								fishj=nj;
							}
						}						
					}else if(fishck>visit[ni][nj]) { //저장해놓은 depth가 이제 방문하는 depth보다 크면 이미 방문 완료했으므로 나가자.
						break loop;
					}
				}else if(ni>=0 && ni<N && nj>=0 && nj<N && visit[ni][nj]==0 && shark<sea[ni][nj]) {
					visit[ni][nj]=-1;//조건에는 안맞지만 계속 탐색하기를 방지하기 위함.
				}
			}		
		}	
		if(fishi != N && fishj != N) { //다 돌고 나왔는데 저장된 좌표가 N,N이 아니면(N이라면 다돌고 나왔는데도 먹을게 없는 경우)
			sea[fishi][fishj]=0; //저장된 제일 작은 좌표에서부터 다시 시작.
			visit = new int[N][N];
			visit[fishi][fishj]=1;
			ck++;
			q.clear();
			q.add(new Point(fishi,fishj));
			si=fishi; sj=fishj;
			if(ck==shark) {
				shark++;
				ck=0;
			}
		}
		return fishck-1;
	}
}