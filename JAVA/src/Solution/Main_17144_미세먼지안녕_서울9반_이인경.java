package Solution;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main_17144_미세먼지안녕_서울9반_이인경 {
	public static Queue<Point> q;
	public static int R,C,T;
	public static int[] di= {-1,1,0,0}, dj= {0,0,-1,1};
	public static int[][] home, dust, clean;

	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		R = Integer.parseInt(st.nextToken());
		C = Integer.parseInt(st.nextToken());
		T = Integer.parseInt(st.nextToken());
		q = new LinkedList<>();
		home = new int[R][C];
		dust = new int[R][C];
		clean = new int[2][1];
		int cn=0;
		for(int i=0;i<R;i++) { //배열 채우기 및 값 찾기
			st = new StringTokenizer(br.readLine());
			for(int j=0;j<C;j++) {
				home[i][j] = Integer.parseInt(st.nextToken());
				if(home[i][j]==-1) {
					clean[cn++][0]=i;
				}
				
			}
		}
		
		for(int k=0;k<T;k++) { //정해진 횟수만큼 돌아보자!
			for(int r=0;r<R;r++) {
				for(int c=0;c<C;c++) {
					if(home[r][c]!=-1) {
						q.add(new Point(r,c));
					}
				}
			}		
			
			spread();		
			for(int i=0;i<dust.length;i++) {
				home[i] = dust[i].clone();
			}
			
			refresh();
			for(int i=0;i<dust.length;i++) {
				home[i] = dust[i].clone();
			}
			dust = new int[R][C];		
	}
		
		int sum=0;
		for(int r=0;r<R;r++) {
			for(int c=0;c<C;c++) {
				sum+=home[r][c];
			}
		}
			
		System.out.println(sum);
		
/*		for(int i=0;i<R;i++) {
			for(int j=0;j<C;j++) {
				System.out.print(dust[i][j]+ " ");
			}System.out.println();
		}*/
	}//end of main

	private static void spread() {
		while(!q.isEmpty()) {
			int x = q.peek().x;
			int y = q.poll().y;

			int dt = home[x][y]/5;
			for(int d=0;d<di.length;d++) {
				int nx = x+di[d];
				int ny = y+dj[d];
							
				if(nx>=0 && nx<R && ny>=0 && ny<C && home[nx][ny]!=-1) {
					dust[nx][ny] += dt;
					home[x][y]-=dt;
					
				}
			}
			dust[x][y]+=home[x][y];
		}		
	}
	
	private static void refresh() {
		int cnt1= 2*C + 2*(clean[0][0]+1)-6; //상공기
		while(cnt1>0) {
			for(int sj=1;sj<C;sj++) { //우
				int si=clean[0][0];
				if(sj+1>=C) break;
				dust[si][sj+1]=home[si][sj];
				cnt1--;
			}
			
			for(int si=clean[0][0];si>=0;si--) { //상
				int sj=C-1;
				if(si-1<0) break;
				dust[si-1][sj]=home[si][sj];
				cnt1--;
			}
			
			for(int sj=C-1;sj>=0;sj--) { //좌
				int si=0;
				if(sj-1<0)break;
				dust[si][sj-1]=home[si][sj];
				cnt1--;
			}
			
			for(int si=0;si<clean[0][0];si++) { //하
				int sj=0;
				if(si+1>=clean[0][0]) break;
				dust[si+1][sj]=home[si][sj];
				cnt1--;
			}
			//dust[clean[0][0]][1]=home[clean[0][0]-1][0];
			dust[clean[0][0]][1]=0;
		}
		
		
		int cnt2= 2*C + 2*(R-clean[1][0])-6; //하공기
		while(cnt2>0) {
			for(int sj=1;sj<C;sj++) { //우
				int si=clean[1][0];
				if(sj+1>=C) break;
				dust[si][sj+1]=home[si][sj];
				cnt2--;
			}

			for(int si=clean[1][0];si<R;si++) { //하
				int sj=C-1;
				if(si+1>=R) break;
				dust[si+1][sj]=home[si][sj];
				cnt2--;
			}

			for(int sj=C-1;sj>=0;sj--) { //좌
				int si=R-1;
				if(sj-1<0)break;
				dust[si][sj-1]=home[si][sj];
				cnt2--;
			}
			
			for(int si=R-1;si>=clean[1][0];si--) { //상
				int sj=0;
				if(si-1<0) break;
				dust[si-1][sj]=home[si][sj];
				cnt2--;
			}			
			//dust[clean[1][0]][1]=home[clean[1][0]+1][0];
			dust[clean[1][0]][1]=0;
		}
		dust[clean[0][0]][0]=0;
		dust[clean[1][0]][0]=0;
	}
}//end of class
