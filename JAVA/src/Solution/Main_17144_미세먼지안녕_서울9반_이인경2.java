package Solution;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Main_17144_미세먼지안녕_서울9반_이인경2 {

	public static void main(String[] args) throws Exception{
		Scanner sc = new Scanner(System.in);
		
		int R = sc.nextInt();
		int C = sc.nextInt();
		int T = sc.nextInt();
		int[][] room = new int[R][C];
		
		int air=0;
		for(int r=0;r<R;r++) {
			for(int c=0;c<C;c++) {
				room[r][c]=sc.nextInt();
				if(room[r][c]==-1) {
					//여기 해결할 것
				}
			}
		}
		air--;
		
		int[] dr= {1,-1,0,0};
		int[] dc= {0,0,-1,1};
		
		for(int t=0;t<T;t++) {
			int[][] toom=new int[R][C];
			for(int r=0;r<R;r++) {
				for(int c=0;c<C;c++) {
					if(room[r][c]!=0 && room[r][c]!=-1) {
						int cnt=0;
						for(int d=0;d<4;d++) {
							int nr=r+dr[d];
							int nc=c+dc[d];					
							if(nr>=0 && nr<R && nc>=0 && nc<C && room[nr][nc]!=-1) {
								cnt++;
								toom[nr][nc]+=room[r][c]/5;
							}
						}
						room[r][c]-=(room[r][c]/5)*cnt;
					}
				}
			}
			
			for(int r=0;r<R;r++) {
				for(int c=0;c<C;c++) {
					room[r][c]+=toom[r][c];
				}
			}
			for(int i=air-1;i>=1;i--) room[i][0]=room[i-1][0];
			for(int i=0;i<C-1;i++) room[0][i]=room[0][i+1];
			for(int i=0;i<air;i++) room[i][C-1]=room[i+1][C-1];
			for(int i=C-1;i>=1;i--) room[air][i]=room[air][i-1];
			room[air][1]=0;
			
			for(int i=air+2;i<R-1;i++) room[i][0]=room[i+1][0];
			for(int i=0;i<C-1;i++) room[R-1][0]=room[R-1][i+1];
			for(int i=R-1;i>air+1;i--) room[i][C-1]=room[i-1][C-1];
			for(int i=C-1;i>=1;i--) room[air+1][i]=room[air+1][i-1];
		}
		
	int sum=2;
	for(int r=0;r<R;r++) {
		for(int c=0;c<C;c++) {
			sum+=room[r][c];
		}
			
	}
	
		
	}//end of main
}//end of class
