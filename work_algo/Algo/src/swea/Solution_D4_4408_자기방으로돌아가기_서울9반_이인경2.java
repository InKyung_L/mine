package swea;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;


class Solution_D4_4408_자기방으로돌아가기_서울9반_이인경2{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_4408.txt"));
		Scanner sc = new Scanner(System.in);
		ArrayList<int[]> al = new ArrayList<>();
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int N = sc.nextInt();
			int[] room = new int[3];
			int max = 0;
			
			for(int i=0;i<N;i++) {
				room = new int[3];
				room[0] = sc.nextInt();
				room[1] = sc.nextInt();
				
				al.add(room);
			}
			Collections.sort(al, new Comparator<int[]>() {
				@Override
				public int compare(int[] o1, int[] o2) {
					return o1[0]>o2[0]?1:-1;
				}
			});
			
			int i=0;
			while(i<N) {
				for(int j=i+1;j<N;j++) {
					if(al.get(i)[0]<al.get(i)[1]) { //현재 작은방 -> 큰방
						if(al.get(j)[0]<al.get(j)[1]) { //뒤에 작은방 -> 큰방
							if(al.get(j)[0]>=al.get(i)[0] && al.get(j)[0]<=al.get(i)[1]+1) {
								al.get(j)[2]=al.get(i)[2]+1;
								max = Math.max(max, al.get(j)[2]);
							}
						}else { //뒤에 큰방 -> 작은방
							if(al.get(j)[1]>=al.get(i)[0] && al.get(j)[1]<=al.get(i)[1]+1) {
								al.get(j)[2]=al.get(i)[2]+1;
								max = Math.max(max, al.get(j)[2]);
							}
						}
						
					}else { //큰방 -> 작은방
						if(al.get(j)[0]<al.get(j)[1]) { //뒤에 애를 보자
							if(al.get(j)[1]>=al.get(i)[1] && al.get(j)[1]<=al.get(i)[0]+1) {
								al.get(j)[2]=al.get(i)[2]+1;
								max = Math.max(max, al.get(j)[2]);
							}
						}else {
							if(al.get(j)[1]>=al.get(i)[1] && al.get(j)[1]<=al.get(i)[0]+1) {
								al.get(j)[2]=al.get(i)[2]+1;
								max = Math.max(max, al.get(j)[2]);
							}
						}
					}
					
				}//end of j
				i++;
			}		
			System.out.println("#" + tc + " " + (max+1));
		}
	}
}