package swea;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Solution_D3_1873_상호의배틀필드_서울9반_이인경3 {
	static int H, W;
	static char[][] map;
	static int x, y;
	static char sem;
	
	static void findxy() {
		for(int i=0; i<H; i++) {
			for(int j=0; j<W; j++) {
				switch(map[i][j]) {
				case '^':
				case 'v':
				case '<':
				case '>':
					sem=map[i][j];
					x=i;
					y=j;
					return;
				}
			}
		}
	}
	static void move(char c) {
		int tx=x, ty=y;
		switch(c) {
		case 'U': tx--; sem='^'; break;
		case 'D': tx++; sem='v'; break;
		case 'L': ty--; sem='<'; break;
		case 'R': ty++; sem='>'; break;
		}
		if( tx < 0 ) tx = 0;
		else if( tx > H-1 ) tx = H-1;
		if( ty < 0 ) ty = 0;
		else if( ty > W-1 ) ty = W-1;
		if( map[tx][ty] == '.' ) {
			map[x][y] = '.';
			map[tx][ty] = sem;
			x = tx;
			y = ty;
			return;
		}
		map[x][y] = sem;
	}
	static void shoot() {
		int sx = x, sy = y;
		int dx=0, dy=0;
		switch(sem) {
			case '^': dy = -1; break;
			case 'v': dy =  1; break;
			case '<': dx = -1; break;
			case '>': dx =  1; break;
		}
		while(true) {
			sx += dx;
			sy += dy;
			if( sx < 0 || sx > W-1 ) return;
			if( sy < 0 || sy > H-1 ) return;
			if( map[sx][sy] == '#') return;
			if( map[sx][sy] == '*') {
				map[sx][sy] = '.';
				return;
			}
		}
	}
	static void start(char c) {
		if( c == 'S') {
			shoot();
			return;
		}
		else
			move(c);
	}
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1873.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=T; tc++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			H = Integer.parseInt(st.nextToken());
			W = Integer.parseInt(st.nextToken());
			map = new char[H][W];
			
			for(int i=0; i<H; i++) {
				String mp = br.readLine();
				for(int j=0;j<W;j++) {
					map[i][j] = mp.charAt(j);
				}
			}
			
			int N = Integer.parseInt(br.readLine());
			String command = br.readLine();
			
			findxy();
			for(int i=0; i<N; i++) {
				start(command.charAt(i));
			}
			
			System.out.print("#" + tc + " ");		
			for(int i=0; i<H; i++) {
				for(int j=0; j<W; j++) {
					System.out.print(map[i][j]);
				}
				System.out.println();
			}
		}
	}
}