package swea;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D3_2806_NQueen_서울9반_이인경{
	
	public static int N;
	public static int result;
	public static int[] col;

	public static boolean promising(int i) {
		for(int j=0;j<i;j++) {
			if(col[j] == col[i] || Math.abs(col[i]-col[j]) == (i-j)) return false; 
		}
		return true;
	}
	
	public static void nqueen(int i) {
		if(i == N) {
			result++;
			return;
		}
		for(int j=0;j<N;j++) {
			col[i] = j;
			if(promising(i)) nqueen(i+1);
		}
		
	}
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_2806.txt"));

		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			N = sc.nextInt();
			col = new int[N];
			nqueen(0);
			System.out.println("#" + tc + " " + result);
			result = 0;
		}
		
	}
}