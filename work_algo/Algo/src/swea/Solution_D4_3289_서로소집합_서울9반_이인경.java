package swea;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

class Solution_D4_3289_서로소집합_서울9반_이인경{
	public static int getParent(int[] p, int x) {
		if(p[x]==x) return x;
		else return p[x]=getParent(p, p[x]);
	}
	
	public static void unionParent(int[] p, int a, int b) {
		a=getParent(p, a);
		b=getParent(p, b);
		if(a<b) p[b]=a;
		else p[a]=b;
	}
	
	public static boolean findParent(int[] p, int a, int b) {
		a=getParent(p, a);
		b=getParent(p, b);
		if(a==b) return true;
		else return false;
	}
		
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_3289.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		int T = Integer.parseInt(br.readLine());
		for(int tc=1;tc<=T;tc++) {
			StringTokenizer s = new StringTokenizer(br.readLine());
			int N = Integer.parseInt(s.nextToken());
			int M = Integer.parseInt(s.nextToken());
			ArrayList<Integer> al = new ArrayList<>();
			
			int[] p = new int[N];
			for(int i=0;i<N;i++) p[i]=i;
			
			for(int i=0;i<M;i++) {
				StringTokenizer st = new StringTokenizer(br.readLine());
				int z = Integer.parseInt(st.nextToken());
				int a = Integer.parseInt(st.nextToken());
				int b = Integer.parseInt(st.nextToken());
				
				if(z==1) {
					if(!findParent(p, a-1, b-1)) {
						al.add(0);
					}else {
						al.add(1);
					}
				}
				else {
					unionParent(p, a-1, b-1);
				}
			}
			System.out.print("#" + tc + " ");
			for(int v:al) System.out.print(v);
			System.out.println();
		}
		
	}

}