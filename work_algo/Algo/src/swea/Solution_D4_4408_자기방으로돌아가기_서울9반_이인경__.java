package swea;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

class Solution_D4_4408_자기방으로돌아가기_서울9반_이인경{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_4408.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			ArrayList<int[]> al = new ArrayList<>();
			int N = sc.nextInt();
			int[] room = new int[3];
			int max = 0;
			
			for(int i=0;i<N;i++) {
				room = new int[3];
				room[0] = sc.nextInt();
				room[1] = sc.nextInt();
				
				if(room[0]%2 == 0) {
					room[0]-=1;
				}
				if(room[1]%2 == 0) {
					room[1]-=1;
				}
				
				int temp=0;
				if(room[0]>room[1]) {
					temp=room[0];
					room[0]=room[1];
					room[1]=temp;
				}
				
				al.add(room);
			}
			Collections.sort(al, new Comparator<int[]>() {
				@Override
				public int compare(int[] o1, int[] o2) {
					return o1[0]>o2[0]?1:-1;
				}
			});
 			int i=0;
			while(i<N) {
				for(int j=i+1;j<N;j++) {
					if(al.get(j)[0]>=al.get(i)[0] && al.get(j)[0]<=al.get(i)[1]) {
						al.get(j)[2]=al.get(i)[2]+1;
						max = Math.max(max, al.get(j)[2]);
					}
				}
				i++;
			}		
			System.out.println("#" + tc + " " + (max+1));
		}
	}
}