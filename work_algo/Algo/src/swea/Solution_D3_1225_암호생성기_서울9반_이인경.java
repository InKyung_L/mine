package swea;

import java.io.FileInputStream;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Solution_D3_1225_암호생성기_서울9반_이인경 {
	
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1225.txt"));
		Scanner sc = new Scanner(System.in);
		int mnum=1;
		int num=0;

		for(int tc = 1; tc <= 10; tc++){
			int ncase = sc.nextInt();
			Queue<Integer> queue = new LinkedList<Integer>();
			
			for(int i=0;i<8;i++) {
				queue.offer(sc.nextInt());
			}
			
here:		while(queue.peek() > 0) {
				for(mnum=1;mnum<=5;mnum++) {
					num = queue.peek();

					if(queue.peek()-mnum <= 0) {
						queue.poll();
						queue.offer(0);
						break here;
					}
					
					queue.offer(queue.peek()-mnum);
					queue.poll();
					
					
				}				
			}
			System.out.print("#" + tc + " ");
			for(int i=0;i<8;i++) {
				System.out.print(queue.poll() + " ");
			}
			System.out.println();
		}
	}
}