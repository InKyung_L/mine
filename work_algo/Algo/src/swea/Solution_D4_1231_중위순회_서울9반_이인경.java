package swea;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;


class Solution_D4_1231_중위순회_서울9반_이인경{
	public static String[] arr;
	public static int T;
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1231.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		
		
		for(int tc=1;tc<=10;tc++) {
			T = Integer.parseInt(br.readLine());
			arr = new String[T+1];
			
			for(int i=0;i<T;i++) {
				String[] sen = br.readLine().split(" ");
				arr[i+1] = sen[1];
			}

			System.out.print("#" + tc + " ");
			inorder(1);
			System.out.println();
		}			
	}
	
	public static void inorder(int i) {
		if(i<=T && arr[i] != "0") { 
			inorder(2*i);
			System.out.print(arr[i]);
			inorder(2*i+1);
		}
	}
}