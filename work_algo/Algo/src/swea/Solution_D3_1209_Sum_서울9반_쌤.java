package swea;
import java.util.*;
import java.io.FileInputStream;


class Solution_D3_1209_Sum_서울9반_쌤{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1209.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int tc=1;tc<=10;tc++) {
			int t=sc.nextInt();
			int N=100;
			int[][] a = new int[N][N];
			int max=-1;
			
			for(int i=0;i<N;i++) {
				int sum=0;
				for(int j=0;j<N;j++) {
					a[i][j]=sc.nextInt();
					sum+=a[i][j];
				}
				if(max<sum) max=sum;
			}
			

			int sum1=0,sum2=0;
			
			for(int i=0;i<N;i++) {
				int sum=0;
				for(int j=0;j<N;j++) {
					sum += a[j][i];
					if(i==j) sum1 +=a[i][j];
					if((i+j) == (N-1)) sum2 += a[i][j];
				}
				if(max<sum) max=sum;
			}
			if(max<sum1) max=sum1;
			if(max<sum2) max=sum2;

			System.out.println("#" + tc + " " + max);
		}
	}
}