package swea;
import java.io.FileInputStream;
import java.util.LinkedList;
import java.util.Scanner;

class Solution_D3_1230_암호문3_서울9반_이인경{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1230.txt"));
		Scanner sc = new Scanner(System.in);
		
		 for(int tc=1;tc<=10;tc++) {
	            LinkedList<Integer> ll = new LinkedList<Integer>();
	            String cal;
	            int x=0; int y=0; int s=0;
	            int wcount = sc.nextInt();
	            for(int i=0;i<wcount;i++) {
	                ll.add(i, sc.nextInt());
	            }
	             
	            int ccount = sc.nextInt();
	            for(int i=0;i<ccount;i++) {
	                cal = sc.next();
	                 
	                switch (cal) {
	                case "I":
	                    x = sc.nextInt();
	                    y = sc.nextInt();
	                     
	                    for(int v=0;v<y;v++) {
	                        ll.add(x, sc.nextInt());
	                        x++;
	                    }
	                break;
	 
	                case "D":
	                    x = sc.nextInt();
	                    y = sc.nextInt();
	                     
	                    for(int v=0;v<y;v++) {
	                        ll.remove(x);
	                    }
	                    break;
	                     
	                case "A":
	                    y = sc.nextInt();
	                     
	                    for(int v=0;v<y;v++) {
	                        ll.add(sc.nextInt());
	                    }
	                    break;
	                }
	            }
	            System.out.print("#" + tc + " ");
	            for(int i=0;i<10;i++) {
	                System.out.print(ll.pop() + " ");
	            }
	            System.out.println();
	        }
	    }
	}