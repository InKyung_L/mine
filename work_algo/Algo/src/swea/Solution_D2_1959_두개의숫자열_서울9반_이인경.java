package swea;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D2_1959_두개의숫자열_서울9반_이인경{
	static int N, M, max,sum;
	static int[] A;
	static int[] B;
	
	public static void mul(int count){
		if(N<M) {
			for(int i=0;i<N;i++) {
				if(count+N > M) return;
				sum += A[i]*B[i+count];
			}
			if(sum>max) {
				max = sum;
			}
			sum=0;
			mul(count+1);
		}
		else {
			for(int i=0;i<M;i++) {
				if(count+M > N) return;
				sum += A[i+count]*B[i];
			}
			if(sum>max) {
				max = sum;
			}
			sum=0;
			mul(count+1);
		}
		
	}
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d2_1959.txt"));

		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		int w=0;
		for(int tc=1;tc<=T;tc++) {
			max=0; sum=0;
			N = sc.nextInt();
			M = sc.nextInt();
			A = new int[N];
			B = new int[M];
			
			for(int i=0;i<N;i++) {
				A[i] = sc.nextInt();
			}
			for(int j=0;j<M;j++) {
				B[j] = sc.nextInt();
			}		
		
			mul(0);		
			System.out.println("#" + tc + " " + max);
		}
		
		
	}
}