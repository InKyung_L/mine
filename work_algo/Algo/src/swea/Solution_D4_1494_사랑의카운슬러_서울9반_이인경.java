package swea;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.StringTokenizer;

class Solution_D4_1494_사랑의카운슬러_서울9반_이인경{
	public static int N, r, n;
	public static long vector, min;
	public static int[][] worm, a;
	public static int[] visit;
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_1494.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=T; tc++) {
			N = Integer.parseInt(br.readLine());
			worm = new int[N][2];
			min=Long.MAX_VALUE;
			
			for(int i=0;i<N;i++) {
				StringTokenizer st = new StringTokenizer(br.readLine());
				worm[i][0] = Integer.parseInt(st.nextToken());
				worm[i][1] = Integer.parseInt(st.nextToken());
			}
			r=N/2;
			visit=new int[N];
			a = new int[N][2];
			permcomb(0,0);
			
			System.out.println("#" + tc + " " + min);
		}
		
	}
	private static void permcomb(int start, int count) {
		int i=0;
 		if(count==N) {
			/*for(int s=0;s<a.length;s++) {
				System.out.println(Arrays.toString(a[s]));
			}System.out.println();*/
			long sumx=0, sumy=0;
			int S=0, K=S+1;
			while(S<a.length) {
				sumx+=a[K][0]-a[S][0];
				sumy+=a[K][1]-a[S][1];
				K+=2; S+=2;
			}

			vector=(sumx*sumx) + (sumy*sumy);
			min = Math.min(vector, min);
			return;
		}
		
		for(i=0;i<N;i++) {
			if(visit[i]==0) {
				visit[i]=1;
				a[count]=worm[i];
				permcomb(i,count+1);
				visit[i]=0;
			}
		}
	}
}