package swea;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.StringTokenizer;

public class Main_정올_1863_종교_서울9반_이인경 {
	public static int getParent(int[] p, int x) {
		if(p[x]==x) return x;
		else return p[x]=getParent(p, p[x]);
	}
	
	public static void unionParent(int[] p, int a, int b) {
		a=getParent(p, a);
		b=getParent(p, b);
//		if(a<b) p[b]=a;
//		else p[a]=b;
	}

	public static void main(String args[]) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		StringTokenizer st = new StringTokenizer(br.readLine());
		int n = Integer.parseInt(st.nextToken());
		int m = Integer.parseInt(st.nextToken());
		
		int[] p = new int[n];
		for(int i=0;i<n;i++) p[i]=i;
		
		for(int i=0; i<m;i++) {
			st = new StringTokenizer(br.readLine());
			int a = Integer.parseInt(st.nextToken());
			int b = Integer.parseInt(st.nextToken());		
			
			unionParent(p, a-1, b-1);
			
		}
		//System.out.println(Arrays.toString(p));
		HashSet<Integer> hs = new HashSet<>();
		for(int i=0;i<n;i++) {
			hs.add(getParent(p, i));
		}
		System.out.println(hs.size());
		
	}
}
