package swea;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Solution_D4_1861_정사각형방_서울9반_이인경{
	public static int[] di= {1,-1,0,0}, dj= {0,0,1,-1}, rm;
	public static int[][] room, visit;
	public static ArrayList<int[]> al;
	public static int max, mx, N, cnt, min;
	
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_1861.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1;tc<=T;tc++) {
			N = Integer.parseInt(br.readLine());
			room = new int[N][N];
			for(int i=0;i<N;i++) {
				StringTokenizer st = new StringTokenizer(br.readLine());
				for(int j=0;j<N;j++) {
					room[i][j]=Integer.parseInt(st.nextToken());
				}
			}
			
			al=new ArrayList<>();
			max = 0; mx=0;
			min = Integer.MAX_VALUE;
			rm = new int[2]; //방번호와 cnt값
			for(int i=0;i<N;i++) {
				for(int j=0;j<N;j++) {
					visit = new int[N][N];
					cnt=1;
					dfs(i,j);
					if(max==cnt) {
						if(max>mx) {
							mx=max;
							rm[0]=room[i][j];
							min=rm[0];
						}else if(max==mx){
							min = Math.min(min, room[i][j]);
							rm[0]=min;
						}
					}					
				}
			}
			System.out.println("#" + tc + " " + rm[0] + " "+ max);
		}
	}

	private static void dfs(int i, int j) {
		visit[i][j]=1;
		
		for(int d=0;d<di.length;d++) {
			int ni = i+di[d];
			int nj = j+dj[d];
			
			if(ni>=0 && ni<N && nj>=0 && nj<N && visit[ni][nj]==0 && room[ni][nj]-room[i][j]==1) {
				visit[ni][nj]=1;				
				cnt++;			
				max = Math.max(max, cnt);
				rm[1]=max;
				dfs(ni,nj);
			}
		}
	}
}