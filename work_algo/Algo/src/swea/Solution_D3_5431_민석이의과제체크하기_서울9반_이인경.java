package swea;
import java.util.*;
import java.io.FileInputStream;


class Solution_D3_5431_민석이의과제체크하기_서울9반_이인경{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_5431.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=0;tc<T;tc++) {
			int[] list = new int[2];
			for(int i=0;i<2;i++) {
				list[i] = sc.nextInt();
			}
			int[] sub_no = new int[list[1]];
			int[] total = new int[list[0]];
			int[] yet = new int[list[0]-list[1]];
			Arrays.fill(total, 0);
			for(int i=0;i<list[1];i++) {
				sub_no[i] = sc.nextInt();
			}
			
			for(int i=0;i<list[1];i++) {
				total[sub_no[i]-1] = sub_no[i];
			}
			
			System.out.print("#"+ (tc+1) + " ");
			for(int i=0;i<list[0];i++) {
				if(total[i] == 0) {
					System.out.print(i+1 + " ");
				}
			}
			System.out.println();
			//for(int v:total) System.out.println(v);
		}
		
	}
}