package swea;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main_백준_2667_단지번호붙이기_Dfs_서울9반_이인경 {
	public static ArrayList<Integer> al;
	public static int[][] vil, visit;
	public static int N,dan;
	public static int[] di= {0,0,1,-1}, dj= {1,-1,0,0};
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		N = sc.nextInt();
		vil = new int[N][N];
		visit = new int[N][N];
		
		for(int i=0;i<N;i++) {
			String v = sc.next();
			for(int j=0;j<N;j++) {
				vil[i][j]=v.charAt(j)-'0';
			}
		}
		
		al = new ArrayList<>();
		for(int i=0;i<N;i++) {
			for(int j=0;j<N;j++) {
				if(vil[i][j]==1 && visit[i][j]==0) {
					al.add(dfs(i,j,1));
					dan++;
				}
			}
		}
		System.out.println(dan);
		Collections.sort(al);
		for(int v:al) System.out.println(v);
	}
	
	private static int dfs(int i, int j, int dep) {
		visit[i][j]=1;
		
		for(int d=0;d<di.length;d++) {
			int ni=i+di[d];
			int nj=j+dj[d];
			
			if(ni>=0 && ni<N && nj>=0 && nj<N && visit[ni][nj]==0 && vil[ni][nj]==1) {
				visit[ni][nj]=1;
				dep = dfs(ni,nj,dep+1);
			}

		}
		return dep;
	}

}
