package swea;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D3_2817_부분수열의합_서울9반_이인경 {
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_2817.txt"));
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();

		for(int tc = 0; tc < T; tc++){
			int cnt=0;
			int N = sc.nextInt();
			int K = sc.nextInt();
			int[] A = new int[N];
			for(int i=0;i<N;i++) {
				A[i] = sc.nextInt();
			}
			
			for(int i=0;i<(1<<A.length); i++) {
				int sum=0;
				for(int j=0;j<A.length;j++) {
					if((i&(1<<j))>0) {
						sum = sum+A[j];					
					}		
				}
				if(sum == K)
					cnt++;	

			}			
			System.out.println("#" + (tc+1) + " " +cnt);
		}
	}
}