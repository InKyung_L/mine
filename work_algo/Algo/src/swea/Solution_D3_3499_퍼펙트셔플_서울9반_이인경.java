package swea;
import java.util.*;
import java.io.FileInputStream;


class Solution_D3_3499_퍼펙트셔플_서울9반_이인경{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_3499.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			int n = sc.nextInt();
			String[] arr = new String[n];
			
			for(int i=0;i<n;i+=2) {
				arr[i] = sc.next();
			}
			for(int i=1;i<n;i+=2) {
				arr[i] = sc.next();
			}
			
			System.out.print("#" + tc + " ");
			for(int i=0;i<n;i++) {
				System.out.print(arr[i] + " ");
			}
			System.out.println();
		}
			
	}
}