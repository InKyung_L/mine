package swea;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Main_백준_2667_단지번호붙이기_Bfs_서울9반_이인경 {
	public static ArrayList<Integer> al;
	public static Queue<Point> q;
	public static int[][] vil, visit;
	public static int N,dan;
	public static int[] di= {0,0,1,-1}, dj= {1,-1,0,0};
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		N = sc.nextInt();
		vil = new int[N][N];
		visit = new int[N][N];
		
		for(int i=0;i<N;i++) {
			String v = sc.next();
			for(int j=0;j<N;j++) {
				vil[i][j]=v.charAt(j)-'0';
			}
		}
		
		al = new ArrayList<>();
		for(int i=0;i<N;i++) {
			for(int j=0;j<N;j++) {
				if(vil[i][j]==1 && visit[i][j]==0) {
					al.add(bfs(i,j));
					dan++;
				}
			}
		}
		System.out.println(dan);
		Collections.sort(al);
		for(int v:al) System.out.println(v);
		
	}
	
	private static int bfs(int i, int j) {
		visit[i][j]=1;
		q = new LinkedList<>();
		q.offer(new Point(i,j));
		int cnt=1;
		
		while(!q.isEmpty()) {
			i = q.peek().x;
			j = q.poll().y;
			
			for(int d=0;d<di.length;d++) {
				int ni=i+di[d];
				int nj=j+dj[d];
				
				if(ni>=0 && ni<N && nj>=0 && nj<N && visit[ni][nj]==0 && vil[ni][nj]==1) {
					visit[ni][nj]=1;
					cnt++;
					q.offer(new Point(ni,nj));
				}
			}
		}		
		return cnt;
	}

}
