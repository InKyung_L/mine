package swea;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

class Solution_D4_1494_사랑의카운슬러_서울9반_이인경2 {
	public static int N;
	public static int[] move, arr;
	public static long min;
	public static ArrayList<int[]> al;

	private static void comb(int a, int b) {
		if (b == N / 2) {
			long x = 0,y = 0;

			for (int i = 0; i < N; i++) {
				if (move[i] == 0) {
					x += al.get(i)[0];
					y += al.get(i)[1];
				} else {
					x -= al.get(i)[0];
					y -= al.get(i)[1];
				}
			}
			min = Math.min(min, (x * x + y * y));
			return;
		}

		for (int i = a; i < N; i++) {
			if (move[i] == 0) {
				move[i] = 1;
				arr[b] = i;
				comb(i, b + 1);
				move[i] = 0;
			}
		}
	}

	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_d4_1494.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		for (int tc = 1; tc <= T; tc++) {
			N = Integer.parseInt(br.readLine());
			al = new ArrayList<>();
			arr = new int[N];
			move = new int[N];
			
			for (int i = 0; i < N; i++) {
				StringTokenizer st = new StringTokenizer(br.readLine());
				al.add(new int[] {Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken())});
			}
			min = Long.MAX_VALUE;
			comb(1, 0);

			System.out.println("#" + tc + " " + min);
		}
	}
}