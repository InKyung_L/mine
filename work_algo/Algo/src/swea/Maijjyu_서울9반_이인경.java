package swea;

import java.awt.Point;
import java.util.LinkedList;
import java.util.Queue;

public class Maijjyu_서울9반_이인경 {

	public static void main(String[] args) {
		Queue<Point> q = new LinkedList<Point>();
		int total = 20;
		
		int i=1, j=1;
		int ans=0;
		while(total>=0) {
			q.add(new Point(i,j));
			int x = q.peek().x;
			int y = q.peek().y;
			q.poll();
			total -= y;
			
			q.add(new Point(x, y+1));
			q.add(new Point(x+1,i));
			ans=q.peek().x;			
		}
		System.out.println(ans);
	}

}
