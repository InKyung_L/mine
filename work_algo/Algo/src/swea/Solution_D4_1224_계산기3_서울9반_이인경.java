package swea;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

class Solution_D4_1224_계산기3_서울9반_이인경{
	public static Stack<Character> stack = new Stack<>();
	public static Stack<Integer> stack2 = new Stack<>();
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_1224.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		for(int tc=1;tc<=10;tc++) {
			String snum="";
			String line = br.readLine(); //한 줄을 읽어들임
			String num = br.readLine();
			StringBuilder sb = new StringBuilder();
			
			for(int i=0;i<num.length();i++) {
				char c = num.charAt(i);
				if('0' <= c && c <= '9') {
					sb.append(c);
				}else if(c == ' ') {
					
				}else if(c == ')') {
					char s='\u0000'; //유니코드의 null문자
					while((s=stack.pop()) != '(') {
						sb.append(s);
					}
				}else { //+,-,*,/
					while(getIcp(c) <= getIsp()) {
						sb.append(stack.pop());
					}
					stack.push(c);
				}
			}
			while(!stack.empty()) {
				sb.append(stack.pop());
			}
			snum = sb.toString();		
			
			for(int i=0;i<snum.length();i++) {
				char c = snum.charAt(i);
				if('0' <= c && c <= '9') { //Character.isDigit(c)
					stack2.push(c-'0'); //Character.getnumValue(c)
				}else {
					int n2 = stack2.pop();
					int n1 = stack2.pop();
					int nn=0;
					switch(c) {
					case '+': nn= n1+n2; break;
					case '*': nn= n1*n2; break;
					}
					stack2.push(nn);
				}
			}
			System.out.println("#" + tc + " " + stack2.pop());
		}
	}	
		public static int getIcp(char c) {
			switch (c) {
			case '+':
				return 1;
			case '*':
				return 2;
			case '(':
				return 3;
			default:
				return 0;
			}
		}
		
		public static int getIsp() {
			char c = stack.empty() ? ' ' : stack.peek(); //비었으면 비우고 안비었으면 꼭대기를 리턴
			switch (c) {
			case '+':
				return 1;
			case '*':
				return 2;
			case '(':
				return 0;
			default:
				return 0;
			}
		}
}
	