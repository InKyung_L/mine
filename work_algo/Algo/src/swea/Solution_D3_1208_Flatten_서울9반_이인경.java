package swea;
import java.util.*;
import java.io.FileInputStream;


class Solution_D3_1208_Flatten_서울9반_이인경{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1208.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int tc=1;tc<=10;tc++) {
			int D = sc.nextInt();
			int[] box = new int[100];
			
			for(int t=0;t<100;t++) {
				box[t] = sc.nextInt();
			}
			//Arrays.sort(box);
			
			int min = 1;
			for(int i=0;i<D;i++) {
				Arrays.sort(box);
				min = box[99]-box[0];
				if(min==0) break;
				box[99] -= 1;
				box[0] += 1;
			}
			Arrays.sort(box);
			min = box[99]-box[0];
			System.out.println("#" + tc + " " + min);
		}
	}
}