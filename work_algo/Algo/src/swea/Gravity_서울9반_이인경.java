package swea;

import java.io.FileInputStream;
import java.util.Random;
import java.util.Scanner;

public class Gravity_서울9반_이인경 {

	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/input_gravity.txt")); 
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		int[] box = new int[N];
		
		for(int i=0;i<N;i++) {
			box[i] = sc.nextInt();
		}
		
		int max=0, cur=0;
		for(int i=0;i<N;i++) {
			for(int j=i+1;j<N;j++) {
				if(box[i] > box[j])
					cur++;
			}
			if(cur>=max)
				max=cur;
				cur=0;
		}
		System.out.println(max);
	}
}
