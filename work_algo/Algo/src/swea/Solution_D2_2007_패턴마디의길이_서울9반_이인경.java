package swea;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D2_2007_패턴마디의길이_서울9반_이인경{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d2_2007.txt"));
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();	
		
		for(int tc=1;tc<=T;tc++) {
			int count=0;
			String sen = sc.next();
			
			exit:
			for(int i=0;i<9;i++) {
				for(int j=i+1;j<10;j++) {
					if(sen.charAt(i) == sen.charAt(j)) {
						for(int k=1;k<j;k++) {
							if(sen.charAt(k) == sen.charAt(j+k)) {
								count = j;
								break exit;					
							}else continue;
						}					
					}
				}
			}
			
			System.out.println("#" + tc + " " + count);
		}
		
	}
}