package swea;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.StringTokenizer;

class Solution_D4_1494_사랑의카운슬러_서울9반_이인경3{
	public static int N, r, n;
	public static long vector, min;
	public static int[][] worm, a;
	public static int[] visit;
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_1494.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int T = Integer.parseInt(br.readLine());
		for(int tc=1; tc<=T; tc++) {
			N = Integer.parseInt(br.readLine());
			worm = new int[N][2];
			min=Long.MAX_VALUE;
			
			for(int i=0;i<N;i++) {
				StringTokenizer st = new StringTokenizer(br.readLine());
				worm[i][0] = Integer.parseInt(st.nextToken());
				worm[i][1] = Integer.parseInt(st.nextToken());
			}
			r=N/2;
			visit=new int[N];
			a = new int[N][2];
			comb(0,0);
			
			System.out.println("#" + tc + " " + min);
		}
		
	}
	public static void comb(int n, int r) {
		if(r == 0) {
			System.out.println(Arrays.toString(a));
			return;
		}else if(n<r) return;
		else{
			a[r-1]=worm[n-1];
			comb(n-1,r-1);
			comb(n-1,r);
		}
		
	}
}