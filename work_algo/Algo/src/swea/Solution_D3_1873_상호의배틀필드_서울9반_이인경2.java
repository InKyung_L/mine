package swea;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Solution_D3_1873_상호의배틀필드_서울9반_이인경2 {
	public static char[][] map;
	public static int H, W, x, y;
	public static char sem;
	
	public static void main(String args[]) throws Exception{		
		System.setIn(new FileInputStream("res/input_d3_1873.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int T = Integer.parseInt(br.readLine());
		for(int tc = 1; tc < T; tc++){
			StringTokenizer st = new StringTokenizer(br.readLine());
			H = Integer.parseInt(st.nextToken());
			W = Integer.parseInt(st.nextToken());
			
			map = new char[H][W];
			for(int h=0;h<H;h++) {
				String hw = br.readLine();
				for(int w=0;w<W;w++) {
					map[h][w] = hw.charAt(w);
				}
			}
			int cm = Integer.parseInt(br.readLine());
			char[] com = new char[cm];
			String c = br.readLine();
			for(int i=0;i<cm;i++) {
				com[i] = c.charAt(i);
			}
			start();
			for(int i=0;i<cm;i++) {
				Move(com[i]);
			}
			
			System.out.println("#" + tc + " ");
			for(int h=0;h<H;h++) {
				for(int w=0;w<W;w++) {
					System.out.print(map[h][w]);
				}System.out.println();
			}
		}
	}

	private static void start() {
		for(int h=0;h<H;h++) {
			for(int w=0;w<W;w++) {
				switch (map[h][w]) {
				case '^':
				case 'v':
				case '<':
				case '>':
					sem=map[h][w];
					x=h;
					y=w;
					return;
				}
			}
		}
		
	}

	private static void Move(char c) {
		int tx=x, ty=y;
		 switch (c) {
			case 'U':
				tx--; 
				sem='^';
				break;				
			case 'D':
				tx++; 
				sem='v';
				break;				
			case 'L':
				ty--; 
				sem='<';
				break;				
			case 'R':
				ty++; 
				sem='>';
				break;	
			case 'S':
				shoot(x,y);
				//Move(c);
				break;	
		}
		if(tx<0) tx=0;
		else if(tx>=W) tx=W-1;
		if(ty<0) ty=0;
		else if(ty>=H) ty=H-1;
		
		if(map[tx][ty]=='.') {
			map[x][y]='.';
			map[tx][ty]=sem;
			x=tx;
			y=ty;
		}
		map[tx][ty]=sem;
	}

	private static void shoot(int x, int y) {
		int sx=x, sy=y;
		int dx=0, dy=0;
		switch (sem) {
		case '^':
			dx=-1;
			break;
		case 'v':
			dx=1;
			break;
		case '<':
			dy=-1;
			break;
		case '>':
			dy=1;
			break;
		}
		while(true) {
			sx+=dx;
			sy+=dy;
			if(sx<0 || sx>W-1 || sy<0 || sy>H-1) return;
			if(map[sx][sy]=='#') return;
			if(map[sx][sy]=='*') {
				map[sx][sy]='.';
				return;
			}
		}
		
	}
}