package swea;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;
import java.io.FileInputStream;

class Solution_D4_5432_쇠막대기자르기_서울9반_이인경{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_5432.txt"));
		Scanner sc = new Scanner(System.in);
		Stack<Character> stack = new Stack<>();
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int b = 0;
			String s = sc.next();
			
			stack.push(s.charAt(0));
			for(int i=1;i<s.length();i++) {
				if(s.charAt(i) == '(') {
					stack.push(s.charAt(i));
				}
				else if(s.charAt(i) == ')') {
					if(s.charAt(i-1) == ')') {
						stack.pop();
						b += 1;
					}else if(s.charAt(i-1) == '(') {
						stack.pop();
						b += stack.size();
					}
						
				}
			}
			System.out.println("#" + tc + " " + b);
		}	
	}
}