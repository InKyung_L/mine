package swea;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D2_1989_초심자의회문검사_서울9반_이인경{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d2_1989.txt"));

		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		
		for(int tc=1;tc<=T;tc++) {
			String origin = sc.next();
			int ans=5;
			StringBuilder re = new StringBuilder(origin);
			StringBuilder reverse = re.reverse();
			
			if(origin.equals(reverse.toString())) { //reverse가 string타입이 아니라서 바꿔줘야함
				ans = 1;
			}
			else ans=0;
			
			System.out.println("#" + tc + " " + ans);
		}
		
	}
}