package swea;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;
import java.io.FileInputStream;

class Solution_D4_1218_괄호짝짓기_서울9반_이인경{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_1218.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int tc=1;tc<=10;tc++) {
			int num = sc.nextInt();
			int ans=1;
			Stack<Character> stack = new Stack<>();
			String s = sc.next();
			here: for(int i=0;i<s.length();i++) {
				if(s.charAt(i) == '<' || s.charAt(i) == '[' || s.charAt(i) == '(' || s.charAt(i) == '{' ) {
					stack.push(s.charAt(i));
				}else {
					switch (s.charAt(i)) {
					case '>':
						if(stack.peek() == '<')	{
							stack.pop();
						}
						else {
							ans=0;
							break here;
						}					
						break;
	
					case '}':
						if(stack.peek() == '{')	{
							stack.pop();
						}
						else {
							ans=0;
							break here;
						}
						break;
						
					case ')':
						if(stack.peek() == '(')	{
							stack.pop();
						}
						else {
							ans=0;
							break here;
						}
						break;
						
					case ']':
						if(stack.peek() == '[')	{
							stack.pop();
						}
						else {
							ans=0;
							break here;
						}
						break;
					}
				}				
			}
		System.out.println("#" + tc + " " + ans);
		}	
	}
}