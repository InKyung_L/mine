package swea;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

class Solution_D3_1216_회문2_서울9반_이인경{
	//public static int start=0, end=7, p=end, ps=start;
	public static int cnt1=0,cnt2=0, ans1=0, ans2=0, max=0, V;
	public static char[][] map;
	public static int N=8;
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1216.txt"));
BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		for(int tc=1;tc<=10;tc++) {
			max=0;
			br.readLine();
			map = new char[N][N];
			for(int i=0;i<N;i++) {
				String num = br.readLine();
				for(int j=0;j<N;j++) {
					map[i][j] = num.charAt(j);
				}
			}
			for(V=0;V<N;V++) {
				go(0,N-1);
			}
			for(V=0;V<N;V++) {
				go1(0,N-1);
			}
			
			System.out.println("#" + tc + " " + max);
		}	
	}

	private static void go(int start, int end) {
		int p=end, ps=start, cnt1=0, last=end;
		while(start<=end) { 
			if(map[V][start]==map[V][end]) {
				cnt1++;
				if(start==end || end-start==1) {
					if(start==end) {
						ans1=(cnt1-1)*2+1;
					}else {
						ans1=cnt1*2;
					}
					max = Math.max(max, ans1);
					go(ps+1,last);
				}
				start++;
				end--;
			}else {
				cnt1=0;
				start=ps;
				p--;
				end=p;
			}
		}
	}
	
	private static void go1(int start, int end) {
		int p=end, ps=start, cnt2=0, last=end;
		while(start<=end) {
			if(map[start][V]==map[end][V]) {
				cnt2++;
				if(start==end || end-start==1) {
					if(start==end) {
						ans2=(cnt2-1)*2+1;
					}else {
						ans2=cnt2*2;
					}
					max = Math.max(max, ans2);
					go1(ps+1,last);
				}
				start++;
				end--;
			}else {
				cnt2=0;
				start=ps;
				p--;
				end=p;
			}
		}
	}
}