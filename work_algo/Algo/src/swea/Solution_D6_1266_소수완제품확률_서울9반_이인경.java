package swea;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;
import java.io.FileInputStream;

class Solution_D6_1266_소수완제품확률_서울9반_이인경{
	public static int[] np = {0,1,4,6,8,9,10,12,14,15,16,18};
	public static double sOMA,sOMB;
	public static double Pr(double p, double n, double r) {
		return nCr(n,r)*Math.pow(p, r)*Math.pow((1-p), n-r);		
	}
	
	public static double nCr(double n, double r) {
		if(n<r) return 0.0;
		if(r==0) return 1.0;
		return nCr(n-1,r-1)+nCr(n-1,r);
	}
	
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d6_1266.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			sOMA = sc.nextInt()*0.01;
			sOMB = sc.nextInt()*0.01;
			double anp = 0.0;
			double bnp = 0.0;
			
			for(int i=0;i<np.length;i++) {
				anp += Pr(sOMA,18, np[i]);
			}
			
			for(int i=0;i<np.length;i++) {
				bnp += Pr(sOMB,18, np[i]);
			}
			
			double ans = 1-(anp*bnp);
			System.out.printf("#%d %.6f\n",tc,ans);
		}	
	}
}