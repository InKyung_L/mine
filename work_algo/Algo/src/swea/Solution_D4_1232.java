package swea;
import java.util.Scanner;

class Solution_D4_1232{
	static class Node{
		boolean isNum;
		double num;
		char op;
		int lidx;
		int ridx;
	}
	public static Node[] nodes;
	
	public static double getNum(int idx){
		if(nodes[idx].isNum) return nodes[idx].num;
		
		double num=-1.0;		
		switch (nodes[idx].op) {
		case '+': num = getNum(nodes[idx].lidx)+getNum(nodes[idx].ridx); break;
		case '-': num = getNum(nodes[idx].lidx)-getNum(nodes[idx].ridx); break;
		case '*': num = getNum(nodes[idx].lidx)*getNum(nodes[idx].ridx); break;
		case '/': num = getNum(nodes[idx].lidx)/getNum(nodes[idx].ridx); break;
		}
		return num;
	}
	
	
	public static void main(String args[]) throws Exception{
		Scanner sc = new Scanner(System.in);
		
		//int T = sc.nextInt();
		for(int tc=1;tc<=10;tc++) {
			int N = sc.nextInt();
			nodes = new Node[N+1];
			for(int i=1;i<=N;i++) {
				Node node = new Node();
				nodes[i] = node;
				
				sc.nextInt();
				String s=sc.next();
				char c = s.charAt(0);
				if('0'<=c && c<='9'){
					node.isNum=true;
					node.num=Integer.parseInt(s);
				}else {
					node.isNum=false;
					node.op=c;
					node.lidx=sc.nextInt();
					node.ridx=sc.nextInt();
				}
			}
			System.out.println("#" + tc + " " + (int)getNum(1));
		}
			
	}

}