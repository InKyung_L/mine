package swea;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;
import java.util.Stack;
import java.io.FileInputStream;

class Solution_D5_1247_최적경로_서울9반_이인경{
	
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d5_1247.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++) {
			int cus = sc.nextInt();
			int[][] hm = new int[2][2];
			int[][] rd = new int[cus][2];
			int xsum=0, ysum=0;
			
			for(int i=0;i<2;i++) {
				hm[i] = new int[] {sc.nextInt(), sc.nextInt()};
			}
			
			for(int i=0;i<cus;i++) {
				rd[i] = new int[] {sc.nextInt(), sc.nextInt()};
			}
			
			Arrays.sort(rd, new Comparator<int[]>() {
				@Override
				public int compare(int[] o1, int[] o2) {
					return Integer.compare(o1[0], o2[0]);
				}
			});
			
			xsum = Math.abs(rd[0][0]-hm[0][0]);
			ysum = Math.abs(rd[0][1]-hm[0][1]);			
			for(int i=1;i<cus;i++) {
				xsum += Math.abs(rd[i][0]-rd[i-1][0]);
				ysum += Math.abs(rd[i][1]-rd[i-1][1]);
			}
			xsum += Math.abs(hm[1][0]-rd[cus-1][0]);
			ysum += Math.abs(hm[1][1]-rd[cus-1][1]);	
			
			System.out.println("#" + tc + " " + (xsum+ysum));
		}	
	}
}