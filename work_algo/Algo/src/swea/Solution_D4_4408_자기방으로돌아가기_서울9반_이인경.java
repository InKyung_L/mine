package swea;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;


class Solution_D4_4408_자기방으로돌아가기_서울9반_이인경3{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_4408.txt"));
		 Scanner sc = new Scanner(System.in);
	        
	        int T = sc.nextInt();	        
	        for(int tc=1; tc<=T; tc++) {
	            int N = sc.nextInt();
	            int[] corri = new int[201];
	            
	            for(int i=0;i<N;i++) {            
	                int a = sc.nextInt();
	                int b = sc.nextInt();
	                
	                if(a%2==1) a+=1;
	                if(b%2==1) b+=1;
                    
	                if(a>b) {
	                	int temp=0;
	                	temp=a;
	                    a=b;
	                    b=temp;
	                }
	                
	                for(int j=a/2;j<=b/2;j++) 
	                    corri[j]++;
	            }
	            
	            Arrays.sort(corri);            
	            System.out.println("#" + tc + " " + corri[200]);
	            
	        }
	    }
	}