package swea;
import java.util.*;
import java.io.FileInputStream;


class Solution_D3_5431_민석이의과제체크하기_서울9반_병찬오빠꺼{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_5431.txt"));
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		for(int tc=0;tc<T;tc++) {
			int total = sc.nextInt();
			int submit_std = sc.nextInt();
			
			int[] student = new int[total];
			int[] submit_num = new int[submit_std];
			
			for(int i=0;i<total;i++) {
				student[i]=i+1;
			}
			
			for(int j=0;j<submit_std;j++) {
				submit_num[j] = sc.nextInt();
			}
			
			for(int n=0;n<total;n++) {
				for(int m=0;m<submit_std;m++) {
					if(student[n] == submit_num[m])
						student[n]=0;
				}
			}
			
			System.out.print("#" + (tc+1) + " ");
			for(int v=0;v<total;v++) {
				if(student[v]!=0)
					System.out.print(student[v]+" ");
			}
			System.out.println();
		}
		
	}
}