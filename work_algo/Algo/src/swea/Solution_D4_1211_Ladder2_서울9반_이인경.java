package swea;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;


class Solution_D4_1211_Ladder2_서울9반_이인경{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_1211.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		for(int tc=1;tc<=1;tc++) {
			br.readLine();
			int[] dx = {0,0,1}, dy = {-1,1,0}, chk=new int[100];
			int curx=0, cury=0;
			int nx=0, ny=0, cnt=0;
			int ans=0;
			int[][] lad = new int[100][100];
						
			for(int i=0;i<100;i++) {
				StringTokenizer st = new StringTokenizer(br.readLine());
				for(int j=0;j<100;j++) {
					lad[i][j] = Integer.parseInt(st.nextToken());
				}
			}
			
			for(int j=0;j<100;j++) {
				if(lad[0][j]==1) {
					int[][] visit = new int[100][100];
					curx = 0; cury = j; cnt=1;
					while(curx<=99) {
						for(int i=0;i<3;i++) {
							nx = curx + dx[i];
							ny = cury + dy[i];
							if(nx == 99) {
								chk[j] = cnt;
							}
							if(nx<0 || nx>=100 || ny<0 || ny>=100) continue;
							if(lad[nx][ny] == 1 && visit[nx][ny]!=j+1) {
								cnt++;
								visit[nx][ny] = j+1;
								curx = nx;
								cury = ny;
								break;
							}
						}
						if(nx == 99) {ans = cnt; break;}
					}		
				}
			}
			
							
			System.out.println("#" + tc + " " + ans);			
		}		
	}
}