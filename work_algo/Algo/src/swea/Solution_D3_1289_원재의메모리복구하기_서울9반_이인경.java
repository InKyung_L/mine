package swea;
import java.util.*;
import java.io.FileInputStream;


class Solution_D3_1289_원재의메모리복구하기_서울9반_이인경{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1289.txt"));
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		
		for(int i=0;i<T;i++) {
			int cnt=0;
			String input = sc.next();
			char[] init = new char[input.length()];
			for(int k=0;k<input.length();k++) {
				init[k] = '0';
			}
			
			for(int p=0;p<input.length();p++) {
				if(input.charAt(p) != init[p]) {
					for(int q=p;q<input.length();q++) {
						init[q] = input.charAt(p);
					}
					cnt++;
				}
			}
			System.out.println("#" + (i+1) + " " + cnt);
		}
	}
}