package swea;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Stack;


class Solution_D4_1233_사칙연산유효성검사_서울9반_이인경{
	public static char[] arr,arr2;
	public static int T;
	public static StringBuilder sb;
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1233.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		
		
		for(int tc=1;tc<=10;tc++) {
			int ans=0;
			T = Integer.parseInt(br.readLine());
			sb = new StringBuilder();
			arr = new char[T+1];
			arr2 = new char[T+1];
			
			for(int i=0;i<T;i++) {
				String[] sen = br.readLine().split(" ");
				arr[i+1] = sen[1].charAt(0);
			}
			
			inorder(1);
			for(int i=0;i<sb.length()-1;i++) {
				if(sb.charAt(i) == '-' || sb.charAt(i) == '*' || sb.charAt(i) == '/' || sb.charAt(i) == '+') {
					if(sb.charAt(i+1) == '-' || sb.charAt(i+1) == '*' || sb.charAt(i+1) == '/' || sb.charAt(i+1) == '+') {
						ans=0;
						break;
					}
				}
				else ans=1;
			}
			System.out.println("#" + tc + " " +ans);
			//System.out.println(sb);
		}			
	}
	
	public static void inorder(int i) {
		if(i<=T && arr[i] != '0') { 
			inorder(2*i);
			//System.out.print(arr[i]);
			sb.append(arr[i]);
			inorder(2*i+1);
		}
	}

}