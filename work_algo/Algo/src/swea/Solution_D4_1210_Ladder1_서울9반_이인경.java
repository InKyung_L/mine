package swea;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D4_1210_Ladder1_서울9반_이인경{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_1210.txt"));

		Scanner sc = new Scanner(System.in);
		
		for(int tc=1;tc<=10;tc++) {
			int T = sc.nextInt();
			int[] dx = {0,0,-1};
			int[] dy = {-1,1,0};
			int curx=0; int cury=0;
			int nx=0; int ny=0;
			int ans=10;
			boolean[][] visit = new boolean[100][100];
			int[][] lad = new int[100][100];
			
			
			for(int i=0;i<100;i++) {
				for(int j=0;j<100;j++) {
					lad[i][j] = sc.nextInt();
					if(lad[i][j] == 2) {
						curx = i; cury = j;
					}
				}
			}
			
			while(curx>0) {
				for(int i=0;i<3;i++) {
					nx = curx + dx[i];
					ny = cury + dy[i];
					if(nx == 0) ans = cury;
					if(nx<0 || nx>=100 || ny<0 || ny>=100) continue;
					if(lad[nx][ny] == 1 && visit[nx][ny]==false) {
						visit[nx][ny] = true;
						curx = nx;
						cury = ny;
						break;
					}
				}
				if(nx == 0) {ans = cury; break;}
			}
						
			System.out.println("#" + tc + " " + ans);
			
		}
		
	}
}