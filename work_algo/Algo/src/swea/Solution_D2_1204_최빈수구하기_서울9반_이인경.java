package swea;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D2_1204_최빈수구하기_서울9반_이인경{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d2_1204.txt")); 

		Scanner sc = new Scanner(System.in);
		int max = 0;
		int T = sc.nextInt(); //테스트케이스
		int[] A= new int[1001]; //배열에 저장
		
		for(int i=0;i<T;i++) {
			int cnt1 = 2, cnt2 = 2;
			int n = sc.nextInt();
			for(int j=0;j<1000;j++) {
				A[j]=sc.nextInt();
			}
			Arrays.sort(A);
		
			for(int p=0;p<999;p++) {
				if(A[p] == A[p+1]) {
					cnt1++;
				}
				else {
					if(cnt1>=cnt2) {
						max = p;
						cnt2=cnt1;
						cnt1 = 2;
					}
					cnt1=2;	
				}

			}			
			System.out.println("#"+(i+1)+" " + A[max]);
		}
	}
}