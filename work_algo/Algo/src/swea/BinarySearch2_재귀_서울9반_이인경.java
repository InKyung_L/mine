package swea;

import java.util.Scanner;

public class BinarySearch2_재귀_서울9반_이인경 {
	
	public static void binarySearch(int arr[], int low, int high, int key) {
	
		int middle = (low+high)/2;
		
		if(low>high) {
			System.out.println("실패");
			return;
		}
		
		if(key == arr[middle]) {
			System.out.println("성공");
			return;
		}			
		
		else if (key<arr[middle])
			binarySearch(arr, low, middle-1, key);
		
		else if(key>arr[middle])
			binarySearch(arr, middle+1, high, key);
		
	}
	
	public static void main(String args[]) {
		int[] arr = {2,4,7,9,11,19,23};
		Scanner sc = new Scanner(System.in);
		int key = sc.nextInt();
		binarySearch(arr, 0, arr.length-1, 20);
	}
}
