package swea;

import java.util.Scanner;

public class BinarySearch1_반복_서울9반_이인경 {
	
	public static void main(String args[]) {
		int[] arr = {2,4,7,9,11,19,23};
		Scanner sc = new Scanner(System.in);
		int key = sc.nextInt();
		int start = 0;
		int end = arr.length-1;
		
		while(start <= end) {
			int middle = (start+end)/2;
			if(arr[middle] == key) {
				System.out.println("성공");
				break;
			}
			else if(arr[middle] > key) {
				end = middle-1;
			}
			else if(arr[middle] < key)
				start = middle+1;
		}
		if(start>end)
			System.out.println("실패");
	}
}
