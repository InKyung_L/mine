package swea;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.StringTokenizer;

public class Main_정올_1863_종교_서울9반_이인경2 { // 학생들이 최대한 가질 수 있는 종교의 가지 수
//유니온 파인드에서 덩어리의 개수?
	public static int i, j, Ans, pp;
	public static int[] p;

//public static boolean[] visited;
	public static int getParent(int x) {
		if (p[x] == x)
			return x;
		else
			return p[x] = getParent(p[x]); // 계속 부모를 따라감(재귀)
	}

	public static void union(int i, int j) {
		i = getParent(i);
		j = getParent(j);
		p[j] = i; // 대소비교 없이 묶기만
	}

	public static boolean find(int i, int j) {
		i = getParent(i);
		j = getParent(j);
		if (i == j)
			return true; // 같은 집합
		else
			return false; // 다른 집합
	}

	public static void main(String[] args) throws Exception {
		System.setIn(new java.io.FileInputStream("res/input_JO_1863.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;

		st = new StringTokenizer(br.readLine());
		int N = Integer.parseInt(st.nextToken()); // 학생수
		int M = Integer.parseInt(st.nextToken()); // 쌍의 수

//make set //학생 set
		p = new int[N];
		for (int n = 0; n < N; n++) {
			p[n] = n; // 스스로가 부모임
		}

		for (int m = 0; m < M; m++) {
			st = new StringTokenizer(br.readLine());
			i = Integer.parseInt(st.nextToken()) - 1;
			j = Integer.parseInt(st.nextToken()) - 1; // 학생인덱스
//학생 i와 학생 j가 같은 종교를 가진다. union
			union(i, j);
		} // 다묶음

//Ans=0;
		/*
		 * int pi = 0; pp = getParent(pi);
		 */
		HashSet<Integer> hs = new HashSet<>();
		for (int n = 0; n < N; n++) {
			hs.add(getParent(n));
		}
		Ans = hs.size();
		System.out.println(Ans);
	}
}
