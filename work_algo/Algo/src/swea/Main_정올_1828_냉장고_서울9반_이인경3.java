package swea;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Main_정올_1828_냉장고_서울9반_이인경3 {
	public static void main(String[] args) throws Exception{
		System.setIn(new FileInputStream("res/input_jo_1828.txt"));
		Scanner sc = new Scanner(System.in);
		
		int N = sc.nextInt();
		int[][] chems = new int[N][2];
		for (int i = 0; i < N; i++) {
			chems[i][0] = sc.nextInt();
			chems[i][1] = sc.nextInt();
		}

		Arrays.sort(chems, new Comparator<int[]>() {
			@Override
			public int compare(int[] a, int[] b) {
				return a[0] <= b[0] ? -1 : 1;
			}
		});
		
		ArrayList<int[]> list = new ArrayList<int[]>();
		list.add(chems[0]);
here:   for (int i = 1; i < chems.length; i++) {
			int[] chem = chems[i];
			for (int j = 0; j < list.size(); j++) {
				int[] ref = list.get(j);
				if (ref[1] >= chem[0]) {
					ref[0] = ref[0] >= chem[0] ? ref[0] : chem[0];
					ref[1] = ref[1] <= chem[1] ? ref[1] : chem[1];
					continue here;
				}
			}
			list.add(chem);
		}
		System.out.println(list.size());
	}
}
