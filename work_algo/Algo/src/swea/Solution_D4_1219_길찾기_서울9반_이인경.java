package swea;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;
import java.io.FileInputStream;

class Solution_D4_1219_길찾기_서울9반_이인경{
	
	private static boolean[] visited;
	private static int E;
	private static int[][] graph;
	private static int ans;
	
	private static void dfs(int node) {
		visited[node] = true;
		for(int next=0;next<100;next++) {
			if(visited[next] == false && graph[node][next]==1) {
				dfs(next);
			}
		}
		if(visited[99] == true)
			ans=1;
		
	}
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_1219.txt"));
		Scanner sc = new Scanner(System.in);
	
		for(int tc=1;tc<=10;tc++) {
			ans=0;
			int num = sc.nextInt();
			E = sc.nextInt();
			graph = new int[100][100];
			
			for(int i=0;i<E;i++) {
				int v1 = sc.nextInt();
				int v2 = sc.nextInt();
				graph[v1][v2] = 1;
			}
			visited = new boolean[100];
			dfs(0);
			System.out.println("#" + tc + " " + ans);
		}	
	}
}