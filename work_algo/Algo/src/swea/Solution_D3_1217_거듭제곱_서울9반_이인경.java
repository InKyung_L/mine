package swea;

import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D3_1217_거듭제곱_서울9반_이인경 {
	
	public static int pows(int n, int p) {
		int sum = 1;
		for(int i=1;i<=p;i++) {
			sum = sum*n;
		}
		return sum;	
	}
	
	public static int dpows(int n, int p) {
		int sum = 1;
		int ans=0;
		if(p%2 == 0) {
			for(int i=1;i<=p/2;i++) {
				sum = sum*n;
			}
			ans = sum*sum;
		}
		else {
			for(int i=1;i<=(p-1)/2;i++) {
				sum = sum*n;
			}
			ans = sum*sum*n;
		}
		
		return ans;	
	}
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1217.txt"));
		Scanner sc = new Scanner(System.in);

		for(int tc = 1; tc <= 10; tc++){
			int tcase = sc.nextInt();
			int num = sc.nextInt();
			int pow = sc.nextInt();
			
			//System.out.println("#" + tc + " " + (int)(Math.pow(num, pow)));	
			System.out.println("#" + tc + " " + dpows(num,pow));	
		}
	}
}