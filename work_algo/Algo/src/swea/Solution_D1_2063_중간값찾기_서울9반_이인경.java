package swea;
import java.util.Arrays;
import java.util.Scanner;
import java.io.FileInputStream;


class Solution_D1_2063_중간값찾기_서울9반_이인경{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d1_2063.txt")); //algo에 res폴더 추가. 그리고 txt파일 추가

		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		int[] A= new int[T];
		
		for(int i=0;i<T;i++) {
			A[i]=sc.nextInt();
		}
		
		//Arrays.sort(A);
		for(int i=A.length-1;i>0;i--) {
			int temp=0;
			for(int j=0;j<i;j++) {
				if(A[j]>A[j+1]) {
					temp = A[j];
					A[j] = A[j+1];
					A[j+1]=temp;
				}
			}
		}
		int a = T/2;
		System.out.println(A[a]);
		
	}
}