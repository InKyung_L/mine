package swea;

import java.awt.Point;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

class Solution_D4_1226_미로1_Bfs_서울9반_이인경{
	public static Queue<Point> q;
	public static int[][] visit;
	public static char[][] maze;
	public static int si,sj,ei,ej,ans;
	public static int[] di= {0,0,1,-1}, dj= {1,-1,0,0};
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_1226.txt"));
		Scanner sc = new Scanner(System.in);
	
		for(int tc=1;tc<=10;tc++) {
			sc.nextInt();
			visit = new int[16][16];
			maze = new char[16][16];
			ans=1;
			for(int i=0;i<16;i++) {
				String st = sc.next();
				for(int j=0;j<16;j++) {
					maze[i][j] = st.charAt(j);
					if(maze[i][j]=='2') {
						si=i; sj=j;
					}else if(maze[i][j]=='3') {
						ei=i; ej=j;
					}
				}
			}
			
			bfs(si,sj);
			if(visit[ei][ej]==0) {
				ans=0;
			}
			
			System.out.println("#"+tc+" "+ans);
		}
		
	}

	private static void bfs(int i, int j) {
		visit[i][j]=1;
		q = new LinkedList<>();
		q.offer(new Point(i,j));
		
		while(!q.isEmpty()) {
			i = q.peek().x;
			j = q.poll().y;
			
			for(int d=0;d<di.length;d++) {
				int ni=i+di[d];
				int nj=j+dj[d];
				
				if(ni>=0 && ni<16 && nj>=0 && nj<16 && visit[ni][nj]==0 && maze[ni][nj]!='1') {
					visit[ni][nj]=1;
					q.offer(new Point(ni,nj));
				}
			}
		}
		
	}
}