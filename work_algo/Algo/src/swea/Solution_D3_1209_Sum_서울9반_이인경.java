package swea;
import java.util.*;
import java.io.FileInputStream;


class Solution_D3_1209_Sum_서울9반_이인경{
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d3_1209.txt"));
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		int[][] A = new int[101][101];
		int[] D = new int[102];
		
		Arrays.fill(D, 1);		
		
		for(int i=0;i<T;i++) {
			int max=0, sumx=0, sumy=0, sumc1=0, sumc2=0;
			for(int p=0;p<100;p++) {
				for(int q=0;q<100;q++) {
					A[p][q] = sc.nextInt();
				}
			}
			
			
			for(int x=0;x<100;x++) {
				for(int y=0;y<100;y++) {
					sumx += A[x][y];
					sumy += A[y][x];
					if(sumx > max)
						max = sumx;
					else if(sumy > max)
						max = sumy;
				}
			}
			
			//오른쪽위->왼쪽아래
			for(int n=0;n<100;n++) {
				for(int m=99;m>=0;m--) {
					sumc1 += A[n+D[n]-1][m-D[n]+1];
					if(sumc1 > max)
						max = sumc1;
				}
			}
			
			//왼쪽위->오른쪽아래
			for(int w=0;w<100;w++) {
				for(int z=0;z<100;z++) {
					sumc2 += A[w+D[w]][z+D[w]];
					if(sumc2 > max)
						max = sumc2;
				}
			}
			
			System.out.println("#"+(i+1)+ " " +max);
		}
	}
}