package swea;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;


class Solution_D4_1211_Ladder2_서울9반_이인경2{
	
	public static void main(String args[]) throws Exception{
		System.setIn(new FileInputStream("res/input_d4_1211.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		for(int tc=1;tc<=10;tc++) {
			br.readLine();
			int[] di = {0,0,1}, dj = {-1,1,0};
			int cnt=0, curi=0, curj=0;
			int[][] lad = new int[100][100];
			int[][] visit = new int[100][100];
			int[][] chk = new int[1][2];
			chk[0][0] = Integer.MAX_VALUE;
			chk[0][1] = Integer.MAX_VALUE;
						
			for(int i=0;i<100;i++) {
				StringTokenizer st = new StringTokenizer(br.readLine());
				for(int j=0;j<100;j++) {
					lad[i][j] = Integer.parseInt(st.nextToken());
				}
			}
			
			for(int j=0;j<100;j++) {
				if(lad[0][j]==1) {
					curi=0; curj=j;
					visit = new int[100][100];
					cnt=1;
					while(curi<=99) {
						for(int d=0;d<3;d++) {
							int ni=curi+di[d];
							int nj=curj+dj[d];
							
							if(ni<0 || ni>=100 || nj<0 || nj>=100) continue;
							if(ni>=0 && ni<100 && nj>=0 && nj<100 && lad[ni][nj]!=0 && visit[ni][nj]==0) {
								cnt++;
								visit[ni][nj]=1;
								curi=ni;
								curj=nj;
							}
						}
						if(curi==99) break;
					}
					if(chk[0][0] >= cnt) {
						chk[0][0] = cnt;
						chk[0][1] = j;
					}
				}
			}
			System.out.println("#" + tc + " " + chk[0][1]);
				
		}		
	}
}