package swea;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

class Solution_D4_1227_미로2_서울9반_이인경{
	public static Stack<Character> stack = new Stack<>();
	public static Stack<Integer> stack2 = new Stack<>();
	public static int[] di= {0,0,-1,1};
	public static int[] dj= {-1,1,0,0};
	public static int[][] visit;
	public static char[][] maze;

	public static int curx,cury;
	public static int endx,endy;
	
	public static void main(String args[]) throws Exception{
//		System.setIn(new FileInputStream("res/input_d4_1227.txt"));
//		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//
		System.setIn(new FileInputStream("res/input_d4_1227.txt"));
		Scanner sc = new Scanner(System.in);
		
		for(int tc=1;tc<=10;tc++) {
			int ans=0;
			visit = new int[100][100];
			int num = sc.nextInt();
			maze = new char[100][100];
			String line = "";
			int n = 0;
			
			while(n<100) {
				line = sc.next();
				for(int j=0;j<line.length();j++) {
					maze[n][j] = line.charAt(j);
					if(maze[n][j] == '2') {
						curx = n;
						cury = j;
					}
					else if(maze[n][j] == '3') {
						endx = n;
						endy = j;
					}
				}
				n++;
			}
				
			
			dfs(curx,cury,0);
			
			if(visit[endx][endy] == 1) {
				ans=1;
			}
			System.out.println("#" + tc + " " + ans);
		}
	}

	private static void dfs(int i, int j, int cnt) {
		visit[i][j] = 1;
		
		if(i==endx && j==endy) {
			//System.out.println("i=" + i + ", j=" + j + ", cnt=" + cnt);
			return;
		}
		
		for(int d=0;d<di.length;d++) {
			int ni=i+di[d];
			int nj=j+dj[d];
			
			if(0<=ni && ni<100 && 0<=nj && nj<100 && visit[ni][nj]==0 && maze[ni][nj]!='1') {
				visit[ni][nj] = 1;
				dfs(ni,nj,cnt+1);
			}
		}
		
		
		
		
		
	}
}
	